﻿using CarPooling.Service.ResponseModels;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CarPooling.Hubs
{
    public class ChatHub : Hub
    {
        public async Task SendMessage(MessageResponseModel message)
            => await Clients.All.SendAsync("receiveMessage", message);
    }
}
