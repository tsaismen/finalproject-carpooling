﻿const chat = document.getElementById('popupchat');

function addMessageToChat(message , username, userprofilepictureurl) {
    // Get current date
    var currentdate = new Date();
    var datestring = (currentdate.getMonth() + 1) + "/"
        + currentdate.getDate() + "/"
        + currentdate.getFullYear() + " "
        + currentdate.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })

    message = "<strong>" + message + "</strong>";
    // Main span container
    let container = document.createElement('span');
    container.className = "chat_msg_item chat_msg_item_admin";
    container.title = datestring;

    let imgcontainer = document.createElement('div');
    imgcontainer.className = "chat_avatar";

    let chatcontent = document.createElement('div');
    chatcontent.className = "chat-content";

    let usernamecontainer = document.createElement('span');
    usernamecontainer.textContent = username;

    let br = document.createElement('br');

    let chatmessage= document.createElement('span');
    chatmessage.innerHTML = message;

    let senderpicture = document.createElement('img');
    senderpicture.className = "rounded-circle user_img_msg";
    if (userprofilepictureurl == null) {
        senderpicture.src = "/DefaultProfilePic.jpg";
    } else
    {
        senderpicture.src = userprofilepictureurl;
    }

    imgcontainer.appendChild(senderpicture);

    chatcontent.appendChild(usernamecontainer);
    chatcontent.appendChild(br);
    chatcontent.appendChild(chatmessage);

    container.appendChild(imgcontainer);
    container.appendChild(chatcontent);

    chat.appendChild(container);
    chat.scrollTop = chat.scrollHeight;
    document.getElementById('messageSent').innerHTML = '';
}

