﻿let currentPageNumberPassanger = 1;

$("#container-table-passanger").on("click", "#prev-button-passanger", function (event) {
    getPreviousPageDataPassanger();
});

$("#container-table-passanger").on("click", "#next-button-passanger", function (event) {
    getNextPageDataPassanger();
});

function getPreviousPageDataPassanger() {
    if (currentPageNumberPassanger > 0) {
        currentPageNumberPassanger--;
        getDataPassanger(currentPageNumberPassanger);
    }
}

function getNextPageDataPassanger() {
    currentPageNumberPassanger++;
    getDataPassanger(currentPageNumberPassanger);
}

function getDataPassanger(newPageNumber) {

    $.ajax({
        url: "/Users/GetTripDataAsPassanger",
        type: "POST",
        data: { "page": newPageNumber },
        dataType : "json",
        success: function (response) {
            $("#container-table-passanger").empty();
            $("#container-table-passanger").html(response.html);
        },
        error: function () {

        }
    });
}

