﻿
showInPopup = (url, title) => {
    $.ajax({
        type: "GET",
        url: url,
        success: function (res) {
            $("#form-modal .modal-body").html(res);
            $("#form-modal .modal-title").html(title);
            $("#form-modal").modal("show");
        }
    });
}
showRatingInPopup = (url, title, id, username,tripId) => {
    $.ajax({
        type: "GET",
        url: url,
        data: { "ratedUserId": id, "username": username ,"tripId" : tripId},
        success: function (res) {
            $("#form-modal .modal-body").html(res);
            $("#form-modal .modal-title").html(title);
            $("#form-modal").modal("show");
        }
    });
}
EmptyAjaxRating = form => {
    $.ajax({
        type: "POST",
        url: "/Ratings/Create",
        data: new FormData(form),
        contentType: false,
        processData: false,
        success: function (res) {
            $("#form-modal .modal-body").html('');
            $("#form-modal .modal-title").html('');
            $("#form-modal .show").remove();
            $("#form-modal").css('display', 'none');
            $(".modal-backdrop").remove();
        }
    });
}

showCandidatesInPopup = (url, title, tripId) => {
    $.ajax({
        type: "GET",
        url: url,
        data: { "tripId": tripId },
        success: function (res) {
            $("#form-modal .modal-body").html(res);
            $("#form-modal .modal-title").html(title);
            $("#form-modal").modal("show");
        }
    });
}
EmptyAjaxCandidates = (url, tripId, userId) => {
    $.ajax({
        type: "POST",
        url: url,
        data: { "tripId": tripId, "userId": userId },
        success: function (res) {
            showCandidatesInPopup('/Trips/ShowCandidates', 'Candidates', tripId);
        }
    });
}
EmptyAjaxPassengers = (url, tripId, userId) => {
    $.ajax({
        type: "POST",
        url: url,
        data: { "tripId": tripId, "userId": userId },
        dataType: "json",
        success: function (response) {
            $("#container-table-passengers").empty();
            $("#container-table-passengers").html(response.html);
        },
        error: function () {

        }
    });
}

AjaxPosting = form => {
    $.ajax({
        type: "POST",
        url: "/Address/Create",
        contentType: false,
        processData: false,
        data: new FormData(form),
        success: function (result) {
            $('#showAdrress').append($('<option>'), {
                value: result.value,
                text: result.html
            })
        }
    });
}

EmptyAjaxUnapply = tripId => {

    $.ajax({
        url: "/Users/UnapplyFromTrip",
        type: "POST",
        data: { "tripId": tripId },
        dataType: "json",
        success: function (response) {
            location.reload();
        },
        error: function () {
            location.reload();
        }
    });
}
EmptyAjaxApply = tripId => {

    $.ajax({
        url: "/Users/ApplyForTrip",
        type: "POST",
        data: { "tripId": tripId },
        dataType: "json",
        success: function (response) {
            location.reload();
        },
        error: function () {
            location.reload();
        }
    });
}
EmptyAjaxLeaveTrip = tripId => {

    $.ajax({
        url: "/Users/LeaveTrip",
        type: "POST",
        data: { "tripId": tripId },
        dataType: "json",
        success: function (response) {
            location.reload();
        },
        error: function () {
            location.reload();
        }
    });
}
