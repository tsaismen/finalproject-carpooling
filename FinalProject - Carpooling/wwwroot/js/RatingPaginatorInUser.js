﻿let currentPageNumberRating = 1;

$("#container-table-rating").on("click", "#prev-button-rating", function (event) {
    getPreviousPageDataRating();
});

$("#container-table-rating").on("click", "#next-button-rating", function (event) {
    getNextPageDataRating();
});

function getPreviousPageDataRating() {
    if (currentPageNumberRating > 0) {
        currentPageNumberRating--;
        getDataRating(currentPageNumberRating);
    }
}

function getNextPageDataRating() {
    currentPageNumberRating++;
    getDataRating(currentPageNumberRating);
}

function getDataRating(newPageNumber) {

    $.ajax({
        url: "/Users/GetRatingData",
        type: "POST",
        data: { "page": newPageNumber },
        dataType : "json",
        success: function (response) {
            $("#container-table-rating").empty();
            $("#container-table-rating").html(response.html);
        },
        error: function () {

        }
    });
}
