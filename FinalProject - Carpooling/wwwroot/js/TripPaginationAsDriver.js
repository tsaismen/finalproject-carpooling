﻿let currentPageNumberDriver = 1;

$("#container-table").on("click", "#prev-button", function (event) {
    getPreviousPageData();
});

$("#container-table").on("click", "#next-button", function (event) {
    getNextPageData();
});

function getPreviousPageData() {
    if (currentPageNumberDriver > 0) {
        currentPageNumberDriver--;
        getData(currentPageNumberDriver);
    }
}

function getNextPageData() {
    currentPageNumberDriver++;
    getData(currentPageNumberDriver);
}

function getData(newPageNumber) {

    $.ajax({
        url: "/Users/GetTripDataAsDriver",
        type: "POST",
        data: { "page": newPageNumber },
        dataType : "json",
        success: function (response) {
            $("#container-table").empty();
            $("#container-table").html(response.html);
        },
        error: function () {

        }
    });
}
