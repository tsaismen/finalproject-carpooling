﻿using CarPooling.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace CarPooling.Helpers
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class MyAuthorisationAttribute : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var role = context.HttpContext.Session.GetString("UserRole");
            if (role != null && Role != null)
            {
                if (!Role.Contains(role))
                {
                    var Error = new ErrorViewModel
                    {
                        ImageUrl = "~/assets/customImages/Icons/ErrorIcons/AuthenticationIcon.png",
                        StatusCode = 401,
                        Message = "You are Unauthorized"
                    };
                    context.Result = new RedirectToActionResult("Error", "Home", Error);
                }
            }
            else if (Role != role)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/AuthenticationIcon.png",
                    StatusCode = 401,
                    Message = "You are Unauthorized"
                };
                context.Result = new RedirectToActionResult("Error", "Home", Error);
            }
        }
        public string Role { get; set; }
    }
}
