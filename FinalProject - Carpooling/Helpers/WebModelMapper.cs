﻿using CarPooling.Data.Database.Contracts;
using CarPooling.Data.Models;
using CarPooling.Models.Account;
using CarPooling.Models.Address;
using CarPooling.Models.Rating;
using CarPooling.Models.Trip;
using CarPooling.Models.User;
using CarPooling.Service.ResponseModels;
using System;
using System.Linq;

namespace CarPooling.Helpers
{
    public class WebModelMapper : IWebModelMapper
    {
        private readonly IDatabase database;

        public WebModelMapper(IDatabase database)
        {
            this.database = database;
        }
        public User ToModel(RegisterViewModel registerViewModel)
        {
            var user = new User
            {
                UserName = registerViewModel.UserName,
                FirstName = registerViewModel.FirstName,
                LastName = registerViewModel.LastName,
                Email = registerViewModel.Email,
                PhoneNumber = registerViewModel.PhoneNumber,
                Password = registerViewModel.Password,
            };
            return user;
        }
        public User ToModel(UserProfileViewModel userViewModel)
        {
            var user = new User
            {
                UserName = userViewModel.UserName,
                FirstName = userViewModel.FirstName,
                LastName = userViewModel.LastName,
                Email = userViewModel.Email,
                PhoneNumber = userViewModel.PhoneNumber,
                ProfilePicture = userViewModel.Image
            };
            return user;
        }
        public UserProfileViewModel ToViewModel(User userModel)
        {
            var userViewModel = new UserProfileViewModel
            {
                UserId = userModel.UserId,
                UserName = userModel.UserName,
                FirstName = userModel.FirstName,
                LastName = userModel.LastName,
                Email = userModel.Email,
                PhoneNumber = userModel.PhoneNumber,
                Blocked = userModel.Blocked,
                ProfilePicture = userModel.ProfilePicture == null ? null : string.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(userModel.ProfilePicture))
            };
            return userViewModel;
        }

        public Trip ToModel(TripViewModel tripViewModel)
        {
            var trip = new Trip
            {
                DepartureTime = tripViewModel.DepartureDate.AddHours(tripViewModel.DepartureTime.Hour).AddMinutes(tripViewModel.DepartureTime.Minute),
                CarColor = tripViewModel.CarColor,
                CarModel = tripViewModel.CarModel,
                StartingPointId = tripViewModel.StartingPointId,
                EndingPointId = tripViewModel.EndingPointId,
                SmokingAllowed = tripViewModel.SmokingAllowed,
                AirConditioner = tripViewModel.AirConditioner,
                PetsAllowed = tripViewModel.PetsAllowed,
                FreeSeats = tripViewModel.FreeSeats,
            };

            return trip;
        }

        public TripViewModel ToViewModel(Trip tripModel)
        {
            var trip = new TripViewModel
            {
                TripId = tripModel.TripId,
                CarColor = tripModel.CarColor,
                CarModel = tripModel.CarModel,
                DepartureDate = tripModel.DepartureTime,
                DepartureTime = tripModel.DepartureTime,
                StartingPointId = tripModel.StartingPointId,
                EndingPointId = tripModel.EndingPointId,
                StartingPoint = tripModel.StartingPoint.ToString(),
                EndingPoint = tripModel.EndingPoint.ToString(),
                SmokingAllowed = tripModel.SmokingAllowed,
                AirConditioner = tripModel.AirConditioner,
                Status = tripModel.TripStatus,
                PetsAllowed = tripModel.PetsAllowed,
                FreeSeats = tripModel.FreeSeats,
                Passengers = tripModel.Passangers.Select(x => new UserResponseModel(x.Passanger)),
                Candidates = tripModel.CandidatesForTrip.Select(x => new UserResponseModel(x)),
                DriverId = tripModel.Driver.UserId,
                DriverName = tripModel.Driver.UserName,
                DriverPhone = tripModel.Driver.PhoneNumber
            };

            return trip;
        }
        public TripViewModel ToViewModel(TripResponseModel tripModel)
        {
            var trip = new TripViewModel
            {
                TripId = tripModel.TripId,
                StartingPoint = tripModel.StartingPoint,
                EndingPoint = tripModel.StartingPoint,
                CarColor = tripModel.CarColor,
                CarModel = tripModel.CarModel,
                SmokingAllowed = tripModel.SmokingAllowed,
                AirConditioner = tripModel.AirConditioner,
                PetsAllowed = tripModel.PetsAllowed,
                FreeSeats = tripModel.FreeSeats,
                Passengers = tripModel.Passangers,
                Candidates = tripModel.CandidatesForTrip
            };

            return trip;
        }
        public Address ToModel(AddressViewModel addressViewModel)
        {
            var address = new Address
            {
                Street = addressViewModel.Street,
                CityId = addressViewModel.CityId
            };
            return address;
        }
        public Rating ToModel(RatingViewModel ratingViewModel)
        {
            var rating = new Rating
            {
                RatedUserId = ratingViewModel.RatedUserId,
                RatingUserId = ratingViewModel.RatingUserId,
                RatingPoints = ratingViewModel.RatingPoints,
                Comment = ratingViewModel.Comment
            };
            return rating;
        }
    }
}
