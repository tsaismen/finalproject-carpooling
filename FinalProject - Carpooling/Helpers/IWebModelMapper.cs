﻿using CarPooling.Data.Models;
using CarPooling.Models.Account;
using CarPooling.Models.Address;
using CarPooling.Models.Rating;
using CarPooling.Models.Trip;
using CarPooling.Models.User;
using CarPooling.Service.ResponseModels;

namespace CarPooling.Helpers
{
    public interface IWebModelMapper
    {
        User ToModel(RegisterViewModel registerViewModel);
        UserProfileViewModel ToViewModel(User responseModel);
        User ToModel(UserProfileViewModel userViewModel);
        TripViewModel ToViewModel(Trip tripModel);
        TripViewModel ToViewModel(TripResponseModel tripResponseModel);
        Trip ToModel(TripViewModel tripViewModel);
        Address ToModel(AddressViewModel addressViewModel);
        Rating ToModel(RatingViewModel ratingViewModel);

    }
}
