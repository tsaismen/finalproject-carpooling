﻿using CarPooling.Data.Models;
using CarPooling.Helpers;
using CarPooling.Models;
using CarPooling.Models.Account;
using CarPooling.Service.Constants;
using CarPooling.Service.Services.Contracts;
using CarPooling.Service.Tools;
using CarPooling.Services.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CarPooling.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService userService;
        private readonly IAuthHelper authHelper;
        private readonly IWebModelMapper modelMapper;

        public AccountController(IUserService userService, IAuthHelper authHelper, IWebModelMapper modelMapper)
        {
            this.userService = userService;
            this.authHelper = authHelper;
            this.modelMapper = modelMapper;
        }
        [MyAuthorisation(Role = null)]
        public async Task<IActionResult> Login()
        {
            var loginViewModel = new LoginViewModel();
            return View(loginViewModel);
        }
        [HttpPost]
        [MyAuthorisation(Role = null)]
        public async Task<IActionResult> Login([Bind("Email, Password")] LoginViewModel loginViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(loginViewModel);
            }
            var user = await this.authHelper.FetchUserByCredentials(loginViewModel.Email, loginViewModel.Password);
            if (user == null)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/AuthenticationIcon.png",
                    StatusCode = 401,
                    Message = ExceptionMessages.AuthenticationException
                };
                return this.View("Error", Error);
            }
            this.HttpContext.Session.SetString("UserId", user.UserId.ToString());
            this.HttpContext.Session.SetString("CurrentUser", user.Email);
            this.HttpContext.Session.SetString("UserRole", user.Role.ToString());
            SetSessionStrings(user);
            return this.RedirectToAction("index", "home");
        }
        [HttpGet]
        [MyAuthorisation(Role = null)]
        public async Task<IActionResult> Register()
        {
            var registerViewModel = new RegisterViewModel();
            return View(registerViewModel);
        }
        [HttpPost]
        [MyAuthorisation(Role = null)]
        public async Task<IActionResult> Register([Bind("UserName,FirstName, LastName, Email,PhoneNumber, Password,ConfirmPassword,")] RegisterViewModel registerViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(registerViewModel);
            }
            try
            {
                var user = this.modelMapper.ToModel(registerViewModel);
                await this.userService.CreateAsync(user);
            }
            catch (DuplicateEntityException e)
            {
                var Error = new ErrorViewModel { ImageUrl = "~/assets/customImages/Icons/ErrorIcons/DuplicateIcon.png", StatusCode = 409, Message = e.Message };
                return this.View("Error", Error);
            }

            return this.RedirectToAction(nameof(this.Login));
        }
        [HttpGet]
        public IActionResult Logout()
        {
            this.HttpContext.Session.Remove("CurrentUser");
            this.HttpContext.Session.Remove("UserId");
            this.HttpContext.Session.Remove("UserRole");
            this.HttpContext.Session.Remove("Applied");

            return this.RedirectToAction("index", "home");
        }
        protected internal async void SetSessionStrings(User user)
        {
            if (user.AppliedTripId != null)
            {
                this.HttpContext.Session.SetString("Applied", "true");
            }
            else
            {
                this.HttpContext.Session.SetString("Applied", "false");
            }
        }
    }
}
