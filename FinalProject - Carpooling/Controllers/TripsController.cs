﻿using CarPooling.Data.Models.Enums;
using CarPooling.Helpers;
using CarPooling.Models;
using CarPooling.Models.Trip;
using CarPooling.Service.Constants;
using CarPooling.Service.Services.Contracts;
using CarPooling.Services.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Controllers
{
    public class TripsController : Controller
    {
        private readonly ITripService tripService;
        private readonly IUserService userService;
        private readonly IWebModelMapper modelMapper;

        public TripsController(ITripService tripService, IUserService userService, IWebModelMapper modelMapper)
        {
            this.tripService = tripService;
            this.userService = userService;
            this.modelMapper = modelMapper;
        }
        public async Task<IActionResult> Index()
        {
            var paginatedTrips = await tripService.GetAllPaginatedTripsAsync(1);
            return View(paginatedTrips);
        }
        [HttpPost]
        public async Task<IActionResult> GetAllPaginatedTrips(int page)
        {
            var result = await tripService.GetAllPaginatedTripsAsync(page);
            return Json(new { html = await Renderer.RenderViewAsync(this, "_Table", result, true) });
        }

        public async Task<IActionResult> Details(int id)
        {
            try
            {
                if (HttpContext.Session.Keys.Contains("UserId"))
                {
                    var userId = int.Parse(this.HttpContext.Session.GetString("UserId"));
                    var user = this.userService.GetByIdAsync(userId);
                    RedirectToAction("SetSessionStrings", "Account", await user);
                }
                var trip = await tripService.GetByIdAsync(id);
                var tripViewModel = this.modelMapper.ToViewModel(trip);

                var weatherModel = await tripService.GetWeather(trip);
                tripViewModel.Weather = weatherModel;

                return View(tripViewModel);
            }
            catch (EntityNotFoundException e)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/NotFoundIcon.png",
                    StatusCode = 404,
                    Message = e.Message
                };
                return View("Error", Error);
            }
        }

        [HttpGet]
        [MyAuthorisation(Role = "User,Admin")]
        public async Task<IActionResult> Edit(int id)
        {
            var driverId = int.Parse(this.HttpContext.Session.GetString("UserId"));
            try
            {
                var tripToUpdate = await tripService.GetByIdAsync(id);
                if (tripToUpdate.DriverId != driverId)
                {
                    var Error = new ErrorViewModel
                    {
                        ImageUrl = "~/assets/customImages/Icons/ErrorIcons/AuthenticationIcon.png",
                        StatusCode = 401,
                        Message = ExceptionMessages.YouCannotDoThat
                    };
                    return View("Error", Error);
                }
                var tripViewModel = modelMapper.ToViewModel(tripToUpdate);
                return await TripView(id, tripViewModel);
            }
            catch (EntityNotFoundException e)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/NotFoundIcon.png",
                    StatusCode = 404,
                    Message = e.Message
                };
                return View("Error", Error);
            }
        }

        [HttpPost]
        [MyAuthorisation(Role = "User,Admin")]
        public async Task<IActionResult> Edit(int id, [Bind("StartingPointId,EndingPointId,DepartureDate,DepartureTime,Hours,CarColor,CarModel,AirConditioner,PetsAllowed,SmokingAllowed,Status,FreeSeats")] TripViewModel tripViewModel)
        {
            if (!ModelState.IsValid || tripViewModel.StartingPointId == tripViewModel.EndingPointId)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/InvalidInputIcon.png",
                    StatusCode = 400,
                    Message = ExceptionMessages.InvalidData
                };
                return View("Error", Error);
            }
            try
            {
                tripViewModel.TripId = id;
                var trip = this.modelMapper.ToModel(tripViewModel);
                trip.TripStatus = tripViewModel.Status;
                await tripService.UpdateAsync(id, trip);
                return this.RedirectToAction("Dashboard", "Users");
            }
            catch (EntityNotFoundException е)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/NotFoundIcon.png",
                    StatusCode = 404,
                    Message = е.Message
                };
                return View("Error", Error);
            }
        }
        //
        [HttpGet]
        [MyAuthorisation(Role = "User,Admin")]
        public async Task<IActionResult> Create(int id)
        {
            var newTrip = new TripViewModel();
            return await TripView(0, newTrip);
        }

        [HttpPost]
        [MyAuthorisation(Role = "User,Admin")]
        public async Task<IActionResult> Create([Bind("StartingPointId,EndingPointId,DepartureDate,DepartureTime,CarColor,CarModel,AirConditioner,PetsAllowed,SmokingAllowed,FreeSeats")] TripViewModel tripViewModel)
        {
            if (!ModelState.IsValid || tripViewModel.StartingPointId == tripViewModel.EndingPointId)
            {
                return await TripView(0, tripViewModel);
            }
            try
            {
                var trip = this.modelMapper.ToModel(tripViewModel);
                var driverId = int.Parse(this.HttpContext.Session.GetString("UserId"));
                trip.DriverId = driverId;
                await tripService.CreateAsync(trip);
                return this.RedirectToAction("Dashboard", "Users");
            }
            catch (EntityNotFoundException е)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/NotFoundIcon.png",
                    StatusCode = 404,
                    Message = е.Message
                };
                return View("Error", Error);
            }
        }
        private async Task<IActionResult> TripView(int tripId, TripViewModel tripViewModel)
        {
            var statuses = GetTripStatuses();
            var addresses = await GetAddresses();
            tripViewModel.StartingPointList = addresses;
            tripViewModel.EndingPointList = addresses;
            tripViewModel.TripStatuses = statuses;
            return View(tripViewModel);
        }
        [HttpGet]
        [MyAuthorisation(Role = "User,Admin")]
        public async Task<IActionResult> ShowCandidates(int tripId)
        {
            var driverId = int.Parse(this.HttpContext.Session.GetString("UserId"));
            var trip = await this.tripService.GetByIdAsync(tripId);
            if (trip.DriverId != driverId)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/AuthenticationIcon.png",
                    StatusCode = 401,
                    Message = ExceptionMessages.YouCannotDoThat
                };
                return View("Error", Error);
            }
            var tripViewModel = modelMapper.ToViewModel(trip);

            return View("_TableCandidates", tripViewModel);
        }
        [HttpPost]
        [MyAuthorisation(Role = "User,Admin")]
        public async Task<IActionResult> ApproveCandidate(int tripId, int userId)
        {
            try
            {
                var result = await this.tripService.ApproveCandidatesAsync(tripId, userId);
                return this.Ok();
            }
            catch (EntityNotFoundException e)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/NotFoundIcon.png",
                    StatusCode = 404,
                    Message = e.Message
                };
                return View("Error", Error);
            }
            catch (InvalidUserOperationException e)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/InvalidOperationIcon.png",
                    StatusCode = 400,
                    Message = e.Message
                };
                return View("Error", Error);
            }
        }
        [HttpPost]
        [MyAuthorisation(Role = "User,Admin")]
        public async Task<IActionResult> DenyCandidate(int tripId, int userId)
        {
            try
            {
                var result = await this.tripService.DeclineCandidatesAsync(tripId, userId);
                return this.Ok();
            }
            catch (EntityNotFoundException e)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/NotFoundIcon.png",
                    StatusCode = 404,
                    Message = e.Message
                };
                return View("Error", Error);
            }
        }
        [HttpPost]
        [MyAuthorisation(Role = "User,Admin")]
        public async Task<IActionResult> RemovePassenger(int tripId, int userId)
        {
            try
            {
                var result = await this.tripService.RemovePassanger(tripId, userId);
                var viewModelForPassangers = modelMapper.ToViewModel(result);
                return Json(new { html = await Renderer.RenderViewAsync(this, "_TablePassengers", viewModelForPassangers, true) });
            }
            catch (EntityNotFoundException e)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/NotFoundIcon.png",
                    StatusCode = 404,
                    Message = e.Message
                };
                return View("Error", Error);
            }
            catch (InvalidUserOperationException e)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/InvalidOperationIcon.png",
                    StatusCode = 400,
                    Message = e.Message
                };
                return View("Error", Error);
            }
        }
        [HttpGet]
        public async Task<IActionResult> FilterByDestination(string destination)
        {
            var result = await this.tripService.FilterByDestination(destination);
            ViewBag.Search = true;
            return View("Index", result);
        }
        private async Task<List<SelectListItem>> GetAddresses()
        {
            var id = int.Parse(this.HttpContext.Session.GetString("UserId"));
            var addresses = await this.userService.TakeUserAddresses(id);
            var addressesList = addresses.Select(x => new SelectListItem()
            {
                Value = x.AddressId.ToString(),
                Text = $"{x.Street} , {x.City}"
            }).ToList();

            return addressesList;
        }

        private SelectList GetTripStatuses()
        {
            var statuses = Enum.GetNames(typeof(TripStatus));

            return new SelectList(statuses);
        }
    }
}
