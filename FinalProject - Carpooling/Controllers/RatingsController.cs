﻿using CarPooling.Helpers;
using CarPooling.Models;
using CarPooling.Models.Rating;
using CarPooling.Service.Constants;
using CarPooling.Service.Services.Contracts;
using CarPooling.Services.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CarPooling.Controllers
{
    public class RatingsController : Controller
    {
        private readonly IRatingService ratingService;
        private readonly IWebModelMapper modelMapper;

        public RatingsController(IRatingService ratingService, IWebModelMapper modelMapper)
        {
            this.ratingService = ratingService;
            this.modelMapper = modelMapper;
        }
        [HttpGet]
        [MyAuthorisation(Role = "User,Admin")]
        public async Task<IActionResult> Create(int ratedUserId, string username, int tripId)
        {
            var ratingView = new RatingViewModel();
            ratingView.RatedUserId = ratedUserId;
            ratingView.TripId = tripId;
            ratingView.RatedUserUserName = username;
            return this.View(ratingView);
        }
        [HttpPost]
        [MyAuthorisation(Role = "User,Admin")]
        public async Task<IActionResult> Create([Bind("RatedUserId,RatingPoints,Comment,TripId")] RatingViewModel ratingViewModel)
        {
            if (!ModelState.IsValid)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/InvalidInputIcon.png",
                    StatusCode = 409,
                    Message = ExceptionMessages.InvalidData
                };
                return View("Error", Error);
            }
            try
            {
                var ratingUserId = int.Parse(this.HttpContext.Session.GetString("UserId"));
                ratingViewModel.RatingUserId = ratingUserId;
                var rating = modelMapper.ToModel(ratingViewModel);
                await this.ratingService.CreateAsync(rating);
                int id = ratingViewModel.TripId;
                return RedirectToAction("Details", "Trips", new { id = id });
            }
            catch (DuplicateEntityException e)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/DuplicateIcon.png",
                    StatusCode = 409,
                    Message = e.Message
                };
                return View("Error", Error);
            }
        }
    }
}
