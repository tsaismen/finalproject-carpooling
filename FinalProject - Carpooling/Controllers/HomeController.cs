﻿using CarPooling.Models;
using CarPooling.Models.Home;
using CarPooling.Service.ResponseModels;
using CarPooling.Service.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;

namespace CarPooling.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserService userService;
        private readonly IHelperService helperService;

        public HomeController(IUserService userService, IHelperService helperService)
        {
            this.userService = userService;
            this.helperService = helperService;
        }

        public async Task<IActionResult> Index()
        {
            var homePage = new HomePageViewModel();
            return await HomePageView(homePage);
        }

        public async Task<IActionResult> About()
        {
            var aboutView = new AboutPageViewModel();
            aboutView.CitiesCount = await this.helperService.CitiesCount();
            aboutView.PositiveRatingsCount = await this.helperService.PositiveRatingsCount();
            aboutView.UsersCount = await this.helperService.UsersCount();
            aboutView.TripsCount = await this.helperService.TripsCount();
            return View(aboutView);
        }
        public async Task<IActionResult> Contact()
        {
            return View();
        }

        //new
        [HttpPost]
        public IActionResult Contact(ContactUsViewModel viewModel)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    using (MailMessage msz = new MailMessage())
                    {
                        msz.From = new MailAddress(viewModel.Email);//Email which you are getting 
                                                                    //from contact us page 
                        msz.To.Add("tripsyrides@gmail.com");//Where mail will be sent 
                        msz.Subject = viewModel.Subject;
                        msz.Body = "From:  " + viewModel.Email + "\n\n" + "Phone Number:" + viewModel.PhoneNumber + "\n\n" + viewModel.Message;

                        SmtpClient smtp = new SmtpClient();

                        smtp.Host = "smtp.gmail.com";

                        smtp.Port = 587;

                        smtp.EnableSsl = true;


                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                        smtp.UseDefaultCredentials = false;

                        smtp.Credentials = new System.Net.NetworkCredential

                        ("tripsyrides@gmail.com", "miroslavmihailov"); //email revieveing and password of the email recieving


                        smtp.Send(msz);

                        ModelState.Clear();
                        ViewBag.Message = "Thank you for Contacting us! ";
                    };
                }
                catch (Exception ex)
                {
                    ModelState.Clear();
                    ViewBag.Message = $" Sorry we are facing Problem here {ex.Message}";
                }
            }

            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Error(ErrorViewModel Error)
        {
            return View(Error);
        }
        private async Task<IActionResult> HomePageView(HomePageViewModel homePage)
        {
            homePage.TopOrganizers = await GetTopOrganizers();
            homePage.TopPassengers = await GetTopPassengers();
            return View(homePage);
        }
        private async Task<IEnumerable<UserResponseModel>> GetTopOrganizers()
        {
            var topOrganizers = await this.userService.GetTopOrganizersAsync();
            return topOrganizers;
        }
        private async Task<IEnumerable<UserResponseModel>> GetTopPassengers()
        {
            var topPassengers = await this.userService.GetTopPassangersAsync();
            return topPassengers;
        }
    }
}
