﻿using CarPooling.Helpers;
using CarPooling.Models;
using CarPooling.Models.Address;
using CarPooling.Service.Services.Contracts;
using CarPooling.Services.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Controllers
{
    public class AddressController : Controller
    {
        private readonly IAddressService addressService;
        private readonly IWebModelMapper modelMapper;
        private readonly IUserService userService;

        public AddressController(IAddressService addressService, IWebModelMapper modelMapper, IUserService userService)
        {
            this.addressService = addressService;
            this.modelMapper = modelMapper;
            this.userService = userService;
        }
        [HttpGet]
        [MyAuthorisation(Role = "User,Admin")]
        public async Task<IActionResult> Create()
        {
            var newAddress = new AddressViewModel();
            newAddress.Cities = await GetCities();
            return View(newAddress);
        }
        [HttpPost]
        [MyAuthorisation(Role = "User,Admin")]
        public async Task<IActionResult> Create(AddressViewModel newAddress)
        {
            var id = int.Parse(this.HttpContext.Session.GetString("UserId"));
            if (!ModelState.IsValid)
            {
                return Json(new { html = await Renderer.RenderViewAsync(this, "Create", newAddress, false) });
            }
            try
            {
                var address = modelMapper.ToModel(newAddress);
                address.UserUsesId = id;
                var createdAddress = await addressService.CreateAsync(address);
                return Json(new { html = $"{createdAddress.Street}", value = $"{createdAddress.AddressId}" });
            }
            catch (DuplicateEntityException e)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/AuthenticationIcon.png",
                    StatusCode = 409,
                    Message = e.Message
                };
                return View("Error", Error);
            }
        }
        private async Task<SelectList> GetCities()
        {
            var cities = await this.addressService.GetAllCitiesAsync();
            return new SelectList(cities, "CityId", "Name");
        }
        private async Task<List<SelectListItem>> GetAddresses()
        {
            var id = int.Parse(this.HttpContext.Session.GetString("UserId"));
            var addresses = await this.userService.TakeUserAddresses(id);
            var addressesList = addresses.Select(x => new SelectListItem()
            {
                Value = x.AddressId.ToString(),
                Text = $"{x.Street} , {x.City}"
            }).ToList();

            return addressesList;
        }
    }
}
