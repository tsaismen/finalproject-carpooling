﻿using CarPooling.Data.Models;
using CarPooling.Helpers;
using CarPooling.Hubs;
using CarPooling.Service.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace CarPooling.Controllers
{
    public class ChatController : Controller
    {
        private readonly IChatService chatService;
        private readonly IUserService userService;
        private readonly IHubContext<ChatHub> chatHub;

        public ChatController(IChatService chatService, IUserService userService, IHubContext<ChatHub> chatHub)
        {
            this.chatService = chatService;
            this.userService = userService;
            this.chatHub = chatHub;
        }

        public async Task<IActionResult> Messenger()
        {
            // var user = await this.userManager.GetUserAsync(User);

            var messanges = await this.chatService.GetAllMessagesAsync();

            return View(messanges);
        }

        [HttpPost]
        [MyAuthorisation(Role = "User,Admin")]
        public async Task<IActionResult> Create(string input)
        {

            var userId = int.Parse(this.HttpContext.Session.GetString("UserId"));
            var user = await this.userService.GetByIdAsync(userId);
            var message = new Message();
            message.Content = input;
            message.UserId = userId;
            var result = await this.chatService.CreateMessageAsync(message);

            if (result == false)
            {
                return BadRequest();
            }
            var picture = user.ProfilePicture == null ? null : string.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(user.ProfilePicture));
            await this.chatHub.Clients.All.SendAsync("receiveMessage", input, user.UserName, picture);

            return Ok();
        }
    }
}
