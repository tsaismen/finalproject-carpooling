﻿using CarPooling.Helpers;
using CarPooling.Models;
using CarPooling.Models.User;
using CarPooling.Service.Constants;
using CarPooling.Service.Services.Contracts;
using CarPooling.Services.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Threading.Tasks;

namespace CarPooling.Controllers
{
    public class UsersController : Controller
    {
        private readonly IUserService userService;
        private readonly IWebModelMapper modelMapper;
        private readonly ITripService tripService;
        private readonly IRatingService ratingService;

        public UsersController(IUserService userService, IWebModelMapper modelMapper, ITripService tripService, IRatingService ratingService)
        {
            this.userService = userService;
            this.modelMapper = modelMapper;
            this.tripService = tripService;
            this.ratingService = ratingService;
        }

        public async Task<IActionResult> Index()
        {
            var paginatedUsers = await userService.GetAllPaginatedUsersAsync(1);
            return View(paginatedUsers);
        }

        [HttpPost]
        public async Task<IActionResult> GetAllPaginatedUsers(int page)
        {
            var result = await userService.GetAllPaginatedUsersAsync(page);
            return Json(new { html = await Renderer.RenderViewAsync(this, "_UsersTable", result, true) });
        }
        [HttpPost]
        public async Task<IActionResult> Filter(string searchWord)
        {
            var filtered = await userService.FilterBySearchword(searchWord);
            ViewBag.Search = true;

            return this.View("Index", filtered);
        }
        [MyAuthorisation(Role = "User,Admin")]
        public async Task<IActionResult> Dashboard()
        {
            try
            {
                var userId = int.Parse(this.HttpContext.Session.GetString("UserId"));
                var user = await userService.GetByIdAsync(userId);
                ViewBag.Rating = user.CalcRating();
                var userProfileViewModel = modelMapper.ToViewModel(user);
                return await UserView(userId, userProfileViewModel);
            }
            catch (EntityNotFoundException e)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/NotFoundIcon.png",
                    StatusCode = 404,
                    Message = e.Message
                };
                return View("Error", Error);
            }
        }
        public async Task<IActionResult> Profile(int id)
        {
            try
            {
                var user = await userService.GetByIdAsync(id);
                ViewBag.Rating = user.CalcRating();
                var userProfileViewModel = modelMapper.ToViewModel(user);
                return await UserView(id, userProfileViewModel);
            }
            catch (EntityNotFoundException e)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/NotFoundIcon.png",
                    StatusCode = 404,
                    Message = e.Message
                };
                return View("Error", Error);
            }
        }
        [HttpPost]
        [MyAuthorisation(Role = "User,Admin")]
        public async Task<IActionResult> Update([Bind("UserName,FirstName, LastName, Email,PhoneNumber")] UserProfileViewModel userViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/InvalidInputIcon.png",
                    StatusCode = 400,
                    Message = ExceptionMessages.InvalidData
                };
                return View("Error", Error);
            }
            var file = Request.Form.Files[0];
            MemoryStream ms = new MemoryStream();
            file.CopyTo(ms);
            userViewModel.Image = ms.ToArray();
            ms.Close();
            ms.Dispose();
            try
            {
                var id = int.Parse(this.HttpContext.Session.GetString("UserId"));
                var user = this.modelMapper.ToModel(userViewModel);
                await this.userService.UpdateAsync(id, user);

                return this.RedirectToAction("Dashboard", "Users");
            }
            catch (EntityNotFoundException e)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/NotFoundIcon.png",
                    StatusCode = 404,
                    Message = e.Message
                };
                return View("Error", Error);
            }
        }

        [HttpPost]
        [MyAuthorisation(Role = "User,Admin")]
        public async Task<IActionResult> ApplyForTrip(int tripId)
        {
            try
            {
                var userId = int.Parse(this.HttpContext.Session.GetString("UserId"));
                await userService.ApplyForTrip(userId, tripId);
                this.HttpContext.Session.SetString("Applied", "true");
                return this.Ok();
            }
            catch (EntityNotFoundException e)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/NotFoundIcon.png",
                    StatusCode = 404,
                    Message = e.Message
                };
                return View("Error", Error);
            }
            catch (InvalidUserOperationException e)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/InvalidOperationIcon.png",
                    StatusCode = 401,
                    Message = e.Message
                };
                return View("Error", Error);
            }
        }
        [HttpPost]
        [MyAuthorisation(Role = "User,Admin")]
        public async Task<IActionResult> UnapplyFromTrip(int tripId)
        {
            try
            {
                var userId = int.Parse(this.HttpContext.Session.GetString("UserId"));
                await userService.LeaveFromCandidates(userId, tripId);
                this.HttpContext.Session.SetString("Applied", "false");
                return this.Ok();
            }
            catch (EntityNotFoundException e)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/NotFoundIcon.png",
                    StatusCode = 404,
                    Message = e.Message
                };
                return View("Error", Error);
            }
        }
        [HttpPost]
        [MyAuthorisation(Role = "User,Admin")]
        public async Task<IActionResult> LeaveTrip(int tripId)
        {
            try
            {
                var userId = int.Parse(this.HttpContext.Session.GetString("UserId"));
                await userService.LeaveTrip(userId, tripId);
                this.HttpContext.Session.SetString("Applied", "false");
                return this.Ok();
            }
            catch (EntityNotFoundException e)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/NotFoundIcon.png",
                    StatusCode = 404,
                    Message = e.Message
                };
                return View("Error", Error);
            }
            catch (InvalidUserOperationException e)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/InvalidOperationIcon.png",
                    StatusCode = 400,
                    Message = e.Message
                };
                return View("Error", Error);
            }
        }
        [HttpGet]
        [MyAuthorisation(Role = "User,Admin")]
        public async Task<IActionResult> ChangePassword()
        {
            var changeView = new ChangePasswordViewModel();
            return this.View(changeView);
        }
        [HttpPost]
        [MyAuthorisation(Role = "User,Admin")]
        public async Task<IActionResult> ChangePassword([Bind("OldPassword,NewPassword,ConfirmPassword")] ChangePasswordViewModel passwordChange)
        {
            if (!ModelState.IsValid)
            {
                return this.View(passwordChange);
            }
            try
            {
                var userId = int.Parse(this.HttpContext.Session.GetString("UserId"));
                var update = await this.userService.UpdatePassword(userId, passwordChange.OldPassword, passwordChange.NewPassword);
                return RedirectToAction("Dashboard");
            }
            catch (EntityNotFoundException e)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/NotFoundIcon.png",
                    StatusCode = 404,
                    Message = e.Message
                };
                return View("Error", Error);
            }
            catch (AuthenticationException e)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/AuthenticationIcon.png",
                    StatusCode = 401,
                    Message = e.Message
                };
                return View("Error", Error);
            }
        }

        [HttpGet]
        [MyAuthorisation(Role = "Admin")]
        public async Task<IActionResult> Block(int id)
        {
            try
            {
                await userService.BlockAsync(id);
                return this.Ok();
            }
            catch (EntityNotFoundException e)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/NotFoundIcon.png",
                    StatusCode = 404,
                    Message = e.Message
                };
                return View("Error", Error);
            }
        }

        [HttpGet]
        [MyAuthorisation(Role = "Admin")]
        public async Task<IActionResult> Unblock(int id)
        {
            try
            {
                await userService.UnblockAsync(id);
                return this.Ok();
            }
            catch (EntityNotFoundException e)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/NotFoundIcon.png",
                    StatusCode = 404,
                    Message = e.Message
                };
                return View("Error", Error);
            }
        }
        [MyAuthorisation(Role = "User,Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                await this.userService.DeleteAsync(id);
            }
            catch (EntityNotFoundException e)
            {
                var Error = new ErrorViewModel
                {
                    ImageUrl = "~/assets/customImages/Icons/ErrorIcons/NotFoundIcon.png",
                    StatusCode = 404,
                    Message = e.Message
                };
                return View("Error", Error);
            }
            return this.RedirectToAction("Home", "Index");
        }
        [HttpPost]
        private async Task<IActionResult> GetTripDataAsPassanger(int page)
        {
            var userId = int.Parse(this.HttpContext.Session.GetString("UserId"));
            var result = await tripService.GetPaginatedTripsByPassangerId(page, userId);
            return Json(new { html = await Renderer.RenderViewAsync(this, "_TableAsPassanger", result, true) });
        }
        [HttpPost]
        private async Task<IActionResult> GetTripDataAsDriver(int page)
        {
            var userId = int.Parse(this.HttpContext.Session.GetString("UserId"));
            var result = await tripService.GetPaginatedTripsByDriverId(page, userId);
            return Json(new { html = await Renderer.RenderViewAsync(this, "_TableAsDriver", result, true) });
        }
        [HttpPost]
        private async Task<IActionResult> GetRatingData(int page)
        {
            var userId = int.Parse(this.HttpContext.Session.GetString("UserId"));
            var result = await ratingService.GetPaginatedRatingForUser(page, userId);
            return Json(new { html = await Renderer.RenderViewAsync(this, "_RatingsTable", result, true) });
        }

        private async Task<IActionResult> UserView(int id, UserProfileViewModel profileView)
        {
            profileView.TripsAsDriver = await tripService.GetPaginatedTripsByDriverId(1, id);
            profileView.TripsAsPassanger = await tripService.GetPaginatedTripsByPassangerId(1, id);
            profileView.ReceivedRatings = await ratingService.GetPaginatedRatingForUser(1, id);
            return this.View(profileView);
        }
    }
}
