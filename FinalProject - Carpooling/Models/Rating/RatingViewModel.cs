﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarPooling.Models.Rating
{
    public class RatingViewModel
    {
        public int RatedUserId { get; set; }
        public string RatedUserUserName { get; set; }
        public int TripId { get; set; }
        public int RatingUserId { get; set; }
        [Required]
        [Range(1, 5, ErrorMessage = "Points must be between {0} and {1} !")]
        public int RatingPoints { get; set; }
        [Required]
        [StringLength(500, MinimumLength = 5, ErrorMessage = "Comment must be between {0} and {1} characters.")]
        public string Comment { get; set; }
    }
}
