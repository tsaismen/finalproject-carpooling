﻿namespace CarPooling.Models.Home
{
    public class AboutPageViewModel
    {
        public int TripsCount { get; set; }
        public int CitiesCount { get; set; }
        public int UsersCount { get; set; }
        public int PositiveRatingsCount { get; set; }
    }
}
