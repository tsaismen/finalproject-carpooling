﻿using CarPooling.Service.ResponseModels;
using System.Collections.Generic;

namespace CarPooling.Models
{
    public class HomePageViewModel
    {
        public IEnumerable<UserResponseModel> TopOrganizers { get; set; }
        public IEnumerable<UserResponseModel> TopPassengers { get; set; }
    }
}
