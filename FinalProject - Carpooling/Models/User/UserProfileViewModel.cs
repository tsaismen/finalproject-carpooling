﻿using CarPooling.Service.ResponseModels;
using System.ComponentModel.DataAnnotations;

namespace CarPooling.Models.User
{
    public class UserProfileViewModel
    {
        public int UserId { get; set; }
        [Required]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public string UserName { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public string FirstName { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public string LastName { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "Not a valid email address.")]
        public string Email { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        public string ProfilePicture { get; set; }
        public byte[]? Image { get; set; }
        public double Rating { get; set; }
        public bool Blocked { get; set; }
        public DataTablePaginator<RatingResponseModel> ReceivedRatings { get; set; }
        public DataTablePaginator<TripResponseModel> TripsAsDriver { get; set; }
        public DataTablePaginator<TripResponseModel> TripsAsPassanger { get; set; }
    }
}
