﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace CarPooling.Models.Address
{
    public class AddressViewModel
    {
        [Required]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public string Street { get; set; }
        public int CityId { get; set; }
        public SelectList Cities { get; set; }
    }
}
