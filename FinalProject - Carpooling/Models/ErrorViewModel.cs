namespace CarPooling.Models
{
    public class ErrorViewModel
    {
        public string ImageUrl { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
}
