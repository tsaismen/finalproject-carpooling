﻿using CarPooling.Data.Models.Enums;
using CarPooling.Service.ResponseModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarPooling.Models.Trip
{
    public class TripViewModel
    {
        public int TripId { get; set; }
        public int StartingPointId { get; set; }
        public List<SelectListItem> StartingPointList { get; set; }
        public string StartingPoint { get; set; }
        public int EndingPointId { get; set; }
        public List<SelectListItem> EndingPointList { get; set; }
        public string EndingPoint { get; set; }

        public TripStatus Status { get; set; }
        public SelectList TripStatuses { get; set; }
        [DataType(DataType.Date)]
        public DateTime DepartureDate { get; set; }
        [DataType(DataType.Time)]
        public DateTime DepartureTime { get; set; }
        public int DriverId { get; set; }
        public string DriverName { get; set; }
        public string DriverPhone { get; set; }
        [Required]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public string CarColor { get; set; }
        [Required]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public string CarModel { get; set; }
        public bool AirConditioner { get; set; }
        public bool PetsAllowed { get; set; }
        public bool SmokingAllowed { get; set; }
        public int FreeSeats { get; set; }
        public IEnumerable<UserResponseModel> Passengers { get; set; }
        public IEnumerable<UserResponseModel> Candidates { get; set; }

        //new
        public TripWeatherResponseModel Weather { get; set; }

    }
}
