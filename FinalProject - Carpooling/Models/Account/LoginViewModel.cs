﻿using System.ComponentModel.DataAnnotations;

namespace CarPooling.Models.Account
{
    public class LoginViewModel
    {
        [Required]
        [EmailAddress(ErrorMessage = "Not a valid email address.")]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
