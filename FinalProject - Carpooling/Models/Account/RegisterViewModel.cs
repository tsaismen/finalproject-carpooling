﻿using System.ComponentModel.DataAnnotations;

namespace CarPooling.Models.Account
{
    public class RegisterViewModel
    {
        [Required]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public string UserName { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public string FirstName { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public string LastName { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "Not a valid email address.")]
        public string Email { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [StringLength(255, ErrorMessage = "Password should be more than 8 characters!", MinimumLength = 8)]
        public string Password { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }
}
