﻿using CarPooling.Service.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CarPooling.ViewComponents
{
    public class ChatViewComponent : ViewComponent
    {
        private readonly IChatService chatService;
        public ChatViewComponent(IChatService chatService)
        {
            this.chatService = chatService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var messanges = await this.chatService.GetAllMessagesAsync();
            return View(messanges);
        }
    }
}
