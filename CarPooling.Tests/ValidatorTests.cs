﻿using CarPooling.Data.Database.Contracts;
using CarPooling.Data.Models;
using CarPooling.Service.Services;
using CarPooling.Service.Tools;
using CarPooling.Services.Exceptions;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockQueryable.Moq;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Tests
{
    [TestClass]
    public class ValidatorTests
    {
        Mock<IDatabase> mockContext;
        Mock<DbSet<City>> mockDbCities;
        Mock<DbSet<User>> mockDbUsers;
        Mock<DbSet<Address>> mockDbAddresses;
        Mock<DbSet<Trip>> mockDbTrips;
        Mock<DbSet<Rating>> mockDbRatings;

        List<User> mockUsers;
        List<Rating> mockRatings;
        List<Address> mockAddresses;
        List<City> mockCities;
        List<Trip> mockTrips;
        List<PassengerTrip> mockPassengerTrips;

        Validator validator;
        [TestInitialize]
        public async Task TestInitialize()
        {
            //Arrange
            var password = "123456789";
            var hashedpassword = new PasswordHasher<object?>().HashPassword(null, password);

            mockContext = new Mock<IDatabase>();
            mockUsers = new List<User>
            {
                new User
                {
                    UserId = 1,
                    UserName = "Pesho",
                    FirstName = "Gero",
                    LastName = "Petkov",
                    Email = "gercata@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08876222746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 2,
                    UserName = "Terminatora",
                    FirstName = "Asparuh",
                    LastName = "Mladenov",
                    Email = "terminatora@gmail.com",
                    Role = Role.Admin,
                    PhoneNumber = "08875322746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 3,
                    UserName = "Mosho",
                    FirstName = "Ivailo",
                    LastName = "Vasilov",
                    Email = "ivchopivcho@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08966222746",
                    Password = hashedpassword,
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 4,
                    UserName = "Bosho",
                    FirstName = "Tonka",
                    LastName = "Miteva",
                    Email = "babavitonka@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08876259746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 5,
                    UserName = "Kitka",
                    FirstName = "Krasi",
                    LastName = "Georgiev",
                    Email = "Krasko404@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08876222746",
                    Blocked = false,
                    isDeleted = false

                }

            };
            mockDbUsers = mockUsers.AsQueryable().BuildMockDbSet();
            mockContext.Setup(db => db.Users).Returns(mockDbUsers.Object);
            validator = new Validator(mockContext.Object);
        }
        [TestMethod]
        public void ValidatesUsername_ThrowsException()
        {
            Assert.ThrowsException<DuplicateEntityException>(() => validator.ValidateUsername("Terminatora"));
        }

        [TestMethod]
        public void ValidatesEmail_ThrowsException()
        {
            Assert.ThrowsException<DuplicateEntityException>(() => validator.ValidateEmail("gercata@gmail.com"));
        }

        [TestMethod]
        public void ValidatesPassword_ThrowsException()
        {
            Assert.ThrowsException<InvalidInputDataException>(() => validator.ValidatePassword("123456789"));
        }

        [TestMethod]
        public void ValidatesPhoneNumber_ThrowsException()
        {
            Assert.ThrowsException<DuplicateEntityException>(() => validator.ValidatePhoneNumber("08966222746"));
        }
    }
}
