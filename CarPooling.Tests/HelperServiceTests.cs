﻿using CarPooling.Data.Database.Contracts;
using CarPooling.Data.Models;
using CarPooling.Data.Models.Enums;
using CarPooling.Service.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Tests
{
    [TestClass]
    public class HelperServiceTests
    {

        Mock<IDatabase> mockContext;
        Mock<DbSet<City>> mockDbCities;
        Mock<DbSet<User>> mockDbUsers;
        Mock<DbSet<Address>> mockDbAddresses;
        Mock<DbSet<Trip>> mockDbTrips;
        Mock<DbSet<Rating>> mockDbRatings;
        Mock<DbSet<PassengerTrip>> mockDbPassengerTrips;

        List<User> mockUsers;
        List<Rating> mockRatings;
        List<Address> mockAddresses;
        List<City> mockCities;
        List<Trip> mockTrips;
        List<PassengerTrip> mockPassengerTrips;
        [TestInitialize]
        public async Task TestInitialize()
        {
            mockContext = new Mock<IDatabase>();
            mockUsers = new List<User>
            {
                new User
                {
                    UserId = 1,
                    UserName = "Pesho",
                    FirstName = "Gero",
                    LastName = "Petkov",
                    Email = "gercata@test.com",
                    Role = Role.User,
                    PhoneNumber = "08876222746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 2,
                    UserName = "Terminatora",
                    FirstName = "Asparuh",
                    LastName = "Mladenov",
                    Email = "terminatora@gmail.com",
                    Role = Role.Admin,
                    PhoneNumber = "08875322746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 3,
                    UserName = "Mosho",
                    FirstName = "Ivailo",
                    LastName = "Vasilov",
                    Email = "ivchopivcho@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08966222746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 4,
                    UserName = "Bosho",
                    FirstName = "Tonka",
                    LastName = "Miteva",
                    Email = "babavitonka@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08876259746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 5,
                    UserName = "Kitka",
                    FirstName = "Krasi",
                    LastName = "Georgiev",
                    Email = "Krasko404@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08876222746",
                    Blocked = false,
                    isDeleted = false

                }

            };

            mockRatings = new List<Rating>
            {

                new Rating
                {
                    RatingId = 1,
                    RatingPoints = 1,
                    Comment = "Mnogo ludo karash",
                    RatedUserId = 1,
                    RatedUser = mockUsers[0],
                    RatingUserId = 2,
                    RatingUser = mockUsers[1],
                    isDeleted = false,
                },
                new Rating
                {
                    RatingId = 2,
                    RatingPoints = 4,
                    Comment = "qki janti",
                    RatedUserId = 3,
                    RatedUser = mockUsers[2],
                    RatingUserId = 5,
                    RatingUser = mockUsers[4],
                    isDeleted = false,
                },

            };

            mockCities = new List<City>
            {
                new City
                {
                    CityId = 1,
                    Name = "Plovdiv"
                },
                new City
                {
                    CityId = 2,
                    Name = "Sofia"
                },

            };

            mockAddresses = new List<Address>
            {
                new Address
                {
                    AddressId = 1,
                    Street = "Botev 45",
                    CityId = 1,
                    City = mockCities[0],
                    UserUsesId= 2,
                    isDeleted = false
                },
                new Address
                {
                    AddressId = 2,
                    Street = "Dundkov 1",
                    CityId = 2,
                    City = mockCities[1],
                    UserUsesId= 1,
                    isDeleted = false
                }
            };

            mockTrips = new List<Trip>
            {
                 new Trip
                {
                    TripId = 1,
                    DriverId = 3,
                    Driver = mockUsers[2],
                    CarColor="Green",
                    CarModel = "Audi r8",
                    StartingPointId = 1,
                   StartingPoint = mockAddresses[0],
                    EndingPointId = 2,
                   EndingPoint = mockAddresses[1],
                    DepartureTime = DateTime.Now.AddHours(3),
                    FreeSeats = 3,
                    PetsAllowed = false,
                    SmokingAllowed = false,
                    AirConditioner = true,
                    TripStatus = TripStatus.Active,
                    isDeleted = false
                },

                 new Trip
                {
                    TripId = 2,
                    DriverId = 1,
                    Driver = mockUsers[0],
                    CarColor = "Blue",
                    CarModel = "Mercedes Benz",
                    StartingPointId = 2,
                    StartingPoint = mockAddresses[1],
                    EndingPointId = 1,
                    EndingPoint = mockAddresses[0],
                    DepartureTime = DateTime.Now.AddHours(6),
                    FreeSeats = 1,
                    PetsAllowed = true,
                    SmokingAllowed = false,
                    AirConditioner = false,
                    TripStatus = TripStatus.Canceled,
                    isDeleted = false
                },

                  new Trip
                {
                    TripId = 3,
                    DriverId = 5,
                    Driver = mockUsers[4],
                    CarColor = "Yellow",
                    CarModel = "Mini Cooper",
                    StartingPointId = 1,
                    StartingPoint = mockAddresses[0],
                    EndingPointId = 2,
                    EndingPoint = mockAddresses[1],
                    DepartureTime = DateTime.Now.AddHours(1),
                    FreeSeats = 2,
                    PetsAllowed = false,
                    SmokingAllowed = true,
                    AirConditioner = true,
                    TripStatus = TripStatus.Active,
                    isDeleted = false
                },

                   new Trip
                {
                    TripId = 4,
                    DriverId = 4,
                    Driver = mockUsers[3],
                    CarColor ="Silver",
                    CarModel = "Honda",
                    StartingPointId = 2,
                    StartingPoint = mockAddresses[1],
                    EndingPointId = 1,
                    EndingPoint = mockAddresses[0],
                    DepartureTime = DateTime.Now.AddMinutes(-30),
                    FreeSeats = 1,
                    PetsAllowed = true,
                    SmokingAllowed = true,
                    AirConditioner = true,
                    TripStatus = TripStatus.Completed,
                    isDeleted = false
                }
            };

            mockPassengerTrips = new List<PassengerTrip>()
            {
                new PassengerTrip
                {
                    Id = 1,
                    UserId = 3,
                    Passanger = mockUsers[2],
                    TripId = 3,
                    Trip = mockTrips[2],
                    isDeleted = false,
                },

                new PassengerTrip
                {
                    Id = 2,
                    UserId = 2,
                    Passanger = mockUsers[1],
                    TripId = 4,
                    Trip = mockTrips[3],
                    isDeleted = false,
                }
            };

            mockDbCities = mockCities.AsQueryable().BuildMockDbSet();
            mockDbUsers = mockUsers.AsQueryable().BuildMockDbSet();
            mockDbAddresses = mockAddresses.AsQueryable().BuildMockDbSet();
            mockDbTrips = mockTrips.AsQueryable().BuildMockDbSet();
            mockDbRatings = mockRatings.AsQueryable().BuildMockDbSet();
            mockDbPassengerTrips = mockPassengerTrips.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Cities).Returns(mockDbCities.Object);
            mockContext.Setup(db => db.Users).Returns(mockDbUsers.Object);
            mockContext.Setup(db => db.Ratings).Returns(mockDbRatings.Object);
            mockContext.Setup(db => db.Trips).Returns(mockDbTrips.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockDbAddresses.Object);
            mockContext.Setup(db => db.PassangerTrips).Returns(mockDbPassengerTrips.Object);
        }
        [TestMethod]
        public async Task CitiesCount_ReturnsCorrectNumber()
        {
            var service = new HelperService(mockContext.Object);

            //Act
            var result = await service.CitiesCount();

            //Assert

            Assert.AreEqual(2, result);
            Assert.AreNotEqual(3, result);
        }
        [TestMethod]
        public async Task UsersCount_ReturnsCorrectNumber()
        {
            //Arrange
            var service = new HelperService(mockContext.Object);

            //Act
            var result = await service.UsersCount();

            //Assert

            Assert.AreEqual(5, result);
            Assert.AreNotEqual(4, result);
            Assert.AreNotEqual(6, result);
        }
        [TestMethod]
        public async Task PositiveRatingsCount_ReturnsCorrectNumber()
        {
            //Arrange
            var service = new HelperService(mockContext.Object);

            //Act
            var result = await service.PositiveRatingsCount();

            //Assert

            Assert.AreEqual(1, result);
            Assert.AreNotEqual(3, result);
            Assert.AreNotEqual(2, result);
        }
        [TestMethod]
        public async Task TripsCount_ReturnsCorrectNumber()
        {
            //Arrange
            var service = new HelperService(mockContext.Object);

            //Act
            var result = await service.TripsCount();

            //Assert

            Assert.AreEqual(4, result);
            Assert.AreNotEqual(3, result);
            Assert.AreNotEqual(5, result);
        }
    }
}
