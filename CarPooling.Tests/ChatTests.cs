﻿using CarPooling.Data.Database.Contracts;
using CarPooling.Data.Models;
using CarPooling.Service.ResponseModels;
using CarPooling.Service.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Tests
{
    [TestClass]
    public class ChatTests
    {
        [TestMethod]
        public async Task CreateMessage_ReturnsCorrect()
        {

            var password = "123456789";
            var hashedpassword = new PasswordHasher<object?>().HashPassword(null, password);

            //Arrange
            var mockContext = new Mock<IDatabase>();

            var mockUsers = new List<User>
            {
                new User
                {
                    UserId = 1,
                    UserName = "Pesho",
                    FirstName = "Gero",
                    LastName = "Petkov",
                    Email = "gercata@test.com",
                    Role = Role.User,
                    PhoneNumber = "08876222746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 2,
                    UserName = "Terminatora",
                    FirstName = "Asparuh",
                    LastName = "Mladenov",
                    Email = "terminatora@gmail.com",
                    Role = Role.Admin,
                    Password = hashedpassword,
                    PhoneNumber = "08875322746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 3,
                    UserName = "Mosho",
                    FirstName = "Ivailo",
                    LastName = "Vasilov",
                    Email = "ivchopivcho@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08966222746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 4,
                    UserName = "Bosho",
                    FirstName = "Tonka",
                    LastName = "Miteva",
                    Email = "babavitonka@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08876259746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 5,
                    UserName = "Kitka",
                    FirstName = "Krasi",
                    LastName = "Georgiev",
                    Email = "Krasko404@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08876222746",
                    Blocked = false,
                    isDeleted = false
                }
            };

            var mockMessages = new List<Message>
            {
                new Message
                {
                    MessageId = 1,
                    UserId = 3,
                    User = mockUsers[2],
                    Content = "Can you pick me up from the metro station?",
                    When = DateTime.Today.Date,
                    isDeleted = false,
                },

                new Message
                {
                    MessageId = 2,
                    UserId = 1,
                    User = mockUsers[0],
                    Content = "Yeah of course, but at which exit?",
                    When = DateTime.Today.Date,
                    isDeleted = false,
                },
            };

            var mockDbUsers = mockUsers.AsQueryable().BuildMockDbSet();
            var mockDbMessages = mockMessages.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockDbUsers.Object);
            mockContext.Setup(db => db.Messages).Returns(mockDbMessages.Object);

            var service = new ChatService(mockContext.Object);

            var message = new Message
            {
                MessageId = 3,
                UserId = 3,
                User = mockUsers[2],
                Content = "From the side of the McDonalds.",
                When = DateTime.Today.Date,
                isDeleted = false,
            };



            //Act
            var result = await service.CreateMessageAsync(message);

            //Assert
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public async Task GetsAllMessages_ReturnsCorrect()
        {

            var password = "123456789";
            var hashedpassword = new PasswordHasher<object?>().HashPassword(null, password);

            //Arrange
            var mockContext = new Mock<IDatabase>();

            var mockUsers = new List<User>
            {
                new User
                {
                    UserId = 1,
                    UserName = "Pesho",
                    FirstName = "Gero",
                    LastName = "Petkov",
                    Email = "gercata@test.com",
                    Role = Role.User,
                    PhoneNumber = "08876222746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 2,
                    UserName = "Terminatora",
                    FirstName = "Asparuh",
                    LastName = "Mladenov",
                    Email = "terminatora@gmail.com",
                    Role = Role.Admin,
                    Password = hashedpassword,
                    PhoneNumber = "08875322746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 3,
                    UserName = "Mosho",
                    FirstName = "Ivailo",
                    LastName = "Vasilov",
                    Email = "ivchopivcho@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08966222746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 4,
                    UserName = "Bosho",
                    FirstName = "Tonka",
                    LastName = "Miteva",
                    Email = "babavitonka@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08876259746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 5,
                    UserName = "Kitka",
                    FirstName = "Krasi",
                    LastName = "Georgiev",
                    Email = "Krasko404@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08876222746",
                    Blocked = false,
                    isDeleted = false
                }
            };

            var mockMessages = new List<Message>
            {
                new Message
                {
                    MessageId = 1,
                    UserId = 3,
                    User = mockUsers[2],
                    Content = "Can you pick me up from the metro station?",
                    When = DateTime.Today.Date,
                    isDeleted = false,
                },

                new Message
                {
                    MessageId = 2,
                    UserId = 1,
                    User = mockUsers[0],
                    Content = "Yeah of course, but at which exit?",
                    When = DateTime.Today.Date,
                    isDeleted = false,
                },
            };

            var mockDbUsers = mockUsers.AsQueryable().BuildMockDbSet();
            var mockDbMessages = mockMessages.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockDbUsers.Object);
            mockContext.Setup(db => db.Messages).Returns(mockDbMessages.Object);

            var service = new ChatService(mockContext.Object);

            // var expected = mockMessages.ToList().ForEach(x => new MessageResponseModel(x));


            var expected = new List<MessageResponseModel>();

            expected.Add(new MessageResponseModel(mockMessages[0]));
            expected.Add(new MessageResponseModel(mockMessages[1]));

            var result = await service.GetAllMessagesAsync();

            //Assert
            for (int i = 0; i < result.Count(); i++)
            {
                Assert.AreEqual(expected[i].MessageId, result.ToList()[i].MessageId);
                Assert.AreEqual(expected[i].UserId, result.ToList()[i].UserId);
                Assert.AreEqual(expected[i].UserName, result.ToList()[i].UserName);
                Assert.AreEqual(expected[i].ProfilePicture, result.ToList()[i].ProfilePicture);
                Assert.AreEqual(expected[i].Content, result.ToList()[i].Content);
                Assert.AreEqual(expected[i].When, result.ToList()[i].When);
            }
        }
    }
}
