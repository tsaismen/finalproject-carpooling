﻿using CarPooling.Data.Database.Contracts;
using CarPooling.Data.Models;
using CarPooling.Service.Services;
using CarPooling.Service.Tools;
using CarPooling.Services.Exceptions;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockQueryable.Moq;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Tests
{
    [TestClass]
    public class AuthHelperTe
    {
        Mock<IDatabase> mockContext;
        Mock<DbSet<City>> mockDbCities;
        Mock<DbSet<User>> mockDbUsers;
        Mock<DbSet<Address>> mockDbAddresses;
        Mock<DbSet<Trip>> mockDbTrips;
        Mock<DbSet<Rating>> mockDbRatings;
        Mock<DbSet<PassengerTrip>> mockDbPassengerTrips;

        List<User> mockUsers;
        List<Rating> mockRatings;
        List<Address> mockAddresses;
        List<City> mockCities;
        List<Trip> mockTrips;
        List<PassengerTrip> mockPassengerTrips;

        string password = "123456789";
        string hashedpassword = new PasswordHasher<object?>().HashPassword(null, "123456789");
        [TestInitialize]
        public async Task TestInitialize()
        {
            //Arrange
            mockContext = new Mock<IDatabase>();

            mockUsers = new List<User>
            {
                new User
                {
                    UserId = 1,
                    UserName = "Pesho",
                    FirstName = "Gero",
                    LastName = "Petkov",
                    Email = "gercata@test.com",
                    Role = Role.User,
                    PhoneNumber = "08876222746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 2,
                    UserName = "Terminatora",
                    FirstName = "Asparuh",
                    LastName = "Mladenov",
                    Email = "terminatora@gmail.com",
                    Role = Role.Admin,
                    Password = hashedpassword,
                    PhoneNumber = "08875322746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 3,
                    UserName = "Mosho",
                    FirstName = "Ivailo",
                    LastName = "Vasilov",
                    Email = "ivchopivcho@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08966222746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 4,
                    UserName = "Bosho",
                    FirstName = "Tonka",
                    LastName = "Miteva",
                    Email = "babavitonka@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08876259746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 5,
                    UserName = "Kitka",
                    FirstName = "Krasi",
                    LastName = "Georgiev",
                    Email = "Krasko404@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08876222746",
                    Blocked = false,
                    isDeleted = false
                }
            };

            mockRatings = new List<Rating>
            {
                new Rating
                {
                    RatingId = 1,
                    RatingPoints = 1,
                    Comment = "Mnogo ludo karash",
                    RatedUserId = 1,
                    RatedUser = mockUsers[0],
                    RatingUserId = 2,
                    RatingUser = mockUsers[1],
                    isDeleted = false,
                },
                new Rating
                {
                    RatingId = 2,
                    RatingPoints = 4,
                    Comment = "qki janti",
                    RatedUserId = 3,
                    RatedUser = mockUsers[2],
                    RatingUserId = 5,
                    RatingUser = mockUsers[4],
                    isDeleted = false,
                },
            };

            mockDbUsers = mockUsers.AsQueryable().BuildMockDbSet();
            mockDbRatings = mockRatings.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockDbUsers.Object);
            mockContext.Setup(db => db.Ratings).Returns(mockDbRatings.Object);
        }
        [TestMethod]
        public async Task FetchUserByCredentials_ReturnsUser()
        {
            var validator = new Validator(mockContext.Object);
            var userService = new UserService(mockContext.Object, validator);
            var service = new AuthHelper(userService);


            var expected = new User()
            {
                UserId = 2,
                UserName = "Terminatora",
                FirstName = "Asparuh",
                LastName = "Mladenov",
                Email = "terminatora@gmail.com",
                Role = Role.Admin,
                Password = hashedpassword,
                PhoneNumber = "08875322746",
                Blocked = false,
                isDeleted = false
            };

            //Act
            var result = await service.FetchUserByCredentials("terminatora@gmail.com", "123456789");

            //Assert
            Assert.AreEqual(expected.UserId, result.UserId);
            Assert.AreEqual(expected.UserName, result.UserName);
            Assert.AreEqual(expected.FirstName, result.FirstName);
            Assert.AreEqual(expected.LastName, result.LastName);
            Assert.AreEqual(expected.Email, result.Email);
            Assert.AreEqual(expected.Role, result.Role);
            Assert.AreEqual(expected.PhoneNumber, result.PhoneNumber);
            Assert.AreEqual(expected.Blocked, result.Blocked);

        }


        [TestMethod]
        public async Task FetchUserByCredentials_ThrowsException()
        {
            var validator = new Validator(mockContext.Object);
            var userService = new UserService(mockContext.Object, validator);
            var service = new AuthHelper(userService);


            var expected = new User()
            {
                UserId = 2,
                UserName = "Terminatora",
                FirstName = "Asparuh",
                LastName = "Mladenov",
                Email = "terminatora@gmail.com",
                Role = Role.Admin,
                Password = hashedpassword,
                PhoneNumber = "08875322746",
                Blocked = false,
                isDeleted = false
            };

            //Act
            var result = await service.FetchUserByCredentials("terminatora@gmail.com", "123456789");

            //Assert
            //await Assert.ThrowsExceptionAsync<AuthenticationException>(async () => await service.FetchUserByCredentials("terminatora@gmail.com", "12345f789"));
            Assert.AreEqual(null, await service.FetchUserByCredentials("terminatora@gmail.com", "12345f789"));
            Assert.AreEqual(null, await service.FetchUserByCredentials("terminafdsfsdftora@gmail.com", "12345f789"));

        }

        [TestMethod]
        public async Task isAdmin_ReturnsBool()
        {
            //Arrange
            var validator = new Validator(mockContext.Object);
            var userService = new UserService(mockContext.Object, validator);
            var service = new AuthHelper(userService);



            var expected = new User()
            {
                UserId = 2,
                UserName = "Terminatora",
                FirstName = "Asparuh",
                LastName = "Mladenov",
                Email = "terminatora@gmail.com",
                Role = Role.Admin,
                Password = hashedpassword,
                PhoneNumber = "08875322746",
                Blocked = false,
                isDeleted = false
            };

            expected = mockUsers[1];
            //Act
            var result = service.IsAdminCheck(mockUsers[1]);

            //Assert
            Assert.AreEqual(true, result);
            Assert.AreNotEqual(false, result);

            var result2 = service.IsAdminCheck(mockUsers[2]);

            Assert.AreEqual(false, result2);
            Assert.AreNotEqual(true, result2);
        }
    }
}
