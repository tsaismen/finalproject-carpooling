using CarPooling.Data.Database.Contracts;
using CarPooling.Data.Models;
using CarPooling.Service.ResponseModels;
using CarPooling.Service.Services;
using CarPooling.Services.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockQueryable.Moq;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Tests
{
    [TestClass]
    public class AddressTests
    {
        Mock<IDatabase> mockContext;
        Mock<DbSet<City>> mockDbCities;
        Mock<DbSet<User>> mockDbUsers;
        Mock<DbSet<Address>> mockDbAddresses;

        List<User> mockUsers;
        List<Address> mockAddresses;
        List<City> mockCities;
        [TestInitialize]
        public async Task TestInitialize()
        {

            mockContext = new Mock<IDatabase>();

            mockCities = new List<City>
            {
                new City
                {
                    CityId = 1,
                    Name = "Plovdiv"
                },
                new City
                {
                    CityId = 2,
                    Name = "Sofia"
                },

            };
            mockUsers = new List<User>
            {
                new User
                {
                    UserId = 1,
                    UserName = "Pesho"
                },
                new User
                {
                    UserId = 2,
                    UserName = "Gosho"
                }

            };
            mockAddresses = new List<Address>
            {
                new Address
                {
                    AddressId = 1,
                    Street = "Botev 45",
                    CityId = 1,
                    City = mockCities[0],
                    UserUsesId= 2,
                },
                new Address
                {
                    AddressId = 2,
                    Street = "Dundkov 1",
                    CityId = 2,
                    City = mockCities[1],
                    UserUsesId= 1,
                }
            };

            mockDbCities = mockCities.AsQueryable().BuildMockDbSet();
            mockDbUsers = mockUsers.AsQueryable().BuildMockDbSet();
            mockDbAddresses = mockAddresses.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Cities).Returns(mockDbCities.Object);
            mockContext.Setup(db => db.Users).Returns(mockDbUsers.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockDbAddresses.Object);
        }
        [TestMethod]
        public async Task GetsById_ReturnsObject_WhenParametersAreCorrect()
        {
            var service = new AddressService(mockContext.Object);

            var address = new Address()
            {
                AddressId = 1,
                Street = "Botev 45",
                CityId = 1,
                City = mockCities[0],
                UserUsesId = 2
            };

            var result = await service.GetByIdAsync(1);
            var shouldReturn = new AddressResponseModel(address);
            Assert.AreEqual(shouldReturn.AddressId, result.AddressId);
            Assert.AreEqual(shouldReturn.Street, result.Street);
            Assert.AreEqual(shouldReturn.City, result.City);
        }

        [TestMethod]
        public async Task GetsById_ReturnsObject_WhenParametersNotCorrect()
        {
            var service = new AddressService(mockContext.Object);

            var address = new Address()
            {
                AddressId = 3,
                Street = "Bote45",
                CityId = 2,
                City = mockCities[1],
                UserUsesId = 2
            };

            var result = await service.GetByIdAsync(1);
            var shouldReturn = new AddressResponseModel(address);
            Assert.AreNotEqual(shouldReturn.AddressId, result.AddressId);
            Assert.AreNotEqual(shouldReturn.Street, result.Street);
            Assert.AreNotEqual(shouldReturn.City, result.City);

            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.GetByIdAsync(19));
        }

        [TestMethod]
        public async Task GetAll_ReturnsObject_WhenParametersAreCorrect()
        {
            var service = new AddressService(mockContext.Object);

            var addresses = new List<Address>
            {
                new Address
                {
                    AddressId = 1,
                    Street = "Botev 45",
                    CityId = 1,
                    City = mockCities[0],
                    UserUsesId= 2,
                },
                new Address
                {
                    AddressId = 2,
                    Street = "Dundkov 1",
                    CityId = 2,
                    City = mockCities[1],
                    UserUsesId= 1,
                }
            };

            var shouldReturn = addresses.Select(x => new AddressResponseModel(x)).ToList();

            var result = await service.GetAllAsync();

            for (int i = 0; i < 2; i++)
            {
                Assert.AreEqual(shouldReturn[i].AddressId, result.ToList()[i].AddressId);
                Assert.AreEqual(shouldReturn[i].Street, result.ToList()[i].Street);
                Assert.AreEqual(shouldReturn[i].City, result.ToList()[i].City);
            }

        }

        [TestMethod]
        public async Task GetAll_ReturnsObject_WhenParametersNotCorrect()
        {
            var service = new AddressService(mockContext.Object);

            var addresses = new List<Address>
            {
                new Address
                {
                    AddressId = 3,
                    Street = "Bot 45",
                    CityId = 4,
                    City = mockCities[1],
                    UserUsesId= 2,
                },
                new Address
                {
                    AddressId = 4,
                    Street = "Dunov 1",
                    CityId = 3,
                    City = mockCities[0],
                    UserUsesId= 1,
                }
            };

            var shouldReturn = addresses.Select(x => new AddressResponseModel(x)).ToList();

            var result = await service.GetAllAsync();

            for (int i = 0; i < 2; i++)
            {
                Assert.AreNotEqual(shouldReturn[i].AddressId, result.ToList()[i].AddressId);
                Assert.AreNotEqual(shouldReturn[i].Street, result.ToList()[i].Street);
                Assert.AreNotEqual(shouldReturn[i].City, result.ToList()[i].City);
            }

        }

        [TestMethod]
        public async Task CreateParcel_ReturnsObject_WhenParametersAreCorrect()
        {
            var service = new AddressService(mockContext.Object);

            var address = new Address()
            {
                AddressId = 1,
                Street = "Botev 75",
                CityId = 1,
                City = mockCities[0],
                UserUsesId = 2
            };

            var address2 = new Address()
            {
                AddressId = 1,
                Street = "Botev 45",
                CityId = 1,
                City = mockCities[0],
                UserUsesId = 2
            };
            var result = await service.CreateAsync(address);
            await Assert.ThrowsExceptionAsync<DuplicateEntityException>(async () => await service.CreateAsync(address2));
            Assert.AreEqual(address.AddressId, result.AddressId);
            Assert.AreEqual(address.Street, result.Street);
            Assert.AreEqual(address.City, result.City);
        }


        [TestMethod]
        public async Task UpdateParcel_ReturnsObject_WhenParametersAreCorrect()
        {
            var service = new AddressService(mockContext.Object);

            var address = new Address()
            {
                AddressId = 1,
                Street = "Botev45",
                CityId = 2,
                City = mockCities[1],
                UserUsesId = 2
            };

            var shoudReturn = new AddressResponseModel(address);

            var result = await service.UpdateAsync(1, address);
            Assert.AreEqual(shoudReturn.AddressId, result.AddressId);
            Assert.AreEqual(shoudReturn.Street, result.Street);
            Assert.AreEqual(shoudReturn.City, result.City);
        }

        [TestMethod]
        public async Task UpdateParcel_ReturnsObject_WhenParametersNotCorrect()
        {
            var service = new AddressService(mockContext.Object);

            var address = new Address()
            {
                AddressId = 3,
                Street = "Botev45",
                CityId = 2,
                City = mockCities[1],
                UserUsesId = 2
            };

            var shoudReturn = new AddressResponseModel(address);


            var result = await service.UpdateAsync(1, address);

            shoudReturn.AddressId = 6;
            shoudReturn.Street = "Baba pena";
            shoudReturn.City = "Varna";

            Assert.AreNotEqual(shoudReturn.AddressId, result.AddressId);
            Assert.AreNotEqual(shoudReturn.Street, result.Street);
            Assert.AreNotEqual(shoudReturn.City, result.City);

            await Assert.ThrowsExceptionAsync<InvalidInputDataException>(async () => await service.UpdateAsync(1, null));
        }

        [TestMethod]
        public async Task DeleteParcel_ReturnsObject_WhenParametersAreCorrect()
        {
            var service = new AddressService(mockContext.Object);
            Assert.IsTrue(await service.DeleteAsync(1) == true);
        }

        [TestMethod]
        public async Task DeleteParcel_ReturnsObject_WhenParametersNotCorrect()
        {
            var service = new AddressService(mockContext.Object);

            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(() => service.DeleteAsync(8));
        }

        [TestMethod]
        public async Task GetAllCitiesAsync_ReturnsObject_WhenParametersAreCorrect()
        {
            var service = new AddressService(mockContext.Object);

            var address = new Address()
            {
                AddressId = 1,
                Street = "Botev 45",
                CityId = 1,
                City = mockCities[0],
                UserUsesId = 2
            };

            var result = await service.GetAllCitiesAsync();

            for (int i = 0; i < result.Count(); i++)
            {
                Assert.AreEqual(mockCities[i].CityId, result.ToList()[i].CityId);
                Assert.AreEqual(mockCities[i].Name, result.ToList()[i].Name);
                Assert.AreEqual(mockCities[i].Latitude, result.ToList()[i].Latitude);
                Assert.AreEqual(mockCities[i].Longitude, result.ToList()[i].Longitude);
            }
           
        }
    }
}
