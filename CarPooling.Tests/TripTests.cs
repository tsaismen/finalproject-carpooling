﻿using CarPooling.Data.Database.Contracts;
using CarPooling.Data.Models;
using CarPooling.Data.Models.Enums;
using CarPooling.Service.Services;
using CarPooling.Service.Tools;
using CarPooling.Services.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Tests
{
    [TestClass]
    public class TripTests
    {
        Mock<IDatabase> mockContext;
        Mock<DbSet<City>> mockDbCities;
        Mock<DbSet<User>> mockDbUsers;
        Mock<DbSet<Address>> mockDbAddresses;
        Mock<DbSet<Trip>> mockDbTrips;
        Mock<DbSet<Rating>> mockDbRatings;
        Mock<DbSet<PassengerTrip>> mockDbPassengerTrips;

        List<User> mockUsers;
        List<Rating> mockRatings;
        List<Address> mockAddresses;
        List<City> mockCities;
        List<Trip> mockTrips;
        List<PassengerTrip> mockPassengerTrips;
        [TestInitialize]
        public async Task TestInitialize()
        {
            mockContext = new Mock<IDatabase>();
            mockUsers = new List<User>
            {
                new User
                {
                    UserId = 1,
                    UserName = "Pesho",
                    FirstName = "Gero",
                    LastName = "Petkov",
                    Email = "gercata@test.com",
                    Role = Role.User,
                    PhoneNumber = "08876222746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 2,
                    UserName = "Terminatora",
                    FirstName = "Asparuh",
                    LastName = "Mladenov",
                    Email = "terminatora@gmail.com",
                    Role = Role.Admin,
                    PhoneNumber = "08875322746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 3,
                    UserName = "Mosho",
                    FirstName = "Ivailo",
                    LastName = "Vasilov",
                    Email = "ivchopivcho@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08966222746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 4,
                    UserName = "Bosho",
                    FirstName = "Tonka",
                    LastName = "Miteva",
                    Email = "babavitonka@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08876259746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 5,
                    UserName = "Kitka",
                    FirstName = "Krasi",
                    LastName = "Georgiev",
                    Email = "Krasko404@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08876222746",
                    Blocked = false,
                    isDeleted = false

                }

            };

            mockRatings = new List<Rating>
            {

                new Rating
                {
                    RatingId = 1,
                    RatingPoints = 1,
                    Comment = "Mnogo ludo karash",
                    RatedUserId = 1,
                    RatedUser = mockUsers[0],
                    RatingUserId = 2,
                    RatingUser = mockUsers[1],
                    isDeleted = false,
                },
                new Rating
                {
                    RatingId = 2,
                    RatingPoints = 4,
                    Comment = "qki janti",
                    RatedUserId = 3,
                    RatedUser = mockUsers[2],
                    RatingUserId = 5,
                    RatingUser = mockUsers[4],
                    isDeleted = false,
                },

            };

            mockCities = new List<City>
            {
                new City
                {
                    CityId = 1,
                    Name = "Plovdiv"
                },
                new City
                {
                    CityId = 2,
                    Name = "Sofia"
                },

            };

            mockAddresses = new List<Address>
            {
                new Address
                {
                    AddressId = 1,
                    Street = "Botev 45",
                    CityId = 1,
                    City = mockCities[0],
                    UserUsesId= 2,
                    isDeleted = false
                },
                new Address
                {
                    AddressId = 2,
                    Street = "Dundkov 1",
                    CityId = 2,
                    City = mockCities[1],
                    UserUsesId= 1,
                    isDeleted = false
                }
            };

            mockTrips = new List<Trip>
            {
                 new Trip
                {
                    TripId = 1,
                    DriverId = 3,
                    Driver = mockUsers[2],
                    CarColor="Green",
                    CarModel = "Audi r8",
                    StartingPointId = 1,
                   StartingPoint = mockAddresses[0],
                    EndingPointId = 2,
                   EndingPoint = mockAddresses[1],
                    DepartureTime = DateTime.Now.AddHours(3),
                    FreeSeats = 3,
                    PetsAllowed = false,
                    SmokingAllowed = false,
                    AirConditioner = true,
                    TripStatus = TripStatus.Active,
                    isDeleted = false
                },

                 new Trip
                {
                    TripId = 2,
                    DriverId = 1,
                    Driver = mockUsers[0],
                    CarColor = "Blue",
                    CarModel = "Mercedes Benz",
                    StartingPointId = 2,
                    StartingPoint = mockAddresses[1],
                    EndingPointId = 1,
                    EndingPoint = mockAddresses[0],
                    DepartureTime = DateTime.Now.AddHours(6),
                    FreeSeats = 1,
                    PetsAllowed = true,
                    SmokingAllowed = false,
                    AirConditioner = false,
                    TripStatus = TripStatus.Canceled,
                    isDeleted = false
                },

                  new Trip
                {
                    TripId = 3,
                    DriverId = 5,
                    Driver = mockUsers[4],
                    CarColor = "Yellow",
                    CarModel = "Mini Cooper",
                    StartingPointId = 1,
                    StartingPoint = mockAddresses[0],
                    EndingPointId = 2,
                    EndingPoint = mockAddresses[1],
                    DepartureTime = DateTime.Now.AddHours(1),
                    FreeSeats = 2,
                    PetsAllowed = false,
                    SmokingAllowed = true,
                    AirConditioner = true,
                    TripStatus = TripStatus.Active,
                    isDeleted = false
                },

                   new Trip
                {
                    TripId = 4,
                    DriverId = 4,
                    Driver = mockUsers[3],
                    CarColor ="Silver",
                    CarModel = "Honda",
                    StartingPointId = 2,
                    StartingPoint = mockAddresses[1],
                    EndingPointId = 1,
                    EndingPoint = mockAddresses[0],
                    DepartureTime = DateTime.Now.AddMinutes(-30),
                    FreeSeats = 1,
                    PetsAllowed = true,
                    SmokingAllowed = true,
                    AirConditioner = true,
                    TripStatus = TripStatus.Completed,
                    isDeleted = false
                }
            };

            mockPassengerTrips = new List<PassengerTrip>()
            {
                new PassengerTrip
                {
                    Id = 1,
                    UserId = 3,
                    Passanger = mockUsers[2],
                    TripId = 3,
                    Trip = mockTrips[2],
                    isDeleted = false,
                },

                new PassengerTrip
                {
                    Id = 2,
                    UserId = 2,
                    Passanger = mockUsers[1],
                    TripId = 4,
                    Trip = mockTrips[3],
                    isDeleted = false,
                }
            };

            mockDbCities = mockCities.AsQueryable().BuildMockDbSet();
            mockDbUsers = mockUsers.AsQueryable().BuildMockDbSet();
            mockDbAddresses = mockAddresses.AsQueryable().BuildMockDbSet();
            mockDbTrips = mockTrips.AsQueryable().BuildMockDbSet();
            mockDbRatings = mockRatings.AsQueryable().BuildMockDbSet();
            mockDbPassengerTrips = mockPassengerTrips.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Cities).Returns(mockDbCities.Object);
            mockContext.Setup(db => db.Users).Returns(mockDbUsers.Object);
            mockContext.Setup(db => db.Ratings).Returns(mockDbRatings.Object);
            mockContext.Setup(db => db.Trips).Returns(mockDbTrips.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockDbAddresses.Object);
            mockContext.Setup(db => db.PassangerTrips).Returns(mockDbPassengerTrips.Object);
        }
        [TestMethod]
        public async Task GetsById_ReturnsObject_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var userService = new UserService(mockContext.Object, validator);
            var service = new TripService(mockContext.Object, userService);

            var expected = new Trip()
            {
                TripId = 3,
                DriverId = 5,
                Driver = mockUsers[4],
                CarColor = "Yellow",
                CarModel = "Mini Cooper",
                StartingPointId = 1,
                StartingPoint = mockAddresses[0],
                EndingPointId = 2,
                EndingPoint = mockAddresses[1],
                DepartureTime = DateTime.Now.AddHours(1),
                FreeSeats = 2,
                PetsAllowed = false,
                SmokingAllowed = true,
                AirConditioner = true,
                TripStatus = TripStatus.Active,
                isDeleted = false
            };
            //Act
            var result = await service.GetByIdAsync(3);

            //Assert

            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.GetByIdAsync(10));

            Assert.AreEqual(expected.TripId, result.TripId);
            Assert.AreEqual(expected.TripStatus.ToString(), result.TripStatus.ToString());
            Assert.AreEqual(expected.CandidatesForTrip.Count, result.CandidatesForTrip.Count);
            Assert.AreEqual(expected.CarColor, result.CarColor);
            Assert.AreEqual(expected.CarModel, result.CarModel);
            Assert.AreEqual(expected.AirConditioner, result.AirConditioner);
            Assert.AreEqual(expected.Driver, result.Driver);
            Assert.AreEqual(expected.DriverId, result.DriverId);
            Assert.AreEqual(expected.SmokingAllowed, result.SmokingAllowed);
            Assert.AreEqual(expected.PetsAllowed, result.PetsAllowed);
            Assert.AreEqual(expected.StartingPointId, result.StartingPointId);
            Assert.AreEqual(expected.StartingPoint, result.StartingPoint);
            Assert.AreEqual(expected.EndingPointId, result.EndingPointId);
            Assert.AreEqual(expected.EndingPoint, result.EndingPoint);
            Assert.AreEqual(expected.FreeSeats, result.FreeSeats);
            Assert.AreEqual(expected.DepartureTime.Date, result.DepartureTime.Date);
            Assert.AreEqual(expected.DepartureTime.Hour, result.DepartureTime.Hour);
        }
        [TestMethod]
        public async Task GetsAll_ReturnsObject_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var userService = new UserService(mockContext.Object, validator);
            var service = new TripService(mockContext.Object, userService);

            var allActive = new List<Trip>()
            {
                mockTrips[0],
                mockTrips[2]
            };
            var allCompleted = new List<Trip>()
            {
                mockTrips[3],
            };

            //Act
            var result = await service.GetAllAsync();
            var result2 = await service.GetAllActiveAsync();
            var result3 = await service.GetAllCompletedAsync();
            //Assert

            for (int i = 0; i < result.Count(); i++)
            {
                Assert.AreEqual(mockTrips[i].TripId, result.ToList()[i].TripId);
                Assert.AreEqual(mockTrips[i].TripStatus.ToString(), result.ToList()[i].TripStatus.ToString());
                Assert.AreEqual(mockTrips[i].CandidatesForTrip.Count, result.ToList()[i].CandidatesForTrip.Count);
                Assert.AreEqual(mockTrips[i].CarColor, result.ToList()[i].CarColor);
                Assert.AreEqual(mockTrips[i].CarModel, result.ToList()[i].CarModel);
                Assert.AreEqual(mockTrips[i].AirConditioner, result.ToList()[i].AirConditioner);
                Assert.AreEqual(mockTrips[i].DriverId, result.ToList()[i].DriverId);
                Assert.AreEqual(mockTrips[i].SmokingAllowed, result.ToList()[i].SmokingAllowed);
                Assert.AreEqual(mockTrips[i].PetsAllowed, result.ToList()[i].PetsAllowed);
                Assert.AreEqual(mockTrips[i].StartingPoint.ToString(), result.ToList()[i].StartingPoint.ToString());
                Assert.AreEqual(mockTrips[i].EndingPoint.ToString(), result.ToList()[i].EndingPoint.ToString());
                Assert.AreEqual(mockTrips[i].FreeSeats, result.ToList()[i].FreeSeats);
                Assert.AreEqual(mockTrips[i].DepartureTime.Date.ToString("dd/MM/yyyy"), result.ToList()[i].DepartureDate);
                Assert.AreEqual(mockTrips[i].DepartureTime.ToShortTimeString(), result.ToList()[i].DepartureTime);
            }


            for (int i = 0; i < result2.Count(); i++)
            {
                Assert.AreEqual(allActive[i].TripId, result2.ToList()[i].TripId);
                Assert.AreEqual(allActive[i].TripStatus.ToString(), result2.ToList()[i].TripStatus.ToString());
                Assert.AreEqual(allActive[i].CandidatesForTrip.Count, result2.ToList()[i].CandidatesForTrip.Count);
                Assert.AreEqual(allActive[i].CarColor, result2.ToList()[i].CarColor);
                Assert.AreEqual(allActive[i].CarModel, result2.ToList()[i].CarModel);
                Assert.AreEqual(allActive[i].AirConditioner, result2.ToList()[i].AirConditioner);
                Assert.AreEqual(allActive[i].DriverId, result2.ToList()[i].DriverId);
                Assert.AreEqual(allActive[i].SmokingAllowed, result2.ToList()[i].SmokingAllowed);
                Assert.AreEqual(allActive[i].PetsAllowed, result2.ToList()[i].PetsAllowed);
                Assert.AreEqual(allActive[i].StartingPoint.ToString(), result2.ToList()[i].StartingPoint.ToString());
                Assert.AreEqual(allActive[i].EndingPoint.ToString(), result2.ToList()[i].EndingPoint.ToString());
                Assert.AreEqual(allActive[i].FreeSeats, result2.ToList()[i].FreeSeats);
                Assert.AreEqual(allActive[i].DepartureTime.Date.ToString("dd/MM/yyyy"), result2.ToList()[i].DepartureDate);
                Assert.AreEqual(allActive[i].DepartureTime.ToShortTimeString(), result2.ToList()[i].DepartureTime);
            }

            for (int i = 0; i < result3.Count(); i++)
            {
                Assert.AreEqual(allCompleted[i].TripId, result3.ToList()[i].TripId);
                Assert.AreEqual(allCompleted[i].TripStatus.ToString(), result3.ToList()[i].TripStatus.ToString());
                Assert.AreEqual(allCompleted[i].CandidatesForTrip.Count, result3.ToList()[i].CandidatesForTrip.Count);
                Assert.AreEqual(allCompleted[i].CarColor, result3.ToList()[i].CarColor);
                Assert.AreEqual(allCompleted[i].CarModel, result3.ToList()[i].CarModel);
                Assert.AreEqual(allCompleted[i].AirConditioner, result3.ToList()[i].AirConditioner);
                Assert.AreEqual(allCompleted[i].DriverId, result3.ToList()[i].DriverId);
                Assert.AreEqual(allCompleted[i].SmokingAllowed, result3.ToList()[i].SmokingAllowed);
                Assert.AreEqual(allCompleted[i].PetsAllowed, result3.ToList()[i].PetsAllowed);
                Assert.AreEqual(allCompleted[i].StartingPoint.ToString(), result3.ToList()[i].StartingPoint.ToString());
                Assert.AreEqual(allCompleted[i].EndingPoint.ToString(), result3.ToList()[i].EndingPoint.ToString());
                Assert.AreEqual(allCompleted[i].FreeSeats, result3.ToList()[i].FreeSeats);
                Assert.AreEqual(allCompleted[i].DepartureTime.Date.ToString("dd/MM/yyyy"), result3.ToList()[i].DepartureDate);
                Assert.AreEqual(allCompleted[i].DepartureTime.ToShortTimeString(), result3.ToList()[i].DepartureTime);
            }
        }
        [TestMethod]
        public async Task Create_ReturnsObject_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var userService = new UserService(mockContext.Object, validator);
            var service = new TripService(mockContext.Object, userService);

            var expected = new Trip()
            {
                TripId = 3,
                DriverId = 5,
                Driver = mockUsers[4],
                CarColor = "Yellow",
                CarModel = "Mini Cooper",
                StartingPointId = 1,
                StartingPoint = mockAddresses[0],
                EndingPointId = 2,
                EndingPoint = mockAddresses[1],
                DepartureTime = DateTime.Now.AddHours(1),
                FreeSeats = 2,
                PetsAllowed = false,
                SmokingAllowed = true,
                AirConditioner = true,
                TripStatus = TripStatus.Active,
                isDeleted = false
            };
            //Act
            var result = await service.CreateAsync(expected);

            //Assert

            Assert.AreEqual(expected.TripId, result.TripId);
            Assert.AreEqual(expected.TripStatus.ToString(), result.TripStatus.ToString());
            Assert.AreEqual(expected.CandidatesForTrip.Count, result.CandidatesForTrip.Count);
            Assert.AreEqual(expected.CarColor, result.CarColor);
            Assert.AreEqual(expected.CarModel, result.CarModel);
            Assert.AreEqual(expected.AirConditioner, result.AirConditioner);
            Assert.AreEqual(expected.Driver, result.Driver);
            Assert.AreEqual(expected.DriverId, result.DriverId);
            Assert.AreEqual(expected.SmokingAllowed, result.SmokingAllowed);
            Assert.AreEqual(expected.PetsAllowed, result.PetsAllowed);
            Assert.AreEqual(expected.StartingPointId, result.StartingPointId);
            Assert.AreEqual(expected.StartingPoint, result.StartingPoint);
            Assert.AreEqual(expected.EndingPointId, result.EndingPointId);
            Assert.AreEqual(expected.EndingPoint, result.EndingPoint);
            Assert.AreEqual(expected.FreeSeats, result.FreeSeats);
            Assert.AreEqual(expected.DepartureTime.Date, result.DepartureTime.Date);
            Assert.AreEqual(expected.DepartureTime.Hour, result.DepartureTime.Hour);
        }
        [TestMethod]
        public async Task Update_ReturnsObject_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var userService = new UserService(mockContext.Object, validator);
            var service = new TripService(mockContext.Object, userService);

            var expected = new Trip()
            {
                TripId = 3,
                DriverId = 5,
                Driver = mockUsers[4],
                CarColor = "Yellow",
                CarModel = "Mini Cooper",
                StartingPointId = 1,
                StartingPoint = mockAddresses[0],
                EndingPointId = 2,
                EndingPoint = mockAddresses[1],
                DepartureTime = DateTime.Now.AddHours(3),
                FreeSeats = 2,
                PetsAllowed = true,
                SmokingAllowed = true,
                AirConditioner = false,
                TripStatus = TripStatus.Canceled,
                isDeleted = false
            };
            //Act
            var result = await service.UpdateAsync(3, expected);

            //Assert

            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.UpdateAsync(90, expected));

            Assert.AreEqual(expected.TripStatus.ToString(), result.TripStatus.ToString());
            Assert.AreEqual(expected.CarColor, result.CarColor);
            Assert.AreEqual(expected.CarModel, result.CarModel);
            Assert.AreEqual(expected.AirConditioner, result.AirConditioner);
            Assert.AreEqual(expected.SmokingAllowed, result.SmokingAllowed);
            Assert.AreEqual(expected.PetsAllowed, result.PetsAllowed);
            Assert.AreEqual(expected.StartingPointId, result.StartingPointId);
            Assert.AreEqual(expected.EndingPointId, result.EndingPointId);
            Assert.AreEqual(expected.DepartureTime.Date, result.DepartureTime.Date);
            Assert.AreEqual(expected.DepartureTime.Hour, result.DepartureTime.Hour);
        }
        [TestMethod]
        public async Task Delete_ReturnsObject_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var userService = new UserService(mockContext.Object, validator);
            var service = new TripService(mockContext.Object, userService);

            var expected = new Trip()
            {
                TripId = 3,
                DriverId = 5,
                Driver = mockUsers[4],
                CarColor = "Yellow",
                CarModel = "Mini Cooper",
                StartingPointId = 1,
                StartingPoint = mockAddresses[0],
                EndingPointId = 2,
                EndingPoint = mockAddresses[1],
                DepartureTime = DateTime.Now.AddHours(1),
                FreeSeats = 2,
                PetsAllowed = false,
                SmokingAllowed = true,
                AirConditioner = true,
                TripStatus = TripStatus.Active,
                isDeleted = false
            };
            //Act
            var result = await service.DeleteAsync(3);

            //Assert

            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.DeleteAsync(10));

            Assert.AreEqual(true, result);

        }
        [TestMethod]
        public async Task ChangeTripStatus_ReturnsObject_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var userService = new UserService(mockContext.Object, validator);
            var service = new TripService(mockContext.Object, userService);

            var expected = new Trip()
            {
                TripId = 3,
                DriverId = 5,
                Driver = mockUsers[4],
                CarColor = "Yellow",
                CarModel = "Mini Cooper",
                StartingPointId = 1,
                StartingPoint = mockAddresses[0],
                EndingPointId = 2,
                EndingPoint = mockAddresses[1],
                DepartureTime = DateTime.Now.AddHours(1),
                FreeSeats = 2,
                PetsAllowed = false,
                SmokingAllowed = true,
                AirConditioner = true,
                TripStatus = TripStatus.Active,
                isDeleted = false
            };
            //Act
            var result = await service.ChangeTripStatus(3, TripStatus.Canceled.ToString());

            //Assert

            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.ChangeTripStatus(9, TripStatus.Canceled.ToString()));
            await Assert.ThrowsExceptionAsync<InvalidInputDataException>(async () => await service.ChangeTripStatus(3, "fdsfsdfsd"));

            Assert.AreEqual(expected.TripId, result.TripId);
            Assert.AreEqual(TripStatus.Canceled.ToString(), result.TripStatus.ToString());
            Assert.AreEqual(expected.CandidatesForTrip.Count, result.CandidatesForTrip.Count);
            Assert.AreEqual(expected.CarColor, result.CarColor);
            Assert.AreEqual(expected.CarModel, result.CarModel);
            Assert.AreEqual(expected.AirConditioner, result.AirConditioner);
            Assert.AreEqual(expected.DriverId, result.DriverId);
            Assert.AreEqual(expected.SmokingAllowed, result.SmokingAllowed);
            Assert.AreEqual(expected.PetsAllowed, result.PetsAllowed);
            Assert.AreEqual(expected.StartingPoint.ToString(), result.StartingPoint.ToString());
            Assert.AreEqual(expected.EndingPoint.ToString(), result.EndingPoint.ToString());
            Assert.AreEqual(expected.FreeSeats, result.FreeSeats);

            var result2 = await service.ChangeTripStatus(3, TripStatus.Completed.ToString());

            Assert.AreEqual(TripStatus.Completed.ToString(), result2.TripStatus.ToString());

        }
        [TestMethod]
        public async Task GetTripsByPassengerId_ReturnsObject_WhenParametersAreCorrect()
        {
            //Arrange
            var validator = new Validator(mockContext.Object);
            var userService = new UserService(mockContext.Object, validator);
            var service = new TripService(mockContext.Object, userService);

            var expected = new Trip()
            {
                TripId = 3,
                DriverId = 5,
                Driver = mockUsers[4],
                CarColor = "Yellow",
                CarModel = "Mini Cooper",
                StartingPointId = 1,
                StartingPoint = mockAddresses[0],
                EndingPointId = 2,
                EndingPoint = mockAddresses[1],
                DepartureTime = DateTime.Now.AddHours(1),
                FreeSeats = 2,
                PetsAllowed = false,
                SmokingAllowed = true,
                AirConditioner = true,
                TripStatus = TripStatus.Active,
                isDeleted = false
            };

            var passengerTrip = new PassengerTrip()
            {
                Id = 1,
                UserId = 3,
                Passanger = mockUsers[2],
                TripId = 3,
                Trip = mockTrips[2],
                isDeleted = false,
            };
            mockTrips[2].Passangers.Add(passengerTrip);

            mockUsers[0].TripsAsPassanger.Add(passengerTrip);
            //Act
            var result = await service.GetTripsByPassangerId(3);

            //Assert

            //await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.GetTripsByPassangerId(10));

            for (int i = 0; i < result.Count(); i++)
            {
                Assert.AreEqual(expected.TripId, result.ToList()[i].TripId);
                Assert.AreEqual(expected.TripStatus.ToString(), result.ToList()[i].TripStatus.ToString());
                Assert.AreEqual(expected.CandidatesForTrip.Count, result.ToList()[i].CandidatesForTrip.Count);
                Assert.AreEqual(expected.CarColor, result.ToList()[i].CarColor);
                Assert.AreEqual(expected.CarModel, result.ToList()[i].CarModel);
                Assert.AreEqual(expected.AirConditioner, result.ToList()[i].AirConditioner);
                Assert.AreEqual(expected.DriverId, result.ToList()[i].DriverId);
                Assert.AreEqual(expected.SmokingAllowed, result.ToList()[i].SmokingAllowed);
                Assert.AreEqual(expected.PetsAllowed, result.ToList()[i].PetsAllowed);
                Assert.AreEqual(expected.StartingPoint.ToString(), result.ToList()[i].StartingPoint.ToString());
                Assert.AreEqual(expected.EndingPoint.ToString(), result.ToList()[i].EndingPoint.ToString());
                Assert.AreEqual(expected.FreeSeats, result.ToList()[i].FreeSeats);
                Assert.AreEqual(expected.DepartureTime.Date.ToString("dd/MM/yyyy"), result.ToList()[i].DepartureDate);
                Assert.AreEqual(expected.DepartureTime.ToString("HH:mm"), result.ToList()[i].DepartureTime);
            }
        }//
        [TestMethod]
        public async Task GetTripsByDriverId_ReturnsObject_WhenParametersAreCorrect()
        {
            //Arrange
            var validator = new Validator(mockContext.Object);
            var userService = new UserService(mockContext.Object, validator);
            var service = new TripService(mockContext.Object, userService);

            var expected = new Trip()
            {
                TripId = 1,
                DriverId = 3,
                Driver = mockUsers[2],
                CarColor = "Green",
                CarModel = "Audi r8",
                StartingPointId = 1,
                StartingPoint = mockAddresses[0],
                EndingPointId = 2,
                EndingPoint = mockAddresses[1],
                DepartureTime = DateTime.Now.AddHours(3),
                FreeSeats = 3,
                PetsAllowed = false,
                SmokingAllowed = false,
                AirConditioner = true,
                TripStatus = TripStatus.Active,
                isDeleted = false
            };

            mockUsers[2].TripsAsDriver.Add(mockTrips[0]);
            //Act
            var result = await service.GetTripsByDriverId(3);

            //Assert

            //await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.GetTripsByDriverId(10));

            for (int i = 0; i < result.Count(); i++)
            {
                Assert.AreEqual(expected.TripId, result.ToList()[i].TripId);
                Assert.AreEqual(expected.TripStatus.ToString(), result.ToList()[i].TripStatus.ToString());
                Assert.AreEqual(expected.CandidatesForTrip.Count, result.ToList()[i].CandidatesForTrip.Count);
                Assert.AreEqual(expected.CarColor, result.ToList()[i].CarColor);
                Assert.AreEqual(expected.CarModel, result.ToList()[i].CarModel);
                Assert.AreEqual(expected.AirConditioner, result.ToList()[i].AirConditioner);
                Assert.AreEqual(expected.DriverId, result.ToList()[i].DriverId);
                Assert.AreEqual(expected.SmokingAllowed, result.ToList()[i].SmokingAllowed);
                Assert.AreEqual(expected.PetsAllowed, result.ToList()[i].PetsAllowed);
                Assert.AreEqual(expected.StartingPoint.ToString(), result.ToList()[i].StartingPoint.ToString());
                Assert.AreEqual(expected.EndingPoint.ToString(), result.ToList()[i].EndingPoint.ToString());
                Assert.AreEqual(expected.FreeSeats, result.ToList()[i].FreeSeats);
                Assert.AreEqual(expected.DepartureTime.Date.ToString("dd/MM/yyyy"), result.ToList()[i].DepartureDate);
                Assert.AreEqual(expected.DepartureTime.ToString("HH:mm"), result.ToList()[i].DepartureTime);
            }
        }//
        [TestMethod]
        public async Task ApproveCandidates_ReturnsObject_WhenParametersAreCorrect()
        {
            //Arrange
            var validator = new Validator(mockContext.Object);
            var userService = new UserService(mockContext.Object, validator);
            var service = new TripService(mockContext.Object, userService);


            mockTrips[2].CandidatesForTrip.Add(mockUsers[1]);

            //Act
            var result = await service.ApproveCandidatesAsync(3, 2);

            //Assert

            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.ApproveCandidatesAsync(9, 2));
            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.ApproveCandidatesAsync(3, 18));

            Assert.AreEqual(mockTrips[2].TripId, result.TripId);
            Assert.AreEqual(mockTrips[2].TripStatus.ToString(), result.TripStatus);
            Assert.AreEqual(mockTrips[2].CandidatesForTrip.Count, result.CandidatesForTrip.Count);
            Assert.AreEqual(mockTrips[2].CarColor, result.CarColor);
            Assert.AreEqual(mockTrips[2].CarModel, result.CarModel);
            Assert.AreEqual(mockTrips[2].AirConditioner, result.AirConditioner);
            Assert.AreEqual(mockTrips[2].DriverId, result.DriverId);
            Assert.AreEqual(mockTrips[2].SmokingAllowed, result.SmokingAllowed);
            Assert.AreEqual(mockTrips[2].PetsAllowed, result.PetsAllowed);
            Assert.AreEqual(mockTrips[2].StartingPoint.ToString(), result.StartingPoint.ToString());
            Assert.AreEqual(mockTrips[2].EndingPoint.ToString(), result.EndingPoint.ToString());
            Assert.AreEqual(mockTrips[2].FreeSeats, result.FreeSeats);


            mockTrips[2].FreeSeats = 0;
            await Assert.ThrowsExceptionAsync<InvalidUserOperationException>(async () => await service.ApproveCandidatesAsync(3, 2));
        }
        [TestMethod]
        public async Task DeclineCandidates_ReturnsObject_WhenParametersAreCorrect()
        {
            //Arrange
            var validator = new Validator(mockContext.Object);
            var userService = new UserService(mockContext.Object, validator);
            var service = new TripService(mockContext.Object, userService);

            mockTrips[3].CandidatesForTrip.Add(mockUsers[1]);

            //Act
            var result = await service.DeclineCandidatesAsync(4, 2);

            //Assert

            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.DeclineCandidatesAsync(89, 2));
            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.DeclineCandidatesAsync(4, 31));

            Assert.AreEqual(0, mockTrips[3].CandidatesForTrip.Count);
            Assert.AreEqual(mockTrips[3].TripId, result.TripId);
            Assert.AreEqual(mockTrips[3].TripStatus.ToString(), result.TripStatus.ToString());
            Assert.AreEqual(mockTrips[3].CandidatesForTrip.Count, result.CandidatesForTrip.Count);
            Assert.AreEqual(mockTrips[3].CarColor, result.CarColor);
            Assert.AreEqual(mockTrips[3].CarModel, result.CarModel);
            Assert.AreEqual(mockTrips[3].AirConditioner, result.AirConditioner);
            Assert.AreEqual(mockTrips[3].DriverId, result.DriverId);
            Assert.AreEqual(mockTrips[3].SmokingAllowed, result.SmokingAllowed);
            Assert.AreEqual(mockTrips[3].PetsAllowed, result.PetsAllowed);
            Assert.AreEqual(mockTrips[3].StartingPoint.ToString(), result.StartingPoint.ToString());
            Assert.AreEqual(mockTrips[3].EndingPoint.ToString(), result.EndingPoint.ToString());
            Assert.AreEqual(mockTrips[3].FreeSeats, result.FreeSeats);
        }
        [TestMethod]
        public async Task RemovePassanger_ReturnsObject_WhenParametersAreCorrect()
        {
            //Arrange
            var validator = new Validator(mockContext.Object);
            var userService = new UserService(mockContext.Object, validator);
            var service = new TripService(mockContext.Object, userService);

            var expected = new Trip()
            {
                TripId = 3,
                DriverId = 5,
                Driver = mockUsers[4],
                CarColor = "Yellow",
                CarModel = "Mini Cooper",
                StartingPointId = 1,
                StartingPoint = mockAddresses[0],
                EndingPointId = 2,
                EndingPoint = mockAddresses[1],
                DepartureTime = DateTime.Now.AddHours(1),
                FreeSeats = 2,
                PetsAllowed = false,
                SmokingAllowed = true,
                AirConditioner = true,
                TripStatus = TripStatus.Active,
                isDeleted = false
            };


            var passengerTrip = new PassengerTrip()
            {
                Id = 1,
                UserId = 1,
                Passanger = mockUsers[0],
                TripId = 3,
                Trip = mockTrips[2],
                isDeleted = false,
            };

            mockTrips[2].Passangers.Add(passengerTrip);

            mockUsers[0].TripsAsPassanger.Add(passengerTrip);
            //Act
            var result = await service.RemovePassanger(3, 1);

            //Assert

            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.RemovePassanger(10, 1));
            //await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.RemovePassanger(3, 20));
            await Assert.ThrowsExceptionAsync<InvalidUserOperationException>(async () => await service.RemovePassanger(4, 1));

            Assert.AreEqual(0, result.Passangers.Count);

            Assert.AreEqual(expected.TripId, result.TripId);
            Assert.AreEqual(expected.TripStatus.ToString(), result.TripStatus.ToString());
            Assert.AreEqual(expected.CandidatesForTrip.Count, result.CandidatesForTrip.Count);
            Assert.AreEqual(expected.CarColor, result.CarColor);
            Assert.AreEqual(expected.CarModel, result.CarModel);
            Assert.AreEqual(expected.AirConditioner, result.AirConditioner);
            Assert.AreEqual(expected.DriverId, result.DriverId);
            Assert.AreEqual(expected.SmokingAllowed, result.SmokingAllowed);
            Assert.AreEqual(expected.PetsAllowed, result.PetsAllowed);
            Assert.AreEqual(expected.StartingPoint.ToString(), result.StartingPoint.ToString());
            Assert.AreEqual(expected.EndingPoint.ToString(), result.EndingPoint.ToString());
            Assert.AreEqual(3, result.FreeSeats);
            Assert.AreEqual(expected.DepartureTime.Date.ToString("dd/MM/yyyy"), result.DepartureDate);
            Assert.AreEqual(expected.DepartureTime.ToString("HH:mm"), result.DepartureTime);
        }
    }
}
