﻿using CarPooling.Data.Database.Contracts;
using CarPooling.Data.Models;
using CarPooling.Data.Models.Enums;
using CarPooling.Service.Services;
using CarPooling.Service.Tools;
using CarPooling.Services.Exceptions;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Tests
{
    [TestClass]
    public class UserTests
    {
        Mock<IDatabase> mockContext;
        Mock<DbSet<City>> mockDbCities;
        Mock<DbSet<User>> mockDbUsers;
        Mock<DbSet<Address>> mockDbAddresses;
        Mock<DbSet<Trip>> mockDbTrips;
        Mock<DbSet<Rating>> mockDbRatings;
        Mock<DbSet<PassengerTrip>> mockDbPassengerTrips;

        List<User> mockUsers;
        List<Rating> mockRatings;
        List<Address> mockAddresses;
        List<City> mockCities;
        List<Trip> mockTrips;
        List<PassengerTrip> mockPassengerTrips;
        [TestInitialize]
        public async Task TestInitialize()
        {
            var password = "123456789";
            var hashedpassword = new PasswordHasher<object?>().HashPassword(null, password);
            mockContext = new Mock<IDatabase>();
            mockUsers = new List<User>
            {
                new User
                {
                    UserId = 1,
                    UserName = "Pesho",
                    FirstName = "Gero",
                    LastName = "Petkov",
                    Email = "gercata@test.com",
                    Role = Role.User,
                    PhoneNumber = "08876222746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 2,
                    UserName = "Terminatora",
                    FirstName = "Asparuh",
                    LastName = "Mladenov",
                    Email = "terminatora@gmail.com",
                    Role = Role.Admin,
                    PhoneNumber = "08875322746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 3,
                    UserName = "Mosho",
                    FirstName = "Ivailo",
                    LastName = "Vasilov",
                    Email = "ivchopivcho@gmail.com",
                    Password=hashedpassword,
                    Role = Role.User,
                    PhoneNumber = "08966222746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 4,
                    UserName = "Bosho",
                    FirstName = "Tonka",
                    LastName = "Miteva",
                    Email = "babavitonka@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08876259746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 5,
                    UserName = "Kitka",
                    FirstName = "Krasi",
                    LastName = "Georgiev",
                    Email = "Krasko404@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08876222746",
                    Blocked = false,
                    isDeleted = false

                }

            };

            mockRatings = new List<Rating>
            {

                new Rating
                {
                    RatingId = 1,
                    RatingPoints = 1,
                    Comment = "Mnogo ludo karash",
                    RatedUserId = 1,
                    RatedUser = mockUsers[0],
                    RatingUserId = 2,
                    RatingUser = mockUsers[1],
                    isDeleted = false,
                },
                new Rating
                {
                    RatingId = 2,
                    RatingPoints = 4,
                    Comment = "qki janti",
                    RatedUserId = 3,
                    RatedUser = mockUsers[2],
                    RatingUserId = 5,
                    RatingUser = mockUsers[4],
                    isDeleted = false,
                },

            };

            mockCities = new List<City>
            {
                new City
                {
                    CityId = 1,
                    Name = "Plovdiv"
                },
                new City
                {
                    CityId = 2,
                    Name = "Sofia"
                },

            };

            mockAddresses = new List<Address>
            {
                new Address
                {
                    AddressId = 1,
                    Street = "Botev 45",
                    CityId = 1,
                    City = mockCities[0],
                    UserUsesId= 2,
                    isDeleted = false
                },
                new Address
                {
                    AddressId = 2,
                    Street = "Dundkov 1",
                    CityId = 2,
                    City = mockCities[1],
                    UserUsesId= 1,
                    isDeleted = false
                }
            };

            mockTrips = new List<Trip>
            {
                 new Trip
                {
                    TripId = 1,
                    DriverId = 3,
                    Driver = mockUsers[2],
                    CarColor="Green",
                    CarModel = "Audi r8",
                    StartingPointId = 1,
                   StartingPoint = mockAddresses[0],
                    EndingPointId = 2,
                   EndingPoint = mockAddresses[1],
                    DepartureTime = DateTime.Now.AddHours(3),
                    FreeSeats = 3,
                    PetsAllowed = false,
                    SmokingAllowed = false,
                    AirConditioner = true,
                    TripStatus = TripStatus.Active,
                    isDeleted = false
                },

                 new Trip
                {
                    TripId = 2,
                    DriverId = 1,
                    Driver = mockUsers[0],
                    CarColor = "Blue",
                    CarModel = "Mercedes Benz",
                    StartingPointId = 2,
                    StartingPoint = mockAddresses[1],
                    EndingPointId = 1,
                    EndingPoint = mockAddresses[0],
                    DepartureTime = DateTime.Now.AddHours(6),
                    FreeSeats = 1,
                    PetsAllowed = true,
                    SmokingAllowed = false,
                    AirConditioner = false,
                    TripStatus = TripStatus.Canceled,
                    isDeleted = false
                },

                  new Trip
                {
                    TripId = 3,
                    DriverId = 5,
                    Driver = mockUsers[4],
                    CarColor = "Yellow",
                    CarModel = "Mini Cooper",
                    StartingPointId = 1,
                    StartingPoint = mockAddresses[0],
                    EndingPointId = 2,
                    EndingPoint = mockAddresses[1],
                    DepartureTime = DateTime.Now.AddHours(1),
                    FreeSeats = 2,
                    PetsAllowed = false,
                    SmokingAllowed = true,
                    AirConditioner = true,
                    TripStatus = TripStatus.Active,
                    isDeleted = false
                },

                   new Trip
                {
                    TripId = 4,
                    DriverId = 4,
                    Driver = mockUsers[3],
                    CarColor ="Silver",
                    CarModel = "Honda",
                    StartingPointId = 2,
                    StartingPoint = mockAddresses[1],
                    EndingPointId = 1,
                    EndingPoint = mockAddresses[0],
                    DepartureTime = DateTime.Now.AddMinutes(-30),
                    FreeSeats = 1,
                    PetsAllowed = true,
                    SmokingAllowed = true,
                    AirConditioner = true,
                    TripStatus = TripStatus.Completed,
                    isDeleted = false
                }
            };

            mockPassengerTrips = new List<PassengerTrip>()
            {
                new PassengerTrip
                {
                    Id = 1,
                    UserId = 3,
                    Passanger = mockUsers[2],
                    TripId = 3,
                    Trip = mockTrips[2],
                    isDeleted = false,
                },

                new PassengerTrip
                {
                    Id = 2,
                    UserId = 2,
                    Passanger = mockUsers[1],
                    TripId = 4,
                    Trip = mockTrips[3],
                    isDeleted = false,
                }
            };

            mockDbCities = mockCities.AsQueryable().BuildMockDbSet();
            mockDbUsers = mockUsers.AsQueryable().BuildMockDbSet();
            mockDbAddresses = mockAddresses.AsQueryable().BuildMockDbSet();
            mockDbTrips = mockTrips.AsQueryable().BuildMockDbSet();
            mockDbRatings = mockRatings.AsQueryable().BuildMockDbSet();
            mockDbPassengerTrips = mockPassengerTrips.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Cities).Returns(mockDbCities.Object);
            mockContext.Setup(db => db.Users).Returns(mockDbUsers.Object);
            mockContext.Setup(db => db.Ratings).Returns(mockDbRatings.Object);
            mockContext.Setup(db => db.Trips).Returns(mockDbTrips.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockDbAddresses.Object);
            mockContext.Setup(db => db.PassangerTrips).Returns(mockDbPassengerTrips.Object);
        }

        [TestMethod]
        public async Task GetsById_ReturnsObject_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var service = new UserService(mockContext.Object, validator);

            var expected = new User()
            {
                UserId = 3,
                UserName = "Mosho",
                FirstName = "Ivailo",
                LastName = "Vasilov",
                Email = "ivchopivcho@gmail.com",
                Role = Role.User,
                PhoneNumber = "08966222746",
                Blocked = false,
                isDeleted = false
            };

            //Act
            var result = await service.GetByIdAsync(3);

            //Assert
            Assert.AreEqual(expected.UserId, result.UserId);
            Assert.AreEqual(expected.UserName, result.UserName);
            Assert.AreEqual(expected.FirstName, result.FirstName);
            Assert.AreEqual(expected.LastName, result.LastName);
            Assert.AreEqual(expected.Email, result.Email);
            Assert.AreEqual(expected.Role, result.Role);
            Assert.AreEqual(expected.PhoneNumber, result.PhoneNumber);
            Assert.AreEqual(expected.Blocked, result.Blocked);
        }
        [TestMethod]
        public async Task GetsById_ReturnsObject_WhenParametersNotCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var service = new UserService(mockContext.Object, validator);

            var expected = new User()
            {
                UserId = 8,
                UserName = "Mofsho",
                FirstName = "Ivfailo",
                LastName = "Vasfilov",
                Email = "ivchofpivcdho@gmail.com",
                Role = Role.Admin,
                PhoneNumber = "0896d6222746",
                Blocked = false,
                isDeleted = false
            };

            //Act
            var result = await service.GetByIdAsync(3);

            //Assert

            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.GetByIdAsync(10));

            Assert.AreNotEqual(expected.UserId, result.UserId);
            Assert.AreNotEqual(expected.UserName, result.UserName);
            Assert.AreNotEqual(expected.FirstName, result.FirstName);
            Assert.AreNotEqual(expected.LastName, result.LastName);
            Assert.AreNotEqual(expected.Email, result.Email);
            Assert.AreNotEqual(expected.Role, result.Role);
            Assert.AreNotEqual(expected.PhoneNumber, result.PhoneNumber);
        }
        [TestMethod]
        public async Task GetsАll_ReturnsObject_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var service = new UserService(mockContext.Object, validator);

            //Act
            var result = await service.GetAllAsync();

            //Assert
            for (int i = 0; i < result.ToList().Count; i++)
            {
                Assert.AreEqual(mockUsers[i].UserId, result.ToList()[i].UserId);
                Assert.AreEqual(mockUsers[i].FirstName, result.ToList()[i].FirstName);
                Assert.AreEqual(mockUsers[i].LastName, result.ToList()[i].LastName);
                Assert.AreEqual(mockUsers[i].Email, result.ToList()[i].Email);
                Assert.AreEqual(mockUsers[i].Role.ToString(), result.ToList()[i].Role);
                Assert.AreEqual(mockUsers[i].PhoneNumber, result.ToList()[i].PhoneNumber);
                Assert.AreEqual(mockUsers[i].Blocked, result.ToList()[i].Blocked);
            }

        }
        [TestMethod]
        public async Task GetsByEmail_ReturnsObject_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var service = new UserService(mockContext.Object, validator);


            var expected = new User()
            {
                UserId = 1,
                UserName = "Pesho",
                FirstName = "Gero",
                LastName = "Petkov",
                Email = "gercata@test.com",
                Role = Role.User,
                PhoneNumber = "08876222746",
                Blocked = false,
                isDeleted = false
            };
            //Act
            var result = await service.GetByEmailAsync("test");

            //Assert

            //Assert.ThrowsException<EntityNotFoundException>(async () => await service.GetByEmailAsync("petrohani987696ca"));

            Assert.AreEqual(expected.UserId, result.ToList()[0].UserId);
            Assert.AreEqual(expected.FirstName, result.ToList()[0].FirstName);
            Assert.AreEqual(expected.LastName, result.ToList()[0].LastName);
            Assert.AreEqual(expected.Email, result.ToList()[0].Email);
            Assert.AreEqual(expected.Role.ToString(), result.ToList()[0].Role);
            Assert.AreEqual(expected.PhoneNumber, result.ToList()[0].PhoneNumber);
            Assert.AreEqual(expected.Blocked, result.ToList()[0].Blocked);
        }
        [TestMethod]
        public async Task GetsSingle_ReturnsObject_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var service = new UserService(mockContext.Object, validator);


            var expected = new User()
            {
                UserId = 1,
                UserName = "Pesho",
                FirstName = "Gero",
                LastName = "Petkov",
                Email = "gercata@test.com",
                Role = Role.User,
                PhoneNumber = "08876222746",
                Blocked = false,
                isDeleted = false
            };
            //Act
            var result = await service.GetSingleUserByEmailAsync("gercata@test.com");

            //Assert

            await Assert.ThrowsExceptionAsync<AuthenticationException>(async () => await service.GetSingleUserByEmailAsync("test"));

            Assert.AreEqual(expected.UserId, result.UserId);
            Assert.AreEqual(expected.UserName, result.UserName);
            Assert.AreEqual(expected.FirstName, result.FirstName);
            Assert.AreEqual(expected.LastName, result.LastName);
            Assert.AreEqual(expected.Email, result.Email);
            Assert.AreEqual(expected.Role.ToString(), result.Role.ToString());
            Assert.AreEqual(expected.PhoneNumber, result.PhoneNumber);
            Assert.AreEqual(expected.Blocked, result.Blocked);
        }
        [TestMethod]
        public async Task GetsByUsername_ReturnsObject_WhenParametersAreCorrect()
        {

            var validator = new Validator(mockContext.Object);
            var service = new UserService(mockContext.Object, validator);


            var expected = new User()
            {
                UserId = 3,
                UserName = "Mosho",
                FirstName = "Ivailo",
                LastName = "Vasilov",
                Email = "ivchopivcho@gmail.com",
                Role = Role.User,
                PhoneNumber = "08966222746",
                Blocked = false,
                isDeleted = false
            };
            //Act
            var result = await service.GetByUsernameAsync("Mosho");

            //Assert

            // await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.GetByUsernameAsync("dsadsafsaf"));

            Assert.AreEqual(expected.UserId, result.ToList()[0].UserId);
            Assert.AreEqual(expected.FirstName, result.ToList()[0].FirstName);
            Assert.AreEqual(expected.LastName, result.ToList()[0].LastName);
            Assert.AreEqual(expected.Email, result.ToList()[0].Email);
            Assert.AreEqual(expected.Role.ToString(), result.ToList()[0].Role);
            Assert.AreEqual(expected.PhoneNumber, result.ToList()[0].PhoneNumber);
            Assert.AreEqual(expected.Blocked, result.ToList()[0].Blocked);
        }
        [TestMethod]
        public async Task GetsByPhone_ReturnsObject_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var service = new UserService(mockContext.Object, validator);

            var expected = new User()
            {
                UserId = 2,
                UserName = "Terminatora",
                FirstName = "Asparuh",
                LastName = "Mladenov",
                Email = "terminatora@gmail.com",
                Role = Role.Admin,
                PhoneNumber = "08875322746",
                Blocked = false,
                isDeleted = false
            };
            //Act
            var result = await service.GetByPhoneNumberAsync("08875322746");

            //Assert

            //await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.GetByPhoneNumberAsync("08875322746fwe"));

            Assert.AreEqual(expected.UserId, result.ToList()[0].UserId);
            Assert.AreEqual(expected.FirstName, result.ToList()[0].FirstName);
            Assert.AreEqual(expected.LastName, result.ToList()[0].LastName);
            Assert.AreEqual(expected.Email, result.ToList()[0].Email);
            Assert.AreEqual(expected.Role.ToString(), result.ToList()[0].Role);
            Assert.AreEqual(expected.PhoneNumber, result.ToList()[0].PhoneNumber);
            Assert.AreEqual(expected.Blocked, result.ToList()[0].Blocked);
        }
        [TestMethod]
        public async Task GetsCount_ReturnsObject_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var service = new UserService(mockContext.Object, validator);

            //Act
            var result = await service.GetCountAsync();

            //Assert
            Assert.AreEqual(5, result);
        }
        [TestMethod]
        public async Task Create_ReturnsObject_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var service = new UserService(mockContext.Object, validator);


            var expected = new User()
            {
                UserId = 6,
                UserName = "Kitkovka",
                FirstName = "Kraska",
                LastName = "Praska",
                Email = "ksposhta@gmail.com",
                Role = Role.Admin,
                Password = "fsAsd&346faS3",
                PhoneNumber = "08876772746",
                Blocked = true,
                isDeleted = false
            };

            //Act
            var result = await service.CreateAsync(expected);

            //Assert

            Assert.AreEqual(expected.UserId, result.UserId);
            Assert.AreEqual(expected.UserName, result.UserName);
            Assert.AreEqual(expected.FirstName, result.FirstName);
            Assert.AreEqual(expected.LastName, result.LastName);
            Assert.AreEqual(expected.Email, result.Email);
            Assert.AreEqual(expected.Role, result.Role);
            Assert.AreEqual(expected.Password, result.Password);
            Assert.AreEqual(expected.PhoneNumber, result.PhoneNumber);
            Assert.AreEqual(expected.Blocked, result.Blocked);
        }
        [TestMethod]
        public async Task Update_ReturnsObject_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var service = new UserService(mockContext.Object, validator);

            var expected = new User()
            {
                UserId = 6,
                UserName = "Kitkovka",
                FirstName = "Kraska",
                LastName = "Praska",
                Email = "ksposhta@gmail.com",
                Role = Role.Admin,
                Password = "fsAsd&346faS3",
                PhoneNumber = "08876772746",
                Blocked = true,
                isDeleted = false
            };

            //Act
            var result = await service.UpdateAsync(5, expected);

            //Assert

            Assert.AreEqual(expected.UserId, result.UserId);
            Assert.AreEqual(expected.UserName, result.UserName);
            Assert.AreEqual(expected.FirstName, result.FirstName);
            Assert.AreEqual(expected.LastName, result.LastName);
            Assert.AreEqual(expected.Email, result.Email);
            Assert.AreEqual(expected.Role, result.Role);
            Assert.AreEqual(expected.Password, result.Password);
            Assert.AreEqual(expected.PhoneNumber, result.PhoneNumber);
            Assert.AreEqual(expected.Blocked, result.Blocked);

            expected.UserName = "fdsfds";
            expected.Email = "Etsttestse@gmai.com";
            expected.Password = "fdsASD32@fds";
            expected.PhoneNumber = "088493628746218765";
            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.UpdateAsync(82, expected));

        }
        [TestMethod]
        public async Task Delete_ReturnsBool_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var service = new UserService(mockContext.Object, validator);

            //Act
            var result = await service.DeleteAsync(1);

            //Assert
            Assert.AreEqual(result, true);
            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.DeleteAsync(8));

        }
        [TestMethod]
        public async Task GetsRecievedRatings_ReturnsObject_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var service = new UserService(mockContext.Object, validator);

            var rating = new Rating
            {
                RatingId = 1,
                RatingPoints = 1,
                Comment = "Mnogo ludo karash",
                RatedUserId = 1,
                RatedUser = mockUsers[0],
                RatingUserId = 2,
                RatingUser = mockUsers[1],
                isDeleted = false,
            };

            //Act
            var result = await service.GetRecievedRatingAsync(1);

            //Assert
            Assert.AreEqual(rating.RatingId, result.ToList()[0].RatingId);
            Assert.AreEqual(rating.RatingPoints, result.ToList()[0].RatingPoints);
            Assert.AreEqual(rating.Comment, result.ToList()[0].Comment);
            Assert.AreEqual(rating.RatedUserId, result.ToList()[0].RatedUserId);
            Assert.AreEqual(rating.RatedUser, result.ToList()[0].RatedUser);
            Assert.AreEqual(rating.RatingUserId, result.ToList()[0].RatingUserId);
            Assert.AreEqual(rating.RatingUser, result.ToList()[0].RatingUser);
            Assert.AreEqual(rating.isDeleted, result.ToList()[0].isDeleted);
        }
        [TestMethod]
        public async Task GetsGivenRatings_ReturnsObject_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var service = new UserService(mockContext.Object, validator);


            var rating = new Rating()
            {
                RatingId = 2,
                RatingPoints = 4,
                Comment = "qki janti",
                RatedUserId = 3,
                RatedUser = mockUsers[2],
                RatingUserId = 5,
                RatingUser = mockUsers[4],
                isDeleted = false,
            };

            //Act
            var result = await service.GetGivenRatingAsync(5);

            //Assert
            Assert.AreEqual(rating.RatingId, result.ToList()[0].RatingId);
            Assert.AreEqual(rating.RatingPoints, result.ToList()[0].RatingPoints);
            Assert.AreEqual(rating.Comment, result.ToList()[0].Comment);
            Assert.AreEqual(rating.RatedUserId, result.ToList()[0].RatedUserId);
            Assert.AreEqual(rating.RatedUser, result.ToList()[0].RatedUser);
            Assert.AreEqual(rating.RatingUserId, result.ToList()[0].RatingUserId);
            Assert.AreEqual(rating.RatingUser, result.ToList()[0].RatingUser);
            Assert.AreEqual(rating.isDeleted, result.ToList()[0].isDeleted);
        }
        [TestMethod]
        public async Task GetsTop10Passengers_ReturnsObject_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var service = new UserService(mockContext.Object, validator);

            var expected = new List<User>()
            {
                 new User
                {
                    UserId = 2,
                    UserName = "Terminatora",
                    FirstName = "Asparuh",
                    LastName = "Mladenov",
                    Email = "terminatora@gmail.com",
                    Role = Role.Admin,
                    PhoneNumber = "08875322746",
                    Blocked = false,
                    isDeleted = false
                },
               new User
                {
                    UserId = 3,
                    UserName = "Mosho",
                    FirstName = "Ivailo",
                    LastName = "Vasilov",
                    Email = "ivchopivcho@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08966222746",
                    Blocked = false,
                    isDeleted = false
                },

            };

            mockTrips[2].Passangers.Add(mockPassengerTrips[0]);

            mockUsers[1].TripsAsPassanger.Add(mockPassengerTrips[1]);
            ////
            mockUsers[2].RecievedRatings.Add(mockRatings[0]);
            mockUsers[1].RecievedRatings.Add(mockRatings[1]);
            //Act
            var result = await service.GetTopPassangersAsync();

            //Assert
            for (int i = 0; i < result.Count(); i++)
            {
                Assert.AreEqual(expected[i].UserId, result.ToList()[i].UserId);
                Assert.AreEqual(expected[i].FirstName, result.ToList()[i].FirstName);
                Assert.AreEqual(expected[i].LastName, result.ToList()[i].LastName);
                Assert.AreEqual(expected[i].Email, result.ToList()[i].Email);
                Assert.AreEqual(expected[i].Role.ToString(), result.ToList()[i].Role);
                Assert.AreEqual(expected[i].PhoneNumber, result.ToList()[i].PhoneNumber);
                Assert.AreEqual(expected[i].Blocked, result.ToList()[i].Blocked);
            }
        }//
        [TestMethod]
        public async Task GetsTop10Drivers_ReturnsObject_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var service = new UserService(mockContext.Object, validator);

            var expected = new List<User>()
            {
               new User
                {
                    UserId = 2,
                    UserName = "Terminatora",
                    FirstName = "Asparuh",
                    LastName = "Mladenov",
                    Email = "terminatora@gmail.com",
                    Role = Role.Admin,
                    PhoneNumber = "08875322746",
                    Blocked = false,
                    isDeleted = false
                },
               new User
                {
                    UserId = 3,
                    UserName = "Mosho",
                    FirstName = "Ivailo",
                    LastName = "Vasilov",
                    Email = "ivchopivcho@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08966222746",
                    Blocked = false,
                    isDeleted = false
                },
            };

            mockUsers[2].TripsAsDriver.Add(mockTrips[0]); //id3
            mockUsers[1].TripsAsDriver.Add(mockTrips[1]); //id2

            mockUsers[2].RecievedRatings.Add(mockRatings[0]);
            mockUsers[1].RecievedRatings.Add(mockRatings[1]);
            //Act
            var result = await service.GetTopOrganizersAsync();

            //Assert
            for (int i = 0; i < result.Count(); i++)
            {
                Assert.AreEqual(expected[i].UserId, result.ToList()[i].UserId);
                Assert.AreEqual(expected[i].FirstName, result.ToList()[i].FirstName);
                Assert.AreEqual(expected[i].LastName, result.ToList()[i].LastName);
                Assert.AreEqual(expected[i].Email, result.ToList()[i].Email);
                Assert.AreEqual(expected[i].Role.ToString(), result.ToList()[i].Role);
                Assert.AreEqual(expected[i].PhoneNumber, result.ToList()[i].PhoneNumber);
                Assert.AreEqual(expected[i].Blocked, result.ToList()[i].Blocked);
            }

        }//
        [TestMethod]
        public async Task Block_ReturnsObject_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var service = new UserService(mockContext.Object, validator);

            //Act
            var result = await service.BlockAsync(1);
            //Assert
            Assert.AreEqual(true, result.Blocked);
            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.UnblockAsync(82));
        }
        [TestMethod]
        public async Task Unblock_ReturnsObject_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var service = new UserService(mockContext.Object, validator);

            var expected = new User()
            {
                UserId = 1,
                UserName = "Pesho",
                FirstName = "Gero",
                LastName = "Petkov",
                Email = "gercata@test.com",
                Role = Role.User,
                PhoneNumber = "08876222746",
                Blocked = true,
                isDeleted = false
            };

            //Act
            var result = await service.UnblockAsync(1);

            //Assert
            Assert.AreEqual(false, result.Blocked);
            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.UnblockAsync(82));

        }
        [TestMethod]
        public async Task ApplyForTrip_WorksAsIntended_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var service = new UserService(mockContext.Object, validator);

            //Act
            var result = await service.ApplyForTrip(1, 4);

            //Assert
            Assert.AreEqual(mockUsers[0].AppliedTripId, 4);
            Assert.AreEqual(mockUsers[0].AppliedTrip, mockTrips[3]);
            foreach (var candidate in mockTrips[3].CandidatesForTrip)
            {
                Assert.AreEqual(mockUsers[0].UserId, candidate.UserId);
                Assert.AreEqual(mockUsers[0].UserName, candidate.UserName);
                Assert.AreEqual(mockUsers[0].FirstName, candidate.FirstName);
                Assert.AreEqual(mockUsers[0].LastName, candidate.LastName);
                Assert.AreEqual(mockUsers[0].Email, candidate.Email);
                Assert.AreEqual(mockUsers[0].Role.ToString(), candidate.Role.ToString());
                Assert.AreEqual(mockUsers[0].PhoneNumber, candidate.PhoneNumber);
                Assert.AreEqual(mockUsers[0].Blocked, candidate.Blocked);
            }
            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.ApplyForTrip(10, 4));
            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.ApplyForTrip(1, 82));
            await Assert.ThrowsExceptionAsync<InvalidUserOperationException>(async () => await service.ApplyForTrip(4, 4));
            await Assert.ThrowsExceptionAsync<InvalidUserOperationException>(async () => await service.ApplyForTrip(1, 4));

            mockTrips[3].FreeSeats = 0;
            await Assert.ThrowsExceptionAsync<InvalidUserOperationException>(async () => await service.ApplyForTrip(1, 4));
        }
        [TestMethod]
        public async Task LeaveFromCandidate_WorksAsIntended_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var service = new UserService(mockContext.Object, validator);


            var expected = new User()
            {
                UserId = 1,
                UserName = "Pesho",
                FirstName = "Gero",
                LastName = "Petkov",
                Email = "gercata@test.com",
                Role = Role.User,
                PhoneNumber = "08876222746",
                Blocked = false,
                isDeleted = false
            };

            //Act
            var applied = await service.ApplyForTrip(1, 4);
            var result = await service.LeaveFromCandidates(1, 4);

            //Assert

            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.LeaveFromCandidates(9, 4));

            Assert.AreEqual(mockUsers[0].AppliedTripId, null);
            Assert.AreEqual(mockUsers[0].AppliedTrip, null);

            Assert.AreEqual(mockTrips[3].CandidatesForTrip.Count, 0);
        }
        [TestMethod]
        public async Task LeaveTrip_WorksAsIntended_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var service = new UserService(mockContext.Object, validator);

            var passengerTrip = new PassengerTrip()
            {
                Id = 1,
                UserId = 1,
                Passanger = mockUsers[0],
                TripId = 3,
                Trip = mockTrips[2],
                isDeleted = false,
            };
            mockTrips[2].Passangers.Add(passengerTrip);

            mockUsers[0].TripsAsPassanger.Add(passengerTrip);

            //Act
            var result = await service.LeaveTrip(1, 3);

            //Assert

            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.LeaveTrip(1, 10));
            await Assert.ThrowsExceptionAsync<InvalidUserOperationException>(async () => await service.LeaveTrip(10, 3));

            Assert.AreEqual(0, mockUsers[0].TripsAsPassanger.Count);
            Assert.AreEqual(0, result.Passangers.Count);


            Assert.AreEqual(mockTrips[2].TripId, result.TripId);
            Assert.AreEqual(mockTrips[2].TripStatus.ToString(), result.TripStatus);
            Assert.AreEqual(mockTrips[2].PetsAllowed, result.PetsAllowed);
            Assert.AreEqual(mockTrips[2].SmokingAllowed, result.SmokingAllowed);
            Assert.AreEqual(mockTrips[2].AirConditioner, result.AirConditioner);
            Assert.AreEqual(mockTrips[2].DriverId, result.DriverId);
            Assert.AreEqual(mockTrips[2].StartingPoint.ToString(), result.StartingPoint);
            Assert.AreEqual(mockTrips[2].EndingPoint.ToString(), result.EndingPoint);
            Assert.AreEqual(3, result.FreeSeats);
        }

        [TestMethod]
        public async Task SearchAllFields_RetunsUsers_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var service = new UserService(mockContext.Object, validator);

            var user = new User
            {
                UserId = 1,
                UserName = "Pesho",
                FirstName = "Gero",
                LastName = "Petkov",
                Email = "gercata@test.com",
                Role = Role.User,
                PhoneNumber = "08876222746",
                Blocked = false,
                isDeleted = false
            };

            //Act
            var result = await service.SearchAllFieldsAsync("test");

            //Assert


            for (int i = 0; i < result.Count(); i++)
            {
                Assert.AreEqual(user.UserId, result.ToList()[i].UserId);
                Assert.AreEqual(user.UserName.ToString(), result.ToList()[i].UserName);
                Assert.AreEqual(user.FirstName, result.ToList()[i].FirstName);
                Assert.AreEqual(user.LastName, result.ToList()[i].LastName);
                Assert.AreEqual(user.Email, result.ToList()[i].Email);
                Assert.AreEqual(user.Role.ToString(), result.ToList()[i].Role);
                Assert.AreEqual(user.PhoneNumber, result.ToList()[i].PhoneNumber);
                Assert.AreEqual(user.Blocked, result.ToList()[i].Blocked);
            }

            var user2 = new User
            {
                UserId = 2,
                UserName = "Terminatora",
                FirstName = "Asparuh",
                LastName = "Mladenov",
                Email = "terminatora@gmail.com",
                Role = Role.Admin,
                PhoneNumber = "08875322746",
                Blocked = false,
                isDeleted = false
            };

            var result2 = await service.SearchAllFieldsAsync("Terminatora");
            for (int i = 0; i < result.Count(); i++)
            {
                Assert.AreEqual(user2.UserId, result2.ToList()[i].UserId);
                Assert.AreEqual(user2.UserName.ToString(), result2.ToList()[i].UserName);
                Assert.AreEqual(user2.FirstName, result2.ToList()[i].FirstName);
                Assert.AreEqual(user2.LastName, result2.ToList()[i].LastName);
                Assert.AreEqual(user2.Email, result2.ToList()[i].Email);
                Assert.AreEqual(user2.Role.ToString(), result2.ToList()[i].Role);
                Assert.AreEqual(user2.PhoneNumber, result2.ToList()[i].PhoneNumber);
                Assert.AreEqual(user2.Blocked, result2.ToList()[i].Blocked);
            }

            var user3 = new User
            {
                UserId = 4,
                UserName = "Bosho",
                FirstName = "Tonka",
                LastName = "Miteva",
                Email = "babavitonka@gmail.com",
                Role = Role.User,
                PhoneNumber = "08876259746",
                Blocked = false,
                isDeleted = false
            };

            var result3 = await service.SearchAllFieldsAsync("25");
            for (int i = 0; i < result.Count(); i++)
            {
                Assert.AreEqual(user3.UserId, result3.ToList()[i].UserId);
                Assert.AreEqual(user3.UserName.ToString(), result3.ToList()[i].UserName);
                Assert.AreEqual(user3.FirstName, result3.ToList()[i].FirstName);
                Assert.AreEqual(user3.LastName, result3.ToList()[i].LastName);
                Assert.AreEqual(user3.Email, result3.ToList()[i].Email);
                Assert.AreEqual(user3.Role.ToString(), result3.ToList()[i].Role);
                Assert.AreEqual(user3.PhoneNumber, result3.ToList()[i].PhoneNumber);
                Assert.AreEqual(user3.Blocked, result3.ToList()[i].Blocked);
            }
        }

        [TestMethod]
        public async Task GetsAllUserAddresses_ReturnsObject_WhenParametersAreCorrect()
        {
            var validator = new Validator(mockContext.Object);
            var service = new UserService(mockContext.Object, validator);

            mockUsers[1].UsedAddresses.Add(mockAddresses[0]);
            mockUsers[1].UsedAddresses.Add(mockAddresses[1]);
            //Act
            var result = await service.TakeUserAddresses(2);

            //Assert

            for (int i = 0; i < result.Count(); i++)
            {
                Assert.AreEqual(mockAddresses[i].AddressId, result.ToList()[i].AddressId);
                Assert.AreEqual(mockAddresses[i].Street, result.ToList()[i].Street);
                Assert.AreEqual(mockAddresses[i].City.Name, result.ToList()[i].City);
            }
        }
        [TestMethod]
        public async Task UpdatesPassword_ReturnsUser_WhenParametersAreCorrect()
        {
            //Arrange
            var validator = new Validator(mockContext.Object);
            var service = new UserService(mockContext.Object, validator);

            //Act
            var result = await service.UpdatePassword(3, "123456789", "dfsafAsd#432");

            //Assert
            Assert.AreEqual(mockUsers[2].UserId, result.UserId);
            Assert.AreEqual(mockUsers[2].UserName, result.UserName);
            Assert.AreEqual(mockUsers[2].FirstName, result.FirstName);
            Assert.AreEqual(mockUsers[2].LastName, result.LastName);
            Assert.AreEqual(mockUsers[2].Email, result.Email);
            Assert.AreEqual(mockUsers[2].Role, result.Role);
            Assert.AreEqual(mockUsers[2].Password, result.Password);
            Assert.AreEqual(mockUsers[2].PhoneNumber, result.PhoneNumber);
            Assert.AreEqual(mockUsers[2].Blocked, result.Blocked);
        }
    }
}
