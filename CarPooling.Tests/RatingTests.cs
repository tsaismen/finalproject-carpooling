﻿using CarPooling.Data.Database.Contracts;
using CarPooling.Data.Models;
using CarPooling.Service.Services;
using CarPooling.Services.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockQueryable.Moq;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Tests
{
    [TestClass]
    public class RatingTests
    {
        Mock<IDatabase> mockContext;

        Mock<DbSet<User>> mockDbUsers;
        Mock<DbSet<Rating>> mockDbRatings;

        List<User> mockUsers;
        List<Rating> mockRatings;

        [TestInitialize]
        public async Task TestInitialize()
        {
            mockContext = new Mock<IDatabase>();

            mockUsers = new List<User>
            {
                new User
                {
                    UserId = 1,
                    UserName = "Pesho",
                    FirstName = "Gero",
                    LastName = "Petkov",
                    Email = "gercata@test.com",
                    Role = Role.User,
                    PhoneNumber = "08876222746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 2,
                    UserName = "Terminatora",
                    FirstName = "Asparuh",
                    LastName = "Mladenov",
                    Email = "terminatora@gmail.com",
                    Role = Role.Admin,
                    PhoneNumber = "08875322746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 3,
                    UserName = "Mosho",
                    FirstName = "Ivailo",
                    LastName = "Vasilov",
                    Email = "ivchopivcho@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08966222746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 4,
                    UserName = "Bosho",
                    FirstName = "Tonka",
                    LastName = "Miteva",
                    Email = "babavitonka@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08876259746",
                    Blocked = false,
                    isDeleted = false
                },
                new User
                {
                    UserId = 5,
                    UserName = "Kitka",
                    FirstName = "Krasi",
                    LastName = "Georgiev",
                    Email = "Krasko404@gmail.com",
                    Role = Role.User,
                    PhoneNumber = "08876222746",
                    Blocked = false,
                    isDeleted = false
                }
            };

            mockRatings = new List<Rating>
            {
                new Rating
                {
                    RatingId = 1,
                    RatingPoints = 1,
                    Comment = "Mnogo ludo karash",
                    RatedUserId = 1,
                    RatedUser = mockUsers[0],
                    RatingUserId = 2,
                    RatingUser = mockUsers[1],
                    isDeleted = false,
                },
                new Rating
                {
                    RatingId = 2,
                    RatingPoints = 4,
                    Comment = "qki janti",
                    RatedUserId = 3,
                    RatedUser = mockUsers[2],
                    RatingUserId = 5,
                    RatingUser = mockUsers[4],
                    isDeleted = false,
                },
            };

            mockDbUsers = mockUsers.AsQueryable().BuildMockDbSet();
            mockDbRatings = mockRatings.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockDbUsers.Object);
            mockContext.Setup(db => db.Ratings).Returns(mockDbRatings.Object);
        }
        [TestMethod]
        public async Task GetsById_ReturnsObject_WhenParametersAreCorrect()
        {
            //Arrange
            var service = new RatingService(mockContext.Object);

            var expected = new Rating()
            {
                RatingId = 1,
                RatingPoints = 1,
                Comment = "Mnogo ludo karash",
                RatedUserId = 1,
                RatedUser = mockUsers[0],
                RatingUserId = 2,
                RatingUser = mockUsers[1],
                isDeleted = false,
            };

            //Act
            var result = await service.GetByIdAsync(1);

            //Assert
            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.GetByIdAsync(9));

            Assert.AreEqual(expected.RatingId, result.RatingId);
            Assert.AreEqual(expected.RatingPoints, result.RatingPoints);
            Assert.AreEqual(expected.RatedUserId, result.RatedUserId);
            Assert.AreEqual(expected.RatedUserId, result.RatedUser.UserId);
            Assert.AreEqual(expected.RatingUserId, result.RatingUserId);
            Assert.AreEqual(expected.RatingUserId, result.RatingUser.UserId);
            Assert.AreEqual(expected.isDeleted, result.isDeleted);
        }

        [TestMethod]
        public async Task GetsAll_ReturnsObject_WhenParametersAreCorrect()
        {
            //Arrange
            var service = new RatingService(mockContext.Object);

            //Act
            var result = await service.GetAllAsync();

            //Assert
            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.GetByIdAsync(9));
            for (int i = 0; i < result.Count(); i++)
            {
                Assert.AreEqual(mockRatings[i].RatingPoints, result.ToList()[i].RatingPoints);
                Assert.AreEqual(mockRatings[i].RatedUserId, result.ToList()[i].RatedUserId);
                Assert.AreEqual(mockRatings[i].RatingUserId, result.ToList()[i].RatingUserId);
            }
        }

        [TestMethod]
        public async Task Create_ReturnsObject_WhenParametersAreCorrect()
        {
            //Arrange
            var service = new RatingService(mockContext.Object);

            var expected = new Rating()
            {
                RatingId = 3,
                RatingPoints = 5,
                Comment = "Mndsadsao karash",
                RatedUserId = 4,
                RatedUser = mockUsers[3],
                RatingUserId = 1,
                RatingUser = mockUsers[0],
                isDeleted = false,
            };


            var duplicate = new Rating()
            {
                RatingId = 1,
                RatingPoints = 1,
                Comment = "Mnogo ludo karash",
                RatedUserId = 1,
                RatedUser = mockUsers[0],
                RatingUserId = 2,
                RatingUser = mockUsers[1],
                isDeleted = false,
            };

            //Act
            var result = await service.CreateAsync(expected);


            //Assert
            await Assert.ThrowsExceptionAsync<DuplicateEntityException>(async () => await service.CreateAsync(duplicate));

            Assert.AreEqual(expected.RatingId, result.RatingId);
            Assert.AreEqual(expected.RatingPoints, result.RatingPoints);
            Assert.AreEqual(expected.RatedUserId, result.RatedUserId);
            Assert.AreEqual(expected.RatedUserId, result.RatedUser.UserId);
            Assert.AreEqual(expected.RatingUserId, result.RatingUserId);
            Assert.AreEqual(expected.RatingUserId, result.RatingUser.UserId);
            Assert.AreEqual(expected.isDeleted, result.isDeleted);
        }

        [TestMethod]
        public async Task Update_ReturnsObject_WhenParametersAreCorrect()
        {
            //Arrange
            var service = new RatingService(mockContext.Object);

            var expected = new Rating()
            {
                RatingId = 1,
                RatingPoints = 4,
                Comment = "Mnogfdsfsdo ludo karash",
                RatedUserId = 4,
                RatedUser = mockUsers[3],
                RatingUserId = 3,
                RatingUser = mockUsers[2],
                isDeleted = false,
            };

            //Act
            var result = await service.UpdateAsync(1, expected);

            //Assert
            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.UpdateAsync(9, expected));

            Assert.AreEqual(expected.RatingPoints, result.RatingPoints);
            Assert.AreEqual(expected.Comment, result.Comment);

        }

        [TestMethod]
        public async Task Delete_ReturnsObject_WhenParametersAreCorrect()
        {
            //Arrange
            var service = new RatingService(mockContext.Object);

            var expected = new Rating()
            {
                RatingId = 1,
                RatingPoints = 1,
                Comment = "Mnogo ludo karash",
                RatedUserId = 1,
                RatedUser = mockUsers[0],
                RatingUserId = 2,
                RatingUser = mockUsers[1],
                isDeleted = false,
            };

            //Act
            var result = await service.DeleteAsync(1);

            //Assert
            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await service.GetByIdAsync(9));

            Assert.AreEqual(expected.RatingId, result.RatingId);
            Assert.AreEqual(expected.RatingPoints, result.RatingPoints);
            Assert.AreEqual(expected.RatedUserId, result.RatedUserId);
            Assert.AreEqual(expected.RatedUserId, result.RatedUser.UserId);
            Assert.AreEqual(expected.RatingUserId, result.RatingUserId);
            Assert.AreEqual(expected.RatingUserId, result.RatingUser.UserId);
            Assert.AreEqual(expected.isDeleted, result.isDeleted);
        }
    }
}
