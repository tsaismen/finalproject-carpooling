# Tripsy :tm:

Tripsy is a platform for shared travels and experiences, with which
You can save money and create new friendships.

![Home Page](Images/homepage.jpg)


# Authors
- [Miroslav Petrov](https://www.linkedin.com/in/miroslav-petrov1/)
- [Mihail Matov](https://www.linkedin.com/in/mihail-matov/)



# Technologies
- ASP .NET Core
- ASP .NET Core MVC
- ASP .NET Core API
- Swagger
- MS SQL Server
- Entity Framework Core
- Google SMTP Server
- JavaScript/jQuery/AJAX
- HTML5/CSS3/Bootstrap/Font Awesome
- SignalR
- Weather API
- Moq


# How to setup:
1. First, get the clone link from the clone button.

![First Step](Images/clone link.jpg)


2. Then make a folder for the project.
3. Open Visual Studio and choose clone repository.

![Second Step](Images/visualstudio.jpg)


4. Use the clone link and select your folder.

![Last Step](Images/clonesetup.jpg)


And you are finished! All the files will be downloaded into your folder.

# Areas
- Public - users are only allowed to register or log in
- Private - available after registration
- Administrative - available for admins only
- API

# Public
You can browse trips and see their details.

![Trips Table](Images/trips.jpg)


You can look at the Top 10 drivers and passengers along with their profiles.

![Top 10 Organisers](Images/top10.jpg)


# Private
The private part provides a large variety of functionalities:

- After you create an account, you can create trips of your own and apply for other trips.
- You can edit your trips anytime.
- Approve/Decline people from your trip.
- You can apply for trips and unapply for them and leave if you change your mind.
- Add your profile picture.
- Update your account information.
- Give ratings for other passengers you have traveled with and for the drivers.


- This is the form for creating a trip.
![Part of user Dashboard](Images/create.jpg)



# Admin
- Block/Unblock users and follow their activity.

![Admin View](Images/admin.jpg)

# API
The API supports all the functionalities of the web part of the application.

![Overview](Images/api.jpg)

Here we will show an example of how the API works.
We are using header authentication.

![Example](Images/apiexample.jpg)

And it has successfully returned the address we were looking for!

![Success](Images/apisuccess.jpg)


# Features

* Every user can send an email to us and contact us!

![Email Form](Images/emailform.jpg)

![Email Form](Images/thankyou.jpg)

* Live statistics!

![Statistics](Images/livestatistic.jpg)

* Weather forecast for every travel.

![Weather forecast](Images/weather.jpg)


* Chat for customers to arrange their journeys.

![Empty Chat](Images/chatempty.jpg)

![Conversation](Images/chatmessages.jpg)


* Dynamic rating display!

![Dynamic Star Rating](Images/rating.jpg)

* Our site also supports various filtering and sorting.
