﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace CarPooling.Data.Models
{
    public class Address
    {
        [Key]
        public int AddressId { get; set; }
        [StringLength(30, MinimumLength = 4)]
        public string Street { get; set; }
        [ForeignKey(nameof(City))]
        public int CityId { get; set; }
        public City City { get; set; }
        [ForeignKey(nameof(UsersUses))]
        public int UserUsesId { get; set; }
        [JsonIgnore]
        public User UsersUses { get; set; }
        [JsonIgnore]
        public bool isDeleted { get; set; }

        public override string ToString()
        {
            return $"{Street}, {City.Name}";
        }
    }
}