﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;

namespace CarPooling.Data.Models
{
    public class User
    {
        [Key]
        public int UserId { get; set; }
        [JsonIgnore]
        public byte[]? ProfilePicture { get; set; }
        [StringLength(20, MinimumLength = 2)]
        public string UserName { get; set; }
        [StringLength(20, MinimumLength = 2)]
        public string FirstName { get; set; }
        [StringLength(20, MinimumLength = 2)]
        public string LastName { get; set; }
        [StringLength(int.MaxValue, MinimumLength = 8)]
        public string Password { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public Role Role { get; set; }
        [Phone]
        public string PhoneNumber { get; set; }
        public bool Blocked { get; set; }
        [JsonIgnore]
        public ICollection<Address> UsedAddresses { get; set; } = new List<Address>();
        [JsonIgnore]
        public ICollection<Rating> GivenRatings { get; set; } = new List<Rating>();
        [JsonIgnore]
        public ICollection<Rating> RecievedRatings { get; set; } = new List<Rating>();
        [JsonIgnore]
        public ICollection<Trip> TripsAsDriver { get; set; } = new List<Trip>();
        [JsonIgnore]
        public ICollection<PassengerTrip> TripsAsPassanger { get; set; } = new List<PassengerTrip>();
        [ForeignKey(nameof(Trip))]
        public int? AppliedTripId { get; set; }
        [JsonIgnore]
        public Trip AppliedTrip { get; set; }

        [JsonIgnore]
        public bool isDeleted { get; set; }
        public double CalcRating()
        {
            if (RecievedRatings.Count == 0)
                return 0;
            var rating = RecievedRatings.Average(x => x.RatingPoints);
            rating = System.Math.Round(rating, 1);
            return rating;
        }
    }
}