﻿using CarPooling.Data.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace CarPooling.Data.Models
{
    public class Trip
    {
        [Key]
        public int TripId { get; set; }
        [ForeignKey(nameof(User))]
        public int DriverId { get; set; }
        public User Driver { get; set; }
        public string CarColor { get; set; }
        public string CarModel { get; set; }
        [ForeignKey(nameof(Address))]
        public int StartingPointId { get; set; }
        public Address StartingPoint { get; set; }
        [ForeignKey(nameof(Address))]
        public int EndingPointId { get; set; }
        public Address EndingPoint { get; set; }
        public DateTime DepartureTime { get; set; }
        public int FreeSeats { get; set; }
        public bool PetsAllowed { get; set; }
        public bool SmokingAllowed { get; set; }
        public bool AirConditioner { get; set; }
        public TripStatus TripStatus { get; set; } = TripStatus.Active;
        public ICollection<PassengerTrip> Passangers { get; set; } = new List<PassengerTrip>();
        public ICollection<User> CandidatesForTrip { get; set; } = new List<User>();
        [JsonIgnore]
        public bool isDeleted { get; set; }
    }
}
