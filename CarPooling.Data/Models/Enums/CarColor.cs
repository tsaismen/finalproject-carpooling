﻿namespace CarPooling.Data.Models
{
    public enum CarColor
    {
        WHITE,
        BLACK,
        SILVER,
        LIGHTRED,
        DARKRED,
        LIGHTGREEN,
        DARKGREEN,
        LIGHTBLUE,
        DARKBLUE,
        LIGHTPURPLE,
        DARKPURPLE
    }
}
