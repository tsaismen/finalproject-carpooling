﻿namespace CarPooling.Data.Models.Enums
{
    public enum TripStatus
    {
        Active,
        Canceled,
        Completed
    }
}
