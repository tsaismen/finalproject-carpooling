﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace CarPooling.Data.Models
{
    public class Message
    {
        [Key]
        public int MessageId { get; set; }
        [ForeignKey(nameof(User))]
        public int UserId { get; set; }
        public User User { get; set; }
        public string Content { get; set; }
        public DateTime When { get; set; } = DateTime.UtcNow;
        [JsonIgnore]
        public bool isDeleted { get; set; }
    }
}
