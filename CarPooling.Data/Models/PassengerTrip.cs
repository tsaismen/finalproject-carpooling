﻿using System.Text.Json.Serialization;

namespace CarPooling.Data.Models
{
    public class PassengerTrip
    {
        [JsonIgnore]
        public int Id { get; set; }

        public int UserId { get; set; }
        [JsonIgnore]
        public User Passanger { get; set; }

        public int TripId { get; set; }
        [JsonIgnore]
        public Trip Trip { get; set; }
        [JsonIgnore]
        public bool isDeleted { get; set; }
    }
}
