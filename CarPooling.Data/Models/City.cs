﻿using System.ComponentModel.DataAnnotations;

namespace CarPooling.Data.Models
{
    public class City
    {
        [Key]
        public int CityId { get; set; }
        [StringLength(20, MinimumLength = 2)]
        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
