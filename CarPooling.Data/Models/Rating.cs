﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace CarPooling.Data.Models
{
    public class Rating
    {
        [Key]
        public int RatingId { get; set; }
        [Range(0, 5, ErrorMessage = "Rating  must be between {0} and {1} stars!")]
        public int RatingPoints { get; set; }
        public string Comment { get; set; }
        [ForeignKey(nameof(User))]
        public int RatedUserId { get; set; }
        public User RatedUser { get; set; }
        [ForeignKey(nameof(User))]
        public int RatingUserId { get; set; }
        public User RatingUser { get; set; }
        [JsonIgnore]
        public bool isDeleted { get; set; }
    }
}
