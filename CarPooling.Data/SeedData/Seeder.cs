﻿using CarPooling.Data.Models;
using CarPooling.Data.Models.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace CarPooling.Data.SeedData
{
    public static class Seeder
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            var password = "123456789";
            var hashedpassword = new PasswordHasher<object?>().HashPassword(null, password);
            var Cities = new List<City>() {
                new City
                {
                    CityId = 1,
                    Name = "Sofia",
                    Latitude = 42.698334,
                    Longitude = 23.319941,
                },
                new City
                {
                    CityId = 2,
                    Name = "Plovdiv",
                    Latitude = 42.15,
                    Longitude = 24.75,

                },
                new City
                {
                    CityId = 3,
                    Name = "Montana",
                    Latitude = 43.4125,
                    Longitude = 23.225,
                },
                new City
                {
                    CityId = 4,
                    Name = "Burgas",
                    Latitude = 42.510578,
                    Longitude = 27.461014,
                },
                new City
                {
                    CityId = 5,
                    Name = "Sozopol",
                    Latitude = 42.41667,
                    Longitude = 27.7,
                },
                new City
                {
                    CityId = 7,
                    Name = "Varna",
                    Latitude = 43.204666,
                    Longitude = 27.910543,
                },
                new City
                {
                    CityId = 8,
                    Name = "Ahtopol",
                    Latitude = 42.0994,
                    Longitude = 27.9407,
                },
                new City
                {
                    CityId = 9,
                    Name = "Berkovitsa",
                    Latitude = 43.23611,
                    Longitude = 23.12583,
                },
                new City
                {
                    CityId = 10,
                    Name = "Vratsa",
                    Latitude = 43.21,
                    Longitude = 23.5625,
                },
                new City
                {
                    CityId = 11,
                    Name = "Blagoevgrad",
                    Latitude = 42.01667,
                    Longitude = 23.1,
                },
                new City
                {
                    CityId = 12,
                    Name = "Pleven",
                    Latitude = 43.41667,
                    Longitude = 24.61667,
                },
                new City
                {
                    CityId = 13,
                    Name = "Veliko Tarnovo",
                    Latitude =  43.08124,
                    Longitude = 25.62904,
                },
                new City
                {
                    CityId = 14,
                    Name = "Gabrovo",
                    Latitude = 42.87472,
                    Longitude = 25.33417,
                },
                new City
                {
                    CityId = 15,
                    Name = "Bansko",
                    Latitude = 41.840424,
                    Longitude = 23.485653,
                },
                new City
                {
                    CityId = 16,
                    Name = "Balchik",
                    Latitude = 43.41667,
                    Longitude = 28.16667,
                },
                new City
                {
                    CityId = 18,
                    Name = "Pernik",
                    Latitude = 42.6,
                    Longitude = 23.03333,
                },
                new City
                {
                    CityId = 19,
                    Name = "Petrich",
                    Latitude = 41.4,
                    Longitude = 23.21667,
                },
                new City
                {
                    CityId = 20,
                    Name = "Lom",
                    Latitude = 43.81389,
                    Longitude = 23.23611,
                },
                new City
                {
                    CityId = 21,
                    Name = "Vidin",
                    Latitude = 43.99,
                    Longitude = 22.8725,
                }
            };

            modelBuilder.Entity<City>().HasData(Cities);

            var Addresses = new List<Address>()
            {
                new Address
                {
                    AddressId = 1,
                    Street = "Opalchenska 78",
                    CityId = 1,
                    UserUsesId=1,
                    isDeleted = false
                },
                new Address
                {
                    AddressId = 2,
                    Street = "Hristo Botev 78",
                    CityId = 5,
                    UserUsesId=2,
                    isDeleted = false
                },
                new Address
                {
                    AddressId = 3,
                    Street = "Kriv Zavoi 78",
                    CityId = 4,
                    UserUsesId=3,
                    isDeleted = false
                },
                new Address
                {
                    AddressId = 4,
                    Street = "Mecha Polqna 78",
                    CityId = 3,
                    UserUsesId=4,
                    isDeleted = false
                },
                new Address
                {
                    AddressId = 5,
                    Street = "Stamen Iliev 2b",
                    CityId = 5,
                    UserUsesId=5,
                    isDeleted = false
                },
                new Address
                {
                    AddressId = 6,
                    Street = "Mladost 2 bl. 256",
                    CityId = 2,
                    UserUsesId=5,
                    isDeleted = false
                },
                new Address
                {
                    AddressId = 7,
                    Street = "Danail Nikolaev 22",
                    CityId = 7,
                    UserUsesId=1,
                    isDeleted = false
                },
                new Address
                {
                    AddressId = 8,
                    Street = "Solun 99",
                    CityId = 1,
                    UserUsesId=5,
                    isDeleted = false
                },
                new Address
                {
                    AddressId = 9,
                    Street = "Alexander Stamboliiski 44",
                    CityId = 2,
                    UserUsesId=2,
                    isDeleted = false
                },
                new Address
                {
                    AddressId = 10,
                    Street = "Evlogi Georgiev 88",
                    CityId = 12,
                    UserUsesId=5,
                    isDeleted = false
                },
                new Address
                {
                    AddressId = 11,
                    Street = "Gen. Totleben 54",
                    CityId = 14,
                    UserUsesId=5,
                    isDeleted = false
                },
                new Address
                {
                    AddressId = 12,
                    Street = "Patriarh Evtimii 34",
                    CityId = 1,
                    UserUsesId=2,
                    isDeleted = false
                },
                new Address
                {
                    AddressId = 13,
                    Street = "Tsarigradsko Shose 55",
                    CityId = 2,
                    UserUsesId=5,
                    isDeleted = false
                },
                new Address
                {
                    AddressId = 14,
                    Street = "Vasil Levski 66",
                    CityId = 4,
                    UserUsesId=1,
                    isDeleted = false
                }
            };

            modelBuilder.Entity<Address>().HasData(Addresses);

            var Users = new List<User>()
            {
                new User
                {
                    UserId=1,
                    ProfilePicture = null,
                    UserName = "Gero55",
                    FirstName = "Gerasim",
                    LastName = "Petkov",
                    Email = "gercata@gmail.com",
                    Password = hashedpassword,
                    Role = Role.User,
                    PhoneNumber = "08876222746",
                    Blocked = false,
                },
                new User //admin
                {
                    UserId=2,
                    ProfilePicture = null,
                    UserName = "Mladenov99",
                    FirstName = "Asparuh",
                    LastName = "Mladenov",
                    Email = "aspmladenov@gmail.com",
                    Password = hashedpassword,
                    Role = Role.Admin,
                    PhoneNumber = "08875322746",
                    Blocked = false,
                },
                new User
                {
                    UserId=3,
                    ProfilePicture = null,
                    UserName = "Ivailo347",
                    FirstName = "Ivailo",
                    LastName = "Vasilov",
                    Email = "ivchopivcho@gmail.com",
                    Password = hashedpassword,
                    Role = Role.User,
                    PhoneNumber = "08966222746",
                    Blocked = false,
                },
                new User
                {
                    UserId=4,
                    ProfilePicture = null,
                    UserName = "BabaTonka22",
                    FirstName = "Tonka",
                    LastName = "Miteva",
                    Email = "babavitonka@gmail.com",
                    Password = hashedpassword,
                    Role = Role.User,
                    PhoneNumber = "08876259746",
                    Blocked = false,
                },
                new User
                {
                    UserId=5,
                    ProfilePicture = null,
                    UserName = "KrasiRoka",
                    FirstName = "Krasimir",
                    LastName = "Georgiev",
                    Email = "Krasko404@gmail.com",
                    Password = hashedpassword,
                    Role = Role.User,
                    PhoneNumber = "08876222746",
                    Blocked = false,
                },
                new User
                {
                    UserId=6,
                    ProfilePicture = null,
                    UserName = "Fritez",
                    FirstName = "Miroslav",
                    LastName = "Petrov",
                    Email = "fritez@abv.bg",
                    Password = hashedpassword,
                    Role = Role.Admin,
                    PhoneNumber = "0887622274",
                    Blocked = false,
                }
                ,
                new User
                {
                    UserId=7,
                    ProfilePicture = null,
                    UserName = "Mi6ko",
                    FirstName = "Mihail",
                    LastName = "Matov",
                    Email = "mi6ko@gmail.com",
                    Password = hashedpassword,
                    Role = Role.Admin,
                    PhoneNumber = "08842421122",
                    Blocked = false,
                }
                ,
                new User
                {
                    UserId=8,
                    ProfilePicture = null,
                    UserName = "Antoncho22",
                    FirstName = "Anton",
                    LastName = "Staykov",
                    Email = "anton.staykov@gmail.com",
                    Password = hashedpassword,
                    Role = Role.User,
                    PhoneNumber = "0887611746",
                    Blocked = false,
                }
                ,
                new User
                {
                    UserId=9,
                    ProfilePicture = null,
                    UserName = "Gopeto98",
                    FirstName = "Georgi",
                    LastName = "Petrov",
                    Email = "georgi.petrov@gmail.com",
                    Password = hashedpassword,
                    Role = Role.User,
                    PhoneNumber = "0887192746",
                    Blocked = false,
                }
                ,
                new User
                {
                    UserId=10,
                    ProfilePicture = null,
                    UserName = "Elencheto",
                    FirstName = "Elena",
                    LastName = "Shtereva",
                    Email = "elena.barca@gmail.com",
                    Password = hashedpassword,
                    Role = Role.User,
                    PhoneNumber = "0883322746",
                    Blocked = false,
                }
                ,
                new User
                {
                    UserId=11,
                    ProfilePicture = null,
                    UserName = "Kircata",
                    FirstName = "Kiril",
                    LastName = "Stanoev",
                    Email = "kircata@gmail.com",
                    Password = hashedpassword,
                    Role = Role.Admin,
                    PhoneNumber = "0887113746",
                    Blocked = false,
                }
                ,
                new User
                {
                    UserId=12,
                    ProfilePicture = null,
                    UserName = "descheto99",
                    FirstName = "Desislava",
                    LastName = "Grigorova",
                    Email = "desi.mnt@gmail.com",
                    Password = hashedpassword,
                    Role = Role.User,
                    PhoneNumber = "0882600746",
                    Blocked = false,
                }
                ,
                new User
                {
                    UserId=13,
                    ProfilePicture = null,
                    UserName = "Blagoi78",
                    FirstName = "Blago",
                    LastName = "Ivanov",
                    Email = "blagcho@gmail.com",
                    Password = hashedpassword,
                    Role = Role.User,
                    PhoneNumber = "0876522746",
                    Blocked = false,
                }
                ,
                new User
                {
                    UserId=14,
                    ProfilePicture = null,
                    UserName = "Nataly77",
                    FirstName = "Natalia",
                    LastName = "Petrova",
                    Email = "natali99@gmail.com",
                    Password = hashedpassword,
                    Role = Role.User,
                    PhoneNumber = "0887666746",
                    Blocked = false,
                }
            };
            modelBuilder.Entity<User>().HasData(Users);
            var PassengerTrips = new List<PassengerTrip>()
            {
                new PassengerTrip
                {
                    Id=1,
                    UserId=5,
                    TripId=2
                },
                new PassengerTrip
                {
                    Id=2,
                    UserId=6,
                    TripId=2
                },
                new PassengerTrip
                {
                    Id=3,
                    UserId=7,
                    TripId=3
                },
                new PassengerTrip
                {
                    Id=4,
                    UserId=8,
                    TripId=3
                },
                new PassengerTrip
                {
                    Id=5,
                    UserId=9,
                    TripId=4
                },
                new PassengerTrip
                {
                    Id=6,
                    UserId=10,
                    TripId=5
                },
                new PassengerTrip
                {
                    Id=7,
                    UserId=11,
                    TripId=5
                },
                new PassengerTrip
                {
                    Id=8,
                    UserId=12,
                    TripId=4
                },
                new PassengerTrip
                {
                    Id=9,
                    UserId=2,
                    TripId=4
                },
                new PassengerTrip
                {
                    Id=10,
                    UserId=1,
                    TripId=2
                },
                new PassengerTrip
                {
                    Id=11,
                    UserId=13,
                    TripId=6
                },
                new PassengerTrip
                {
                    Id=12,
                    UserId=4,
                    TripId=1
                },
                new PassengerTrip
                {
                    Id=13,
                    UserId=2,
                    TripId=1
                },
                new PassengerTrip
                {
                    Id=14,
                    UserId=7,
                    TripId=14
                },
                new PassengerTrip
                {
                    Id=15,
                    UserId=8,
                    TripId=14
                },
                new PassengerTrip
                {
                    Id=16,
                    UserId=1,
                    TripId=14
                }
            };

            modelBuilder.Entity<PassengerTrip>().HasData(PassengerTrips);

            var Ratings = new List<Rating>()
            {
                new Rating
                {
                    RatingId=1,
                    RatingPoints=5,
                    Comment="I really enjoyed the ride.",
                    RatedUserId=1,
                    RatingUserId=5,
                    isDeleted=false,
                },

                new Rating
                {
                    RatingId=2,
                    RatingPoints=2,
                    Comment="I wasn't comfortable.",
                    RatedUserId=2,
                    RatingUserId=6,
                    isDeleted=false,
                },

                new Rating
                {
                    RatingId=3,
                    RatingPoints=1,
                    Comment="Didn't turn the AC on.",
                    RatedUserId=3,
                    RatingUserId=7,
                    isDeleted=false,
                },


                new Rating
                {
                    RatingId=4,
                    RatingPoints=1,
                    Comment="Constantly smoking.",
                    RatedUserId=3,
                    RatingUserId=10,
                    isDeleted=false,
                },

                new Rating
                {
                    RatingId=5,
                    RatingPoints=2,
                    Comment="He didn't stop talking.",
                    RatedUserId=3,
                    RatingUserId=12,
                    isDeleted=false,
                },

                new Rating
                {
                    RatingId=6,
                    RatingPoints=5,
                    Comment="I enjoyed the trip with this driver.",
                    RatedUserId=2,
                    RatingUserId=12,
                    isDeleted=false,
                },
                new Rating
                {
                    RatingId=7,
                    RatingPoints=5,
                    Comment="Very calm driver.",
                    RatedUserId=2,
                    RatingUserId=12,
                    isDeleted=false,
                },
                new Rating
                {
                    RatingId=8,
                    RatingPoints=5,
                    Comment="He was my passanger and I enjoyed his company",
                    RatedUserId=10,
                    RatingUserId=1,
                    isDeleted=false,
                },
                new Rating
                {
                    RatingId=9,
                    RatingPoints=2,
                    Comment="Didn't like that he was destracting me when I was driving.",
                    RatedUserId=11,
                    RatingUserId=3,
                    isDeleted=false,
                },
                new Rating
                {
                    RatingId=10,
                    RatingPoints=5,
                    Comment="Small talk all the way. I enjoyed it.",
                    RatedUserId=7,
                    RatingUserId=4,
                    isDeleted=false,
                },
                new Rating
                {
                    RatingId=11,
                    RatingPoints=5,
                    Comment="Great Driver",
                    RatedUserId=5,
                    RatingUserId=4,
                    isDeleted=false,
                },
                new Rating
                {
                    RatingId=12,
                    RatingPoints=5,
                    Comment="Would love to share a trip with him again.",
                    RatedUserId=5,
                    RatingUserId=2,
                    isDeleted=false,
                }
            };

            modelBuilder.Entity<Rating>().HasData(Ratings);
            var Trips = new List<Trip>
            {
                new Trip
                {
                    TripId=1,
                    DriverId = 1,
                    CarColor="Green",
                    CarModel = "Audi r8",
                    StartingPointId = 1,
                    EndingPointId = 3,
                    DepartureTime = DateTime.Now.AddHours(3),
                    FreeSeats = 3,
                    PetsAllowed = false,
                    SmokingAllowed = false,
                    AirConditioner = true
                },

                new Trip
                {
                    TripId=2,
                    DriverId = 2,
                    CarColor = "Blue",
                    CarModel = "Mercedes Benz",
                    StartingPointId = 5,
                    EndingPointId = 2,
                    DepartureTime = DateTime.Now.AddHours(6),
                    FreeSeats = 1,
                    PetsAllowed = true,
                    SmokingAllowed = false,
                    AirConditioner = false,
                    TripStatus = TripStatus.Canceled,
                    isDeleted = false
                },

                new Trip
                {
                    TripId=3,
                    DriverId = 3,
                    CarColor = "Yellow",
                    CarModel = "Mini Cooper",
                    StartingPointId = 3,
                    EndingPointId = 5,
                    DepartureTime = DateTime.Now.AddHours(1),
                    FreeSeats = 2,
                    PetsAllowed = false,
                    SmokingAllowed = true,
                    AirConditioner = true,
                    TripStatus = TripStatus.Active,
                },

                new Trip
                {
                    TripId=4,
                    DriverId = 4,
                    CarColor ="Silver",
                    CarModel = "Honda",
                    StartingPointId = 2,
                    EndingPointId = 1,
                    DepartureTime = DateTime.Now.AddMinutes(-30),
                    FreeSeats = 3,
                    PetsAllowed = true,
                    SmokingAllowed = true,
                    AirConditioner = true,
                    TripStatus = TripStatus.Completed,
                },
                new Trip
                {
                    TripId=5,
                    DriverId = 1,
                    CarColor="Green",
                    CarModel = "Audi r8",
                    StartingPointId = 4,
                    EndingPointId = 5,
                    DepartureTime = DateTime.Now.AddHours(3),
                    FreeSeats = 1,
                    PetsAllowed = false,
                    SmokingAllowed = true,
                    AirConditioner = true
                },
                new Trip
                {
                    TripId=6,
                    DriverId = 2,
                    CarColor="Blue",
                    CarModel = "Golf 4",
                    StartingPointId = 5,
                    EndingPointId = 4,
                    DepartureTime = DateTime.Now.AddDays(3),
                    FreeSeats = 2,
                    PetsAllowed = true,
                    SmokingAllowed = false,
                    AirConditioner = false
                },
                new Trip
                {
                    TripId=7,
                    DriverId = 3,
                    CarColor="Silver",
                    CarModel = "Honda CRV",
                    StartingPointId = 1,
                    EndingPointId = 2,
                    DepartureTime = DateTime.Now.AddHours(5),
                    FreeSeats = 1,
                    PetsAllowed = false,
                    SmokingAllowed = false,
                    AirConditioner = true
                },
                new Trip
                {
                    TripId=8,
                    DriverId = 4,
                    CarColor="Black",
                    CarModel = "Cadilac ctsv",
                    StartingPointId = 6,
                    EndingPointId = 2,
                    DepartureTime = DateTime.Now.AddHours(24),
                    FreeSeats = 3,
                    PetsAllowed = false,
                    SmokingAllowed = false,
                    AirConditioner = true
                },
                new Trip
                {
                    TripId=9,
                    DriverId = 4,
                    CarColor="Black",
                    CarModel = "Merc",
                    StartingPointId = 2,
                    EndingPointId = 4,
                    DepartureTime = DateTime.Now.AddHours(44),
                    FreeSeats = 3,
                    PetsAllowed = false,
                    SmokingAllowed = true,
                    AirConditioner = true
                },
                new Trip
                {
                    TripId=10,
                    DriverId = 2,
                    CarColor="Red",
                    CarModel = "Kia",
                    StartingPointId = 1,
                    EndingPointId = 7,
                    DepartureTime = DateTime.Now.AddHours(13),
                    FreeSeats = 2,
                    PetsAllowed = false,
                    SmokingAllowed = false,
                    AirConditioner = false
                },
                new Trip
                {
                    TripId=11,
                    DriverId = 1,
                    CarColor="Yellow",
                    CarModel = "Audi A3",
                    StartingPointId = 3,
                    EndingPointId = 1,
                    DepartureTime = DateTime.Now.AddHours(3),
                    FreeSeats = 3,
                    PetsAllowed = false,
                    SmokingAllowed = false,
                    AirConditioner = true
                },
                new Trip
                {
                    TripId=12,
                    DriverId = 3,
                    CarColor="Silver",
                    CarModel = "Opel Astra",
                    StartingPointId = 1,
                    EndingPointId = 3,
                    DepartureTime = DateTime.Now.AddHours(23),
                    FreeSeats = 3,
                    PetsAllowed = true,
                    SmokingAllowed = false,
                    AirConditioner = true
                },
                new Trip
                {
                    TripId=13,
                    DriverId = 2,
                    CarColor="White",
                    CarModel = "Toyota corolla",
                    StartingPointId = 1,
                    EndingPointId = 4,
                    DepartureTime = DateTime.Now.AddHours(66),
                    FreeSeats = 2,
                    PetsAllowed = false,
                    SmokingAllowed = false,
                    AirConditioner = true
                },
                new Trip
                {
                    TripId=14,
                    DriverId = 5,
                    CarColor="White",
                    CarModel = "Toyota corolla",
                    StartingPointId = 1,
                    EndingPointId = 4,
                    DepartureTime = DateTime.Now.AddHours(77),
                    FreeSeats = 0,
                    PetsAllowed = false,
                    SmokingAllowed = false,
                    AirConditioner = true
                },
            };

            modelBuilder.Entity<Trip>().HasData(Trips);
        }
    }
}
