﻿using CarPooling.Data.Database.Contracts;
using CarPooling.Data.Models;
using CarPooling.Data.SeedData;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace CarPooling.Data.Database
{
    public class CarPoolingDbContext : DbContext, IDatabase
    {
        public CarPoolingDbContext(DbContextOptions<CarPoolingDbContext> options)
           : base(options)
        {

        }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Trip> Trips { get; set; }
        public virtual DbSet<Message> Messages { get; set; }
        public virtual DbSet<Rating> Ratings { get; set; }
        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<PassengerTrip> PassangerTrips { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Address>()
               .HasOne(s => s.City)
               .WithMany()
               .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<Address>()
              .HasOne(s => s.UsersUses)
              .WithMany(x => x.UsedAddresses)
              .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Rating>()
               .HasOne(x => x.RatedUser)
               .WithMany(x => x.RecievedRatings)
               .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Rating>()
               .HasOne(x => x.RatingUser)
               .WithMany(x => x.GivenRatings)
               .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Trip>()
               .HasOne(s => s.Driver)
               .WithMany(s => s.TripsAsDriver)
               .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Trip>()
               .HasMany(x => x.Passangers);

            modelBuilder.Entity<Trip>()
              .HasMany(x => x.CandidatesForTrip);

            modelBuilder.Entity<Trip>()
              .HasOne(x => x.StartingPoint)
              .WithMany()
              .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Trip>()
              .HasOne(x => x.EndingPoint)
              .WithMany()
              .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Trip>()
                .HasMany(x => x.CandidatesForTrip)
                .WithOne(x => x.AppliedTrip)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<User>()
                .HasMany(x => x.TripsAsDriver)
                .WithOne(x => x.Driver)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<PassengerTrip>()
              .HasOne(x => x.Passanger)
              .WithMany(x => x.TripsAsPassanger)
              .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<User>().Property<bool>("isDeleted");
            modelBuilder.Entity<User>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            modelBuilder.Entity<Rating>().Property<bool>("isDeleted");
            modelBuilder.Entity<Rating>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            modelBuilder.Entity<Message>().Property<bool>("isDeleted");
            modelBuilder.Entity<Message>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            modelBuilder.Entity<Trip>().Property<bool>("isDeleted");
            modelBuilder.Entity<Trip>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            modelBuilder.Entity<Address>().Property<bool>("isDeleted");
            modelBuilder.Entity<Address>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            modelBuilder.Entity<PassengerTrip>().Property<bool>("isDeleted");
            modelBuilder.Entity<PassengerTrip>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            modelBuilder.Seed();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseSqlServer(
                    @"Server=.;Database=CarPooling;Trusted_Connection=True",
                    o => o.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery));
        }

        public override int SaveChanges()
        {
            UpdateSoftDeleteStatuses();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            UpdateSoftDeleteStatuses();
            return base.SaveChangesAsync(cancellationToken);
        }

        private void UpdateSoftDeleteStatuses()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                if (entry.GetType().GetProperty("isDeleted") == null)
                {
                    continue;
                }
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.CurrentValues["isDeleted"] = false;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Modified;
                        entry.CurrentValues["isDeleted"] = true;
                        break;
                }
            }
        }
    }
}
