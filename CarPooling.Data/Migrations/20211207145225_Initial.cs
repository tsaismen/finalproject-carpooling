﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace CarPooling.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cities",
                columns: table => new
                {
                    CityId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Latitude = table.Column<double>(type: "float", nullable: false),
                    Longitude = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cities", x => x.CityId);
                });

            migrationBuilder.CreateTable(
                name: "Trips",
                columns: table => new
                {
                    TripId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DriverId = table.Column<int>(type: "int", nullable: false),
                    CarColor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CarModel = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StartingPointId = table.Column<int>(type: "int", nullable: false),
                    EndingPointId = table.Column<int>(type: "int", nullable: false),
                    DepartureTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FreeSeats = table.Column<int>(type: "int", nullable: false),
                    PetsAllowed = table.Column<bool>(type: "bit", nullable: false),
                    SmokingAllowed = table.Column<bool>(type: "bit", nullable: false),
                    AirConditioner = table.Column<bool>(type: "bit", nullable: false),
                    TripStatus = table.Column<int>(type: "int", nullable: false),
                    isDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trips", x => x.TripId);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProfilePicture = table.Column<byte[]>(type: "varbinary(max)", nullable: true),
                    UserName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    FirstName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", maxLength: 2147483647, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Role = table.Column<int>(type: "int", nullable: false),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Blocked = table.Column<bool>(type: "bit", nullable: false),
                    AppliedTripId = table.Column<int>(type: "int", nullable: true),
                    isDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_Users_Trips_AppliedTripId",
                        column: x => x.AppliedTripId,
                        principalTable: "Trips",
                        principalColumn: "TripId",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "Addresses",
                columns: table => new
                {
                    AddressId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Street = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    CityId = table.Column<int>(type: "int", nullable: false),
                    UserUsesId = table.Column<int>(type: "int", nullable: false),
                    isDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.AddressId);
                    table.ForeignKey(
                        name: "FK_Addresses_Cities_CityId",
                        column: x => x.CityId,
                        principalTable: "Cities",
                        principalColumn: "CityId");
                    table.ForeignKey(
                        name: "FK_Addresses_Users_UserUsesId",
                        column: x => x.UserUsesId,
                        principalTable: "Users",
                        principalColumn: "UserId");
                });

            migrationBuilder.CreateTable(
                name: "Messages",
                columns: table => new
                {
                    MessageId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    When = table.Column<DateTime>(type: "datetime2", nullable: false),
                    isDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Messages", x => x.MessageId);
                    table.ForeignKey(
                        name: "FK_Messages_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PassangerTrips",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    TripId = table.Column<int>(type: "int", nullable: false),
                    isDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PassangerTrips", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PassangerTrips_Trips_TripId",
                        column: x => x.TripId,
                        principalTable: "Trips",
                        principalColumn: "TripId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PassangerTrips_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId");
                });

            migrationBuilder.CreateTable(
                name: "Ratings",
                columns: table => new
                {
                    RatingId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RatingPoints = table.Column<int>(type: "int", nullable: false),
                    Comment = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RatedUserId = table.Column<int>(type: "int", nullable: false),
                    RatingUserId = table.Column<int>(type: "int", nullable: false),
                    isDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ratings", x => x.RatingId);
                    table.ForeignKey(
                        name: "FK_Ratings_Users_RatedUserId",
                        column: x => x.RatedUserId,
                        principalTable: "Users",
                        principalColumn: "UserId");
                    table.ForeignKey(
                        name: "FK_Ratings_Users_RatingUserId",
                        column: x => x.RatingUserId,
                        principalTable: "Users",
                        principalColumn: "UserId");
                });

            migrationBuilder.InsertData(
                table: "Cities",
                columns: new[] { "CityId", "Latitude", "Longitude", "Name" },
                values: new object[,]
                {
                    { 1, 42.698334000000003, 23.319941, "Sofia" },
                    { 21, 43.990000000000002, 22.872499999999999, "Vidin" },
                    { 20, 43.813890000000001, 23.23611, "Lom" },
                    { 18, 42.600000000000001, 23.033329999999999, "Pernik" },
                    { 16, 43.416670000000003, 28.16667, "Balchik" },
                    { 15, 41.840423999999999, 23.485652999999999, "Bansko" },
                    { 14, 42.874720000000003, 25.33417, "Gabrovo" },
                    { 13, 43.081240000000001, 25.62904, "Veliko Tarnovo" },
                    { 12, 43.416670000000003, 24.616669999999999, "Pleven" },
                    { 19, 41.399999999999999, 23.216670000000001, "Petrich" },
                    { 10, 43.210000000000001, 23.5625, "Vratsa" },
                    { 9, 43.236109999999996, 23.125830000000001, "Berkovitsa" },
                    { 8, 42.099400000000003, 27.9407, "Ahtopol" },
                    { 7, 43.204666000000003, 27.910543000000001, "Varna" },
                    { 5, 42.416670000000003, 27.699999999999999, "Sozopol" },
                    { 4, 42.510578000000002, 27.461013999999999, "Burgas" },
                    { 3, 43.412500000000001, 23.225000000000001, "Montana" },
                    { 2, 42.149999999999999, 24.75, "Plovdiv" },
                    { 11, 42.016669999999998, 23.100000000000001, "Blagoevgrad" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserId", "AppliedTripId", "Blocked", "Email", "FirstName", "LastName", "Password", "PhoneNumber", "ProfilePicture", "Role", "UserName", "isDeleted" },
                values: new object[,]
                {
                    { 12, null, false, "desi.mnt@gmail.com", "Desislava", "Grigorova", "AQAAAAEAACcQAAAAEO8aqik6LMkx3zkcYI2V6W/HaWMIiBUUOuax9dJuDkAZ80zfl/WFmJgAguBVI4+x2A==", "0882600746", null, 0, "descheto99", false },
                    { 11, null, false, "kircata@gmail.com", "Kiril", "Stanoev", "AQAAAAEAACcQAAAAEO8aqik6LMkx3zkcYI2V6W/HaWMIiBUUOuax9dJuDkAZ80zfl/WFmJgAguBVI4+x2A==", "0887113746", null, 1, "Kircata", false },
                    { 10, null, false, "elena.barca@gmail.com", "Elena", "Shtereva", "AQAAAAEAACcQAAAAEO8aqik6LMkx3zkcYI2V6W/HaWMIiBUUOuax9dJuDkAZ80zfl/WFmJgAguBVI4+x2A==", "0883322746", null, 0, "Elencheto", false },
                    { 9, null, false, "georgi.petrov@gmail.com", "Georgi", "Petrov", "AQAAAAEAACcQAAAAEO8aqik6LMkx3zkcYI2V6W/HaWMIiBUUOuax9dJuDkAZ80zfl/WFmJgAguBVI4+x2A==", "0887192746", null, 0, "Gopeto98", false },
                    { 8, null, false, "anton.staykov@gmail.com", "Anton", "Staykov", "AQAAAAEAACcQAAAAEO8aqik6LMkx3zkcYI2V6W/HaWMIiBUUOuax9dJuDkAZ80zfl/WFmJgAguBVI4+x2A==", "0887611746", null, 0, "Antoncho22", false },
                    { 7, null, false, "mi6ko@gmail.com", "Mihail", "Matov", "AQAAAAEAACcQAAAAEO8aqik6LMkx3zkcYI2V6W/HaWMIiBUUOuax9dJuDkAZ80zfl/WFmJgAguBVI4+x2A==", "08842421122", null, 1, "Mi6ko", false },
                    { 3, null, false, "ivchopivcho@gmail.com", "Ivailo", "Vasilov", "AQAAAAEAACcQAAAAEO8aqik6LMkx3zkcYI2V6W/HaWMIiBUUOuax9dJuDkAZ80zfl/WFmJgAguBVI4+x2A==", "08966222746", null, 0, "Ivailo347", false },
                    { 5, null, false, "Krasko404@gmail.com", "Krasimir", "Georgiev", "AQAAAAEAACcQAAAAEO8aqik6LMkx3zkcYI2V6W/HaWMIiBUUOuax9dJuDkAZ80zfl/WFmJgAguBVI4+x2A==", "08876222746", null, 0, "KrasiRoka", false },
                    { 4, null, false, "babavitonka@gmail.com", "Tonka", "Miteva", "AQAAAAEAACcQAAAAEO8aqik6LMkx3zkcYI2V6W/HaWMIiBUUOuax9dJuDkAZ80zfl/WFmJgAguBVI4+x2A==", "08876259746", null, 0, "BabaTonka22", false },
                    { 2, null, false, "aspmladenov@gmail.com", "Asparuh", "Mladenov", "AQAAAAEAACcQAAAAEO8aqik6LMkx3zkcYI2V6W/HaWMIiBUUOuax9dJuDkAZ80zfl/WFmJgAguBVI4+x2A==", "08875322746", null, 1, "Mladenov99", false },
                    { 1, null, false, "gercata@gmail.com", "Gerasim", "Petkov", "AQAAAAEAACcQAAAAEO8aqik6LMkx3zkcYI2V6W/HaWMIiBUUOuax9dJuDkAZ80zfl/WFmJgAguBVI4+x2A==", "08876222746", null, 0, "Gero55", false },
                    { 13, null, false, "blagcho@gmail.com", "Blago", "Ivanov", "AQAAAAEAACcQAAAAEO8aqik6LMkx3zkcYI2V6W/HaWMIiBUUOuax9dJuDkAZ80zfl/WFmJgAguBVI4+x2A==", "0876522746", null, 0, "Blagoi78", false },
                    { 6, null, false, "fritez@abv.bg", "Miroslav", "Petrov", "AQAAAAEAACcQAAAAEO8aqik6LMkx3zkcYI2V6W/HaWMIiBUUOuax9dJuDkAZ80zfl/WFmJgAguBVI4+x2A==", "0887622274", null, 1, "Fritez", false },
                    { 14, null, false, "natali99@gmail.com", "Natalia", "Petrova", "AQAAAAEAACcQAAAAEO8aqik6LMkx3zkcYI2V6W/HaWMIiBUUOuax9dJuDkAZ80zfl/WFmJgAguBVI4+x2A==", "0887666746", null, 0, "Nataly77", false }
                });

            migrationBuilder.InsertData(
                table: "Addresses",
                columns: new[] { "AddressId", "CityId", "Street", "UserUsesId", "isDeleted" },
                values: new object[,]
                {
                    { 1, 1, "Opalchenska 78", 1, false },
                    { 13, 2, "Tsarigradsko Shose 55", 5, false },
                    { 10, 12, "Evlogi Georgiev 88", 5, false },
                    { 8, 1, "Solun 99", 5, false },
                    { 6, 2, "Mladost 2 bl. 256", 5, false },
                    { 5, 5, "Stamen Iliev 2b", 5, false },
                    { 4, 3, "Mecha Polqna 78", 4, false },
                    { 11, 14, "Gen. Totleben 54", 5, false },
                    { 12, 1, "Patriarh Evtimii 34", 2, false },
                    { 9, 2, "Alexander Stamboliiski 44", 2, false },
                    { 2, 5, "Hristo Botev 78", 2, false },
                    { 14, 4, "Vasil Levski 66", 1, false },
                    { 7, 7, "Danail Nikolaev 22", 1, false },
                    { 3, 4, "Kriv Zavoi 78", 3, false }
                });

            migrationBuilder.InsertData(
                table: "Ratings",
                columns: new[] { "RatingId", "Comment", "RatedUserId", "RatingPoints", "RatingUserId", "isDeleted" },
                values: new object[,]
                {
                    { 5, "He didn't stop talking.", 3, 2, 12, false },
                    { 9, "Didn't like that he was destracting me when I was driving.", 11, 2, 3, false },
                    { 8, "He was my passanger and I enjoyed his company", 10, 5, 1, false },
                    { 4, "Constantly smoking.", 3, 1, 10, false },
                    { 10, "Small talk all the way. I enjoyed it.", 7, 5, 4, false },
                    { 12, "Would love to share a trip with him again.", 5, 5, 2, false },
                    { 2, "I wasn't comfortable.", 2, 2, 6, false },
                    { 11, "Great Driver", 5, 5, 4, false },
                    { 1, "I really enjoyed the ride.", 1, 5, 5, false },
                    { 6, "I enjoyed the trip with this driver.", 2, 5, 12, false },
                    { 3, "Didn't turn the AC on.", 3, 1, 7, false },
                    { 7, "Very calm driver.", 2, 5, 12, false }
                });

            migrationBuilder.InsertData(
                table: "Trips",
                columns: new[] { "TripId", "AirConditioner", "CarColor", "CarModel", "DepartureTime", "DriverId", "EndingPointId", "FreeSeats", "PetsAllowed", "SmokingAllowed", "StartingPointId", "TripStatus", "isDeleted" },
                values: new object[,]
                {
                    { 10, false, "Red", "Kia", new DateTime(2021, 12, 8, 5, 52, 24, 280, DateTimeKind.Local).AddTicks(3742), 2, 7, 2, false, false, 1, 0, false },
                    { 4, true, "Silver", "Honda", new DateTime(2021, 12, 7, 16, 22, 24, 280, DateTimeKind.Local).AddTicks(3699), 4, 1, 3, true, true, 2, 2, false },
                    { 7, true, "Silver", "Honda CRV", new DateTime(2021, 12, 7, 21, 52, 24, 280, DateTimeKind.Local).AddTicks(3730), 3, 2, 1, false, false, 1, 0, false },
                    { 1, true, "Green", "Audi r8", new DateTime(2021, 12, 7, 19, 52, 24, 275, DateTimeKind.Local).AddTicks(4756), 1, 3, 3, false, false, 1, 0, false },
                    { 11, true, "Yellow", "Audi A3", new DateTime(2021, 12, 7, 19, 52, 24, 280, DateTimeKind.Local).AddTicks(3746), 1, 1, 3, false, false, 3, 0, false },
                    { 12, true, "Silver", "Opel Astra", new DateTime(2021, 12, 8, 15, 52, 24, 280, DateTimeKind.Local).AddTicks(3749), 3, 3, 3, true, false, 1, 0, false },
                    { 9, true, "Black", "Merc", new DateTime(2021, 12, 9, 12, 52, 24, 280, DateTimeKind.Local).AddTicks(3737), 4, 4, 3, false, true, 2, 0, false },
                    { 13, true, "White", "Toyota corolla", new DateTime(2021, 12, 10, 10, 52, 24, 280, DateTimeKind.Local).AddTicks(3753), 2, 4, 2, false, false, 1, 0, false },
                    { 14, true, "White", "Toyota corolla", new DateTime(2021, 12, 10, 21, 52, 24, 280, DateTimeKind.Local).AddTicks(3756), 5, 4, 0, false, false, 1, 0, false },
                    { 2, false, "Blue", "Mercedes Benz", new DateTime(2021, 12, 7, 22, 52, 24, 280, DateTimeKind.Local).AddTicks(3203), 2, 2, 1, true, false, 5, 1, false },
                    { 3, true, "Yellow", "Mini Cooper", new DateTime(2021, 12, 7, 17, 52, 24, 280, DateTimeKind.Local).AddTicks(3685), 3, 5, 2, false, true, 3, 0, false },
                    { 5, true, "Green", "Audi r8", new DateTime(2021, 12, 7, 19, 52, 24, 280, DateTimeKind.Local).AddTicks(3712), 1, 5, 1, false, true, 4, 0, false },
                    { 6, false, "Blue", "Golf 4", new DateTime(2021, 12, 10, 16, 52, 24, 280, DateTimeKind.Local).AddTicks(3721), 2, 4, 2, true, false, 5, 0, false },
                    { 8, true, "Black", "Cadilac ctsv", new DateTime(2021, 12, 8, 16, 52, 24, 280, DateTimeKind.Local).AddTicks(3733), 4, 2, 3, false, false, 6, 0, false }
                });

            migrationBuilder.InsertData(
                table: "PassangerTrips",
                columns: new[] { "Id", "TripId", "UserId", "isDeleted" },
                values: new object[,]
                {
                    { 5, 4, 9, false },
                    { 8, 4, 12, false },
                    { 9, 4, 2, false },
                    { 12, 1, 4, false },
                    { 13, 1, 2, false },
                    { 14, 14, 7, false },
                    { 15, 14, 8, false },
                    { 16, 14, 1, false },
                    { 1, 2, 5, false },
                    { 2, 2, 6, false },
                    { 10, 2, 1, false },
                    { 3, 3, 7, false },
                    { 4, 3, 8, false },
                    { 6, 5, 10, false },
                    { 7, 5, 11, false },
                    { 11, 6, 13, false }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_CityId",
                table: "Addresses",
                column: "CityId");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_UserUsesId",
                table: "Addresses",
                column: "UserUsesId");

            migrationBuilder.CreateIndex(
                name: "IX_Messages_UserId",
                table: "Messages",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_PassangerTrips_TripId",
                table: "PassangerTrips",
                column: "TripId");

            migrationBuilder.CreateIndex(
                name: "IX_PassangerTrips_UserId",
                table: "PassangerTrips",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Ratings_RatedUserId",
                table: "Ratings",
                column: "RatedUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Ratings_RatingUserId",
                table: "Ratings",
                column: "RatingUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Trips_DriverId",
                table: "Trips",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_Trips_EndingPointId",
                table: "Trips",
                column: "EndingPointId");

            migrationBuilder.CreateIndex(
                name: "IX_Trips_StartingPointId",
                table: "Trips",
                column: "StartingPointId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_AppliedTripId",
                table: "Users",
                column: "AppliedTripId");

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_Addresses_EndingPointId",
                table: "Trips",
                column: "EndingPointId",
                principalTable: "Addresses",
                principalColumn: "AddressId");

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_Addresses_StartingPointId",
                table: "Trips",
                column: "StartingPointId",
                principalTable: "Addresses",
                principalColumn: "AddressId");

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_Users_DriverId",
                table: "Trips",
                column: "DriverId",
                principalTable: "Users",
                principalColumn: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Addresses_Cities_CityId",
                table: "Addresses");

            migrationBuilder.DropForeignKey(
                name: "FK_Addresses_Users_UserUsesId",
                table: "Addresses");

            migrationBuilder.DropForeignKey(
                name: "FK_Trips_Users_DriverId",
                table: "Trips");

            migrationBuilder.DropTable(
                name: "Messages");

            migrationBuilder.DropTable(
                name: "PassangerTrips");

            migrationBuilder.DropTable(
                name: "Ratings");

            migrationBuilder.DropTable(
                name: "Cities");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Trips");

            migrationBuilder.DropTable(
                name: "Addresses");
        }
    }
}
