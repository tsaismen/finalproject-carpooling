﻿using CarPooling.Data.Models;
using System;

namespace CarPooling.Service.ResponseModels
{
    public class RatingResponseModel
    {
        public RatingResponseModel(Rating rating)
        {
            this.RatingId = rating.RatingId;
            this.RatingPoints = rating.RatingPoints;
            this.Comment = rating.Comment;
            this.RatedUserId = rating.RatedUserId;
            this.RatedUser = rating.RatedUser.UserName;
            this.RatingUserId = rating.RatingUserId;
            this.RatingUser = rating.RatingUser.UserName;
            this.RatingUserProfilePic = rating.RatingUser.ProfilePicture == null ? null : string.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(rating.RatingUser.ProfilePicture));
        }
        public int RatingId { get; set; }
        public int RatingPoints { get; set; }
        public string Comment { get; set; }
        public int RatedUserId { get; set; }
        public string RatedUser { get; set; }
        public int RatingUserId { get; set; }
        public string RatingUser { get; set; }
        public string RatingUserProfilePic { get; set; }
    }
}
