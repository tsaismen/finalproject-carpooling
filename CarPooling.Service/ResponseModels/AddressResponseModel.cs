﻿using CarPooling.Data.Models;

namespace CarPooling.Service.ResponseModels
{
    public class AddressResponseModel
    {
        public AddressResponseModel(Address address)
        {
            this.AddressId = address.AddressId;
            this.Street = address.Street;
            this.City = address.City.Name;
        }

        public int AddressId { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
    }
}
