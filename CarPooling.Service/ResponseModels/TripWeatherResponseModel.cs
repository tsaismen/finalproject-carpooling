﻿namespace CarPooling.Service.ResponseModels
{
    public class TripWeatherResponseModel
    {
        public string Temperature { get; set; }
        public string Date { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public string Visibility { get; set; }
        public string CityName { get; set; }
        public string CountryCode { get; set; }
    }

}
