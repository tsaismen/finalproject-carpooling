﻿using CarPooling.Service.Constants;
using System.Collections.Generic;

namespace CarPooling.Service.ResponseModels
{
    public class DataTablePaginator<T>
    {
        public DataTablePaginator(IEnumerable<T> items,
            int CurrentPage,
            int totalDisplayRecords)
        {
            Data = items;
            this.CurrentPage = CurrentPage;
            TotalItems = totalDisplayRecords;
        }
        public IEnumerable<T> Data { get; set; }

        public int CurrentPage { get; set; }
        public int TotalItems { get; set; }
        public int Take { get; set; } = Pagin.Take;
    }
}
