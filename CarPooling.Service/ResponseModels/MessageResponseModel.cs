﻿using CarPooling.Data.Models;
using System;

namespace CarPooling.Service.ResponseModels
{
    public class MessageResponseModel
    {
        public MessageResponseModel()
        {

        }
        public MessageResponseModel(Message message)
        {
            this.MessageId = message.MessageId;
            this.UserId = message.UserId;
            this.UserName = message.User.UserName;
            this.ProfilePicture = message.User.ProfilePicture == null ? null : string.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(message.User.ProfilePicture));
            this.Content = message.Content;
            this.When = message.When;
        }
        public int MessageId { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string ProfilePicture { get; set; }
        public string Content { get; set; }
        public DateTime When { get; set; }
    }
}
