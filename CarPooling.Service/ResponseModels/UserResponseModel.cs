﻿using CarPooling.Data.Models;
using System;
using System.Linq;
using System.Text.Json.Serialization;

namespace CarPooling.Service.ResponseModels
{
    public class UserResponseModel
    {
        public UserResponseModel(User user)
        {
            this.UserId = user.UserId;
            this.ProfilePicture = user.ProfilePicture == null ? null : string.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(user.ProfilePicture));
            this.UserName = user.UserName;
            this.FirstName = user.FirstName;
            this.LastName = user.LastName;
            this.Email = user.Email;
            this.Role = user.Role.ToString();
            this.AppliedTripId = user.AppliedTripId;
            this.PhoneNumber = user.PhoneNumber;
            this.Blocked = user.Blocked;
            this.RatingPoints = user.CalcRating();
            this.TripAsDriver = user.TripsAsDriver.Count();
            this.TripsAsPassanger = user.TripsAsPassanger.Count();
        }

        public int UserId { get; set; }
        [JsonIgnore]
        public string ProfilePicture { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public string PhoneNumber { get; set; }
        public double RatingPoints { get; set; }
        public int? AppliedTripId { get; set; }
        public int TripAsDriver { get; set; }
        public int TripsAsPassanger { get; set; }
        public bool Blocked { get; set; }

    }
}