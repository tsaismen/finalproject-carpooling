﻿using CarPooling.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace CarPooling.Service.ResponseModels
{
    public class TripResponseModel
    {
        public TripResponseModel(Trip tripModel)
        {
            this.TripId = tripModel.TripId;
            this.DriverId = tripModel.DriverId;
            this.DriverUserName = tripModel.Driver.UserName;
            this.DriverName = $"{tripModel.Driver.FirstName} {tripModel.Driver.LastName}";
            this.DriverPhone = tripModel.Driver.PhoneNumber;
            this.CarColor = tripModel.CarColor.ToString();
            this.CarModel = tripModel.CarModel;
            this.StartingPoint = tripModel.StartingPoint.ToString();
            this.EndingPoint = tripModel.EndingPoint.ToString();
            this.DepartureDate = tripModel.DepartureTime.ToString("dd/MM/yyyy");
            this.DepartureTime = tripModel.DepartureTime.ToString("HH:mm");
            this.FreeSeats = tripModel.FreeSeats;
            this.PetsAllowed = tripModel.PetsAllowed;
            this.SmokingAllowed = tripModel.SmokingAllowed;
            this.AirConditioner = tripModel.AirConditioner;
            this.TripStatus = tripModel.TripStatus.ToString();
            this.Passangers = tripModel.Passangers
                .Where(x => x.TripId.Equals(TripId))
                .Select(x => x.Passanger)
                .Select(x => new UserResponseModel(x)).ToList();
            this.CandidatesForTrip = tripModel.CandidatesForTrip
                .Select(x => new UserResponseModel(x)).ToList();
        }
        public int TripId { get; }
        public int DriverId { get; }
        public string DriverUserName { get; set; }
        public string DriverName { get; }
        public string DriverPhone { get; }
        public string CarColor { get; }
        public string CarModel { get; }
        public string StartingPoint { get; }
        public string EndingPoint { get; }
        public string DepartureDate { get; }
        public string DepartureTime { get; }
        public int FreeSeats { get; }
        public bool PetsAllowed { get; }
        public bool SmokingAllowed { get; }
        public bool AirConditioner { get; }
        public string TripStatus { get; }
        public List<UserResponseModel> Passangers { get; }
        public List<UserResponseModel> CandidatesForTrip { get; }
    }
}
