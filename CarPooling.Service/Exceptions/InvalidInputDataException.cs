﻿using System;

namespace CarPooling.Services.Exceptions
{
    public class InvalidInputDataException : ApplicationException
    {
        public InvalidInputDataException(string message)
            : base(message)
        {
        }
    }
}
