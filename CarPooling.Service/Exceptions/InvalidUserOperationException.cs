﻿using System;

namespace CarPooling.Services.Exceptions
{
    public class InvalidUserOperationException : ApplicationException
    {
        public InvalidUserOperationException(string message)
            : base(message)
        {
        }
    }
}
