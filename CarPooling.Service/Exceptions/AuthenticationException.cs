﻿using System;

namespace CarPooling.Services.Exceptions
{
    public class AuthenticationException : ApplicationException
    {
        public AuthenticationException(string message)
            : base(message)
        {

        }
    }
}
