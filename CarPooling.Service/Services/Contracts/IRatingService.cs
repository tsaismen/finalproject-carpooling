﻿using CarPooling.Data.Models;
using CarPooling.Service.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarPooling.Service.Services.Contracts
{
    public interface IRatingService
    {
        Task<Rating> GetByIdAsync(int id);
        Task<IEnumerable<RatingResponseModel>> GetAllAsync();
        Task<Rating> CreateAsync(Rating rating);
        Task<Rating> UpdateAsync(int id, Rating incomingRating);
        Task<Rating> DeleteAsync(int id);
        Task<DataTablePaginator<RatingResponseModel>> GetPaginatedRatingForUser(int userId, int page);
    }
}
