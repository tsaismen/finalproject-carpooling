﻿using System.Threading.Tasks;

namespace CarPooling.Service.Services.Contracts
{
    public interface IHelperService
    {
        Task<int> CitiesCount();
        Task<int> UsersCount();
        Task<int> PositiveRatingsCount();
        Task<int> TripsCount();
    }
}