﻿using CarPooling.Data.Models;
using CarPooling.Service.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarPooling.Service.Services.Contracts
{
    public interface IUserService
    {
        Task<User> GetByIdAsync(int id);
        Task<IEnumerable<UserResponseModel>> GetAllAsync();
        Task<int> GetCountAsync();
        Task<IEnumerable<UserResponseModel>> GetByEmailAsync(string email);
        Task<User> GetSingleUserByEmailAsync(string email);
        Task<IEnumerable<UserResponseModel>> GetByUsernameAsync(string username);
        Task<IEnumerable<UserResponseModel>> GetByPhoneNumberAsync(string phoneNumber);
        Task<IEnumerable<UserResponseModel>> SearchAllFieldsAsync(string searchWord);
        Task<IEnumerable<Rating>> GetRecievedRatingAsync(int id);
        Task<IEnumerable<Rating>> GetGivenRatingAsync(int id);
        Task<User> CreateAsync(User user);
        Task<User> UpdateAsync(int id, User user);
        Task<User> UpdatePassword(int userId, string oldPassword, string newPassword);
        Task<bool> DeleteAsync(int id);
        Task<IEnumerable<UserResponseModel>> GetTopOrganizersAsync();
        Task<IEnumerable<UserResponseModel>> GetTopPassangersAsync();
        Task<User> BlockAsync(int id);
        Task<User> UnblockAsync(int id);
        Task<TripResponseModel> ApplyForTrip(int userId, int tripId);
        Task<TripResponseModel> LeaveFromCandidates(int userId, int tripId);
        Task<TripResponseModel> LeaveTrip(int userId, int tripId);
        Task<IEnumerable<AddressResponseModel>> TakeUserAddresses(int userId);
        Task<DataTablePaginator<UserResponseModel>> GetAllPaginatedUsersAsync(int page);

        Task<DataTablePaginator<UserResponseModel>> FilterBySearchword(string searchWord);
    }
}
