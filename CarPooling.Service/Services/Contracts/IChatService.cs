﻿using CarPooling.Data.Models;
using CarPooling.Service.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarPooling.Service.Services.Contracts
{
    public interface IChatService
    {
        Task<IEnumerable<MessageResponseModel>> GetAllMessagesAsync();
        Task<bool> CreateMessageAsync(Message message);
    }
}
