﻿using CarPooling.Data.Models;
using CarPooling.Service.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarPooling.Service.Services.Contracts
{
    public interface ITripService
    {
        Task<Trip> GetByIdAsync(int id);
        Task<IEnumerable<TripResponseModel>> GetAllAsync();
        Task<IEnumerable<TripResponseModel>> GetAllActiveAsync();
        Task<IEnumerable<TripResponseModel>> GetAllCompletedAsync();
        Task<Trip> CreateAsync(Trip tripModel);
        Task<Trip> UpdateAsync(int id, Trip incomingTrip);
        Task<TripResponseModel> ChangeTripStatus(int tripId, string newStatus);
        Task<bool> DeleteAsync(int id);
        Task<IEnumerable<TripResponseModel>> GetTripsByPassangerId(int passangerId);
        Task<IEnumerable<TripResponseModel>> GetTripsByDriverId(int driverId);
        Task<TripResponseModel> ApproveCandidatesAsync(int tripId, int candidateId);
        Task<TripResponseModel> DeclineCandidatesAsync(int tripId, int candidateId);
        Task<TripResponseModel> RemovePassanger(int tripId, int passangerId);
        Task<DataTablePaginator<TripResponseModel>> GetAllPaginatedTripsAsync(int page);
        Task<DataTablePaginator<TripResponseModel>> GetPaginatedTripsByDriverId(int page, int driverId);
        Task<DataTablePaginator<TripResponseModel>> GetPaginatedTripsByPassangerId(int page, int passangerId);
        Task<DataTablePaginator<TripResponseModel>> FilterByDestination(string destination);
        Task<TripWeatherResponseModel> GetWeather(Trip trip);
    }
}
