﻿using CarPooling.Data.Models;
using CarPooling.Service.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarPooling.Service.Services.Contracts
{
    public interface IAddressService
    {
        Task<AddressResponseModel> GetByIdAsync(int id);
        Task<IEnumerable<AddressResponseModel>> GetAllAsync();
        Task<IEnumerable<City>> GetAllCitiesAsync();
        Task<Address> CreateAsync(Address address);
        Task<AddressResponseModel> UpdateAsync(int id, Address address);
        Task<bool> DeleteAsync(int id);
    }
}
