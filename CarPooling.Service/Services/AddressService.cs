﻿using CarPooling.Data.Database.Contracts;
using CarPooling.Data.Models;
using CarPooling.Service.Constants;
using CarPooling.Service.ResponseModels;
using CarPooling.Service.Services.Contracts;
using CarPooling.Services.Exceptions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Service.Services
{
    public class AddressService : IAddressService
    {
        private readonly IDatabase database;

        public AddressService(IDatabase database)
        {
            this.database = database;
        }

        private IQueryable<Address> Query
            => this.database.Addresses
            .Include(x => x.City);

        public virtual async Task<AddressResponseModel> GetByIdAsync(int id)
        {
            var addresses = await Query
                .Where(x => x.AddressId == id)
                .Select(x => new AddressResponseModel(x))
                .FirstOrDefaultAsync()
                ?? throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(Address)));

            return addresses;
        }

        public async Task<IEnumerable<AddressResponseModel>> GetAllAsync()
        {
            var addresses = await Query
                .Select(x => new AddressResponseModel(x))
                .ToListAsync();

            return addresses;
        }
        public async Task<IEnumerable<City>> GetAllCitiesAsync()
        {
            var addresses = await this.database.Cities
                .ToListAsync();

            return addresses;
        }

        public async Task<Address> CreateAsync(Address address)
        {
            if (this.database.Addresses.Any(x => x.Street.ToLower().Equals(address.Street.ToLower())))
            {
                throw new DuplicateEntityException(string.Format(ExceptionMessages.Duplicate, nameof(Address), address.Street));
            }

            await this.database.Addresses.AddAsync(address);

            await this.database.SaveChangesAsync();

            return address;
        }

        public async Task<AddressResponseModel> UpdateAsync(int id, Address address)
        {
            if (address == null)
                throw new InvalidInputDataException(string.Format(ExceptionMessages.NullObject, nameof(Address)));

            var addressToBeUpdated = this.database.Addresses
                .FirstOrDefault(x => x.AddressId == id);

            addressToBeUpdated.Street = address.Street;
            addressToBeUpdated.CityId = address.CityId;
            addressToBeUpdated.isDeleted = address.isDeleted;

            await this.database.SaveChangesAsync();

            return new AddressResponseModel(address);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var addressToBeDeleted = await Query
                .FirstOrDefaultAsync(x => x.AddressId == id)
                ?? throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(Address)));

            this.database.Addresses.Remove(addressToBeDeleted);
            await this.database.SaveChangesAsync();

            return true;
        }
    }
}
