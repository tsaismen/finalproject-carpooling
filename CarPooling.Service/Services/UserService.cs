﻿using CarPooling.Data.Database.Contracts;
using CarPooling.Data.Models;
using CarPooling.Service.Constants;
using CarPooling.Service.ResponseModels;
using CarPooling.Service.Services.Contracts;
using CarPooling.Service.Tools;
using CarPooling.Services.Exceptions;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Service.Services
{
    public class UserService : IUserService
    {
        private readonly IDatabase database;
        private readonly Validator validator;

        public UserService(IDatabase database, Validator validator)
        {
            this.database = database;
            this.validator = validator;
        }

        private IQueryable<User> Querry
            => this.database.Users
            .Include(x => x.GivenRatings)
            .Include(x => x.RecievedRatings)
            .Include(x => x.TripsAsDriver)
            .Include(x => x.TripsAsPassanger)
            .Include(x => x.UsedAddresses);

        public async Task<User> GetByIdAsync(int id)
        {
            var user = await Querry
                .Where(x => x.UserId == id)
                .FirstOrDefaultAsync()
                ?? throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(User)));

            return user;
        }

        public async Task<IEnumerable<UserResponseModel>> GetAllAsync()
        {
            var users = await Querry
                .Select(x => new UserResponseModel(x))
                .ToListAsync();

            return users;
        }

        public async Task<IEnumerable<UserResponseModel>> GetByEmailAsync(string email)
        {
            var users = await Querry
                .Where(x => x.Email.ToLower().Contains(email.ToLower()))
                .Select(x => new UserResponseModel(x))
                .ToListAsync();

            return users;
        }

        public async Task<User> GetSingleUserByEmailAsync(string email)
        {
            var users = await Querry
                .Where(x => x.Email.Equals(email))
                .FirstOrDefaultAsync()
                ?? throw new AuthenticationException(ExceptionMessages.AuthenticationException);

            return users;
        }

        public async Task<IEnumerable<UserResponseModel>> GetByPhoneNumberAsync(string phoneNumber)
        {
            var user = await Querry
                .Where(x => x.PhoneNumber.Contains(phoneNumber))
                .Select(x => new UserResponseModel(x))
                .ToListAsync();

            return user;
        }

        public async Task<IEnumerable<UserResponseModel>> GetByUsernameAsync(string username)
        {
            var user = await Querry
                .Where(x => x.UserName.ToLower().Contains(username.ToLower()))
                .Select(x => new UserResponseModel(x)).ToListAsync();

            return user;
        }

        public async Task<IEnumerable<UserResponseModel>> SearchAllFieldsAsync(string searchWord)
        {

            var byEmail = await GetByEmailAsync(searchWord);
            var byUsername = await GetByUsernameAsync(searchWord);
            var byPhone = await GetByPhoneNumberAsync(searchWord);

            var searched = new HashSet<UserResponseModel>();

            byEmail.ToList().ForEach(x => searched.Add(x));
            byUsername.ToList().ForEach(x => searched.Add(x));
            byPhone.ToList().ForEach(x => searched.Add(x));

            return searched;
        }

        public async Task<User> CreateAsync(User user)
        {
            validator.ValidateUsername(user.UserName);
            validator.ValidateEmail(user.Email);
            validator.ValidatePhoneNumber(user.PhoneNumber);
            validator.ValidatePassword(user.Password);
            var hashedpassword = new PasswordHasher<object?>().HashPassword(null, user.Password);
            user.Password = hashedpassword;

            await this.database.Users.AddAsync(user);
            await this.database.SaveChangesAsync();

            return user;
        }
        public async Task<User> UpdateAsync(int id, User user)
        {
            var userToBeUpdated = await this.database.Users
                .FirstOrDefaultAsync(x => x.UserId == id)
                ?? throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(User)));
            userToBeUpdated.FirstName = user.FirstName;
            userToBeUpdated.LastName = user.LastName;
            userToBeUpdated.Email = user.Email;
            userToBeUpdated.PhoneNumber = user.PhoneNumber;
            userToBeUpdated.ProfilePicture = user.ProfilePicture;

            await this.database.SaveChangesAsync();

            return user;
        }
        public async Task<User> UpdatePassword(int userId, string oldPassword, string newPassword)
        {
            var userToBeUpdated = await this.database.Users
                .FirstOrDefaultAsync(x => x.UserId == userId)
                ?? throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(User)));

            var passwordVerificationResult = new PasswordHasher<object?>().VerifyHashedPassword(null, userToBeUpdated.Password, oldPassword);
            switch (passwordVerificationResult)
            {
                case PasswordVerificationResult.Failed:
                    throw new AuthenticationException(ExceptionMessages.WrongCredentials);
            }
            this.validator.ValidatePassword(newPassword);
            var hashedpassword = new PasswordHasher<object?>().HashPassword(null, newPassword);
            userToBeUpdated.Password = hashedpassword;

            await this.database.SaveChangesAsync();

            return userToBeUpdated;
        }
        public async Task<bool> DeleteAsync(int id)
        {
            var userToBeDeleted = await Querry
                .FirstOrDefaultAsync(x => x.UserId == id)
                ?? throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(User)));

            this.database.Users.Remove(userToBeDeleted);
            await this.database.SaveChangesAsync();

            return true;
        }

        public async Task<int> GetCountAsync()
        {
            var count = await this.database.Users.CountAsync();

            return count;
        }

        public async Task<IEnumerable<Rating>> GetGivenRatingAsync(int id)
        {
            var ratings = await this.database.Ratings
                .Where(x => x.RatingUserId == id)
                .Include(x => x.RatedUser)
                .ToListAsync();

            return ratings;
        }

        public async Task<IEnumerable<Rating>> GetRecievedRatingAsync(int id)
        {
            var ratings = await this.database.Ratings
                .Where(x => x.RatedUserId == id)
                .Include(x => x.RatingUser)
                .ToListAsync();

            return ratings;
        }

        public async Task<IEnumerable<UserResponseModel>> GetTopOrganizersAsync()
        {
            var top10 = await Querry
                .Where(x => x.TripsAsDriver.Count() > 0)
                .OrderByDescending(x => x.RecievedRatings.Average(x => x.RatingPoints))
                .Select(x => new UserResponseModel(x))
                .ToListAsync();

            return top10;
        }

        public async Task<IEnumerable<UserResponseModel>> GetTopPassangersAsync()
        {
            var top10 = await Querry
                .Where(x => x.TripsAsPassanger.Count() > 0)
                .OrderByDescending(x => x.TripsAsPassanger.Count())
                .OrderByDescending(x => x.RecievedRatings.Average(x => x.RatingPoints))
                .Select(x => new UserResponseModel(x))
                .ToListAsync();

            return top10;
        }

        public async Task<User> BlockAsync(int id)
        {
            var user = await Querry
                .FirstOrDefaultAsync(x => x.UserId == id)
                ?? throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(User)));

            user.Blocked = true;
            await this.database.SaveChangesAsync();

            return user;
        }
        public async Task<User> UnblockAsync(int id)
        {
            var user = await Querry
                .FirstOrDefaultAsync(x => x.UserId == id)
                ?? throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(User)));

            user.Blocked = false;
            await this.database.SaveChangesAsync();

            return user;
        }
        public async Task<TripResponseModel> ApplyForTrip(int userId, int tripId)
        {
            var tripToApplyTo = await this.database.Trips
            .Include(x => x.Driver)
            .Include(x => x.StartingPoint)
            .ThenInclude(x => x.City)
            .Include(x => x.EndingPoint)
            .ThenInclude(x => x.City)
            .Include(x => x.Passangers)
            .ThenInclude(x => x.Passanger)
            .Include(x => x.CandidatesForTrip)
                .Where(x => x.TripId.Equals(tripId))
                .FirstOrDefaultAsync()
                ?? throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(Trip)));

            var user = await GetByIdAsync(userId);

            if (tripToApplyTo.DriverId.Equals(userId))
            {
                throw new InvalidUserOperationException(ExceptionMessages.CannotApplyForYourOwnTrip);
            }
            if (tripToApplyTo.FreeSeats == 0)
            {
                throw new InvalidUserOperationException(ExceptionMessages.TripIsFull);
            }
            if (user.AppliedTripId != null)
            {
                if (user.AppliedTripId.Equals(tripId))
                    throw new InvalidUserOperationException("You can't apply twice for a trip. duh.");
                throw new InvalidUserOperationException("You can't apply for another trip.");
            }
            tripToApplyTo.CandidatesForTrip.Add(user);
            user.AppliedTripId = tripId;
            user.AppliedTrip = tripToApplyTo;
            await this.database.SaveChangesAsync();

            return new TripResponseModel(tripToApplyTo);
        }
        public async Task<TripResponseModel> LeaveFromCandidates(int userId, int tripId)
        {
            var trip = await this.database.Trips
            .Include(x => x.Driver)
            .Include(x => x.StartingPoint)
            .ThenInclude(x => x.City)
            .Include(x => x.EndingPoint)
            .ThenInclude(x => x.City)
            .Include(x => x.Passangers)
            .ThenInclude(x => x.Passanger)
            .Include(x => x.CandidatesForTrip)
                .Include(x => x.CandidatesForTrip)
                .Where(x => x.TripId.Equals(tripId))
                .FirstOrDefaultAsync();
            if (!trip.CandidatesForTrip.Any(x => x.UserId.Equals(userId)))
            {
                throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(User)));
            }
            var user = trip.CandidatesForTrip.FirstOrDefault(x => x.UserId.Equals(userId));
            trip.CandidatesForTrip.Remove(user);
            user.AppliedTripId = null;
            user.AppliedTrip = null;
            await this.database.SaveChangesAsync();
            return new TripResponseModel(trip);
        }
        public async Task<TripResponseModel> LeaveTrip(int userId, int tripId)
        {
            var trip = await this.database.Trips
                .Include(x => x.Driver)
                .Include(x => x.StartingPoint)
                .ThenInclude(x => x.City)
                .Include(x => x.EndingPoint)
                .ThenInclude(x => x.City)
                .Include(x => x.Passangers)
                .ThenInclude(x => x.Passanger)
                .Where(x => x.TripId.Equals(tripId))
                .FirstOrDefaultAsync()
                ?? throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(User)));
            if (trip.Passangers.Count <= 0 || !trip.Passangers.Any(x => x.UserId.Equals(userId)))
            {
                throw new InvalidUserOperationException(ExceptionMessages.CannotLeaveCar);
            }
            var passengerTrip = trip.Passangers.FirstOrDefault(x => x.UserId.Equals(userId));
            var user = await this.database.Users.FirstOrDefaultAsync(x => x.UserId == userId);
            //var passengerTrip = await this.database.PassangerTrips.FirstOrDefaultAsync(x => x.TripId == tripId);
            trip.Passangers.Remove(passengerTrip);
            trip.FreeSeats++;
            user.TripsAsPassanger.Remove(passengerTrip);
            await this.database.SaveChangesAsync();
            return new TripResponseModel(trip);
        }

        public async Task<IEnumerable<AddressResponseModel>> TakeUserAddresses(int userId)
        {
            var addresses = await this.database.Addresses
                .Include(x => x.City)
                .Where(x => x.UserUsesId == userId)
                .Select(x => new AddressResponseModel(x))
                .ToListAsync();

            return addresses;
        }

        //new
        public async Task<DataTablePaginator<UserResponseModel>> GetAllPaginatedUsersAsync(int page)
        {
            var count = await database.Users.CountAsync();
            var skip = (page * Pagin.Take) - Pagin.Take;
            var paginatedUsers = await Querry
                .OrderByDescending(x => x.UserId)
                .Skip(skip).Take(Pagin.Take)
                .Select(x => new UserResponseModel(x))
                .ToListAsync();
            var PaginatedUsers = new DataTablePaginator<UserResponseModel>(paginatedUsers, page, count);

            return PaginatedUsers;
        }

        public async Task<DataTablePaginator<UserResponseModel>> FilterBySearchword(string searchWord)
        {
            var filteredUsers = await SearchAllFieldsAsync(searchWord);

            filteredUsers.ToList()
            .OrderByDescending(x => x.UserId);

            var PaginatedTripsByDestination = new DataTablePaginator<UserResponseModel>(filteredUsers, 1, 1);
            return PaginatedTripsByDestination;
        }
    }
}
