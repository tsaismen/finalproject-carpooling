﻿using CarPooling.Data.Database.Contracts;
using CarPooling.Data.Models;
using CarPooling.Service.ResponseModels;
using CarPooling.Service.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Service.Services
{
    public class ChatService : IChatService
    {
        private readonly IDatabase database;

        public ChatService(IDatabase database)
        {
            this.database = database;
        }

        public async Task<IEnumerable<MessageResponseModel>> GetAllMessagesAsync()
        {
            var messages = await this.database.Messages
                .Include(x => x.User)
                .Select(x => new MessageResponseModel(x))
                .ToListAsync();

            return messages;
        }

        public async Task<bool> CreateMessageAsync(Message message)
        {
                await this.database.Messages.AddAsync(message);
                await this.database.SaveChangesAsync();

                return true;
        }
    }
}
