﻿using CarPooling.Data.Database.Contracts;
using CarPooling.Data.Models;
using CarPooling.Data.Models.Enums;
using CarPooling.Service.Constants;
using CarPooling.Service.ResponseModels;
using CarPooling.Service.Services.Contracts;
using CarPooling.Services.Exceptions;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CarPooling.Service.Services
{
    public class TripService : ITripService
    {
        private readonly IDatabase database;
        private readonly IUserService userService;

        public TripService(IDatabase database, IUserService userService)
        {
            this.database = database;
            this.userService = userService;
        }

        private IQueryable<Trip> Querry
            => this.database.Trips
            .Include(x => x.Driver)
            .Include(x => x.StartingPoint)
            .ThenInclude(x => x.City)
            .Include(x => x.EndingPoint)
            .ThenInclude(x => x.City)
            .Include(x => x.Passangers)
            .ThenInclude(x => x.Passanger)
            .ThenInclude(x => x.RecievedRatings)
            .Include(x => x.CandidatesForTrip);
        public async Task<Trip> GetByIdAsync(int id)
        {
            var trip = await Querry
                .FirstOrDefaultAsync(x => x.TripId.Equals(id))
                ?? throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(Trip)));
            return trip;
        }
        public async Task<IEnumerable<TripResponseModel>> GetAllAsync()
        {
            var allTrips = await Querry
                .Select(x => new TripResponseModel(x))
                .ToListAsync();

            return allTrips;
        }
        public async Task<IEnumerable<TripResponseModel>> GetAllActiveAsync()
        {
            var allActiveTrips = await Querry
                .Where(x => x.TripStatus == TripStatus.Active)
                .Select(x => new TripResponseModel(x))

                .ToListAsync();

            return allActiveTrips;
        }
        public async Task<IEnumerable<TripResponseModel>> GetAllCompletedAsync()
        {
            var allCompletedTrips = await Querry
             .Where(x => x.TripStatus == TripStatus.Completed)
             .Select(x => new TripResponseModel(x))
             .ToListAsync();

            return allCompletedTrips;
        }
        public async Task<Trip> CreateAsync(Trip tripModel)
        {
            await this.database.Trips.AddAsync(tripModel);
            await this.database.SaveChangesAsync();

            return tripModel;
        }
        public async Task<Trip> UpdateAsync(int id, Trip incomingTrip)
        {

            var tripToUpdate = await Querry
                .FirstOrDefaultAsync(x => x.TripId.Equals(id))
                ?? throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(Trip)));

            tripToUpdate.CarColor = incomingTrip.CarColor;
            tripToUpdate.CarModel = incomingTrip.CarModel;
            tripToUpdate.FreeSeats = incomingTrip.FreeSeats;
            tripToUpdate.StartingPointId = incomingTrip.StartingPointId;
            tripToUpdate.StartingPoint = await this.database.Addresses.FirstOrDefaultAsync(x => x.AddressId == incomingTrip.StartingPointId);
            tripToUpdate.EndingPointId = incomingTrip.EndingPointId;
            tripToUpdate.EndingPoint = await this.database.Addresses.FirstOrDefaultAsync(x => x.AddressId == incomingTrip.EndingPointId);
            tripToUpdate.DepartureTime = incomingTrip.DepartureTime;
            tripToUpdate.PetsAllowed = incomingTrip.PetsAllowed;
            tripToUpdate.SmokingAllowed = incomingTrip.SmokingAllowed;
            tripToUpdate.AirConditioner = incomingTrip.AirConditioner;
            tripToUpdate.TripStatus = incomingTrip.TripStatus;

            await this.database.SaveChangesAsync();

            return tripToUpdate;
        }
        public async Task<bool> DeleteAsync(int id)
        {
            var tripToDelete = await Querry
                .FirstOrDefaultAsync(x => x.TripId.Equals(id))
                ?? throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(Trip)));

            this.database.Trips.Remove(tripToDelete);
            await this.database.SaveChangesAsync();

            return true;
        }
        public async Task<TripResponseModel> ChangeTripStatus(int tripId, string newStatus)
        {
            var tripToChange = await this.Querry
                .FirstOrDefaultAsync(x => x.TripId.Equals(tripId))
                ?? throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(Trip)));

            if (newStatus.ToLower().Equals("completed"))
            {
                tripToChange.TripStatus = TripStatus.Completed;
            }
            else if (newStatus.ToLower().Equals("canceled"))
            {
                tripToChange.TripStatus = TripStatus.Canceled;
            }
            else
            {
                throw new InvalidInputDataException(ExceptionMessages.TripStatusInvalid);
            }
            await this.database.SaveChangesAsync();
            return new TripResponseModel(tripToChange);
        }
        public async Task<IEnumerable<TripResponseModel>> GetTripsByPassangerId(int passangerId)
        {
            var userTripsAsPassanger = await this.database.PassangerTrips
                .Include(x => x.Trip)
                .ThenInclude(x => x.StartingPoint)
                .ThenInclude(x => x.City)
                .Include(x => x.Trip)
                .ThenInclude(x => x.EndingPoint)
                .ThenInclude(x => x.City)
                .Include(x => x.Trip)
                .ThenInclude(x => x.Passangers)
                .ThenInclude(x => x.Passanger)
                .Include(x => x.Trip)
                .ThenInclude(x => x.CandidatesForTrip)
                .Include(x => x.Trip)
                .ThenInclude(x => x.Driver)
                .Where(x => x.UserId.Equals(passangerId))
                .Select(x => new TripResponseModel(x.Trip))
                .ToListAsync()
                ?? throw new EntityNotFoundException(ExceptionMessages.NotFound);

            return userTripsAsPassanger;
        }
        public async Task<IEnumerable<TripResponseModel>> GetTripsByDriverId(int driverId)
        {
            var userTripsAsDriver = await Querry
                .Where(x => x.DriverId.Equals(driverId))
                .Where(x => x.Driver.TripsAsDriver.Count() > 0)
                .Select(x => new TripResponseModel(x))
                .ToListAsync();

            return userTripsAsDriver;
        }
        public async Task<TripResponseModel> ApproveCandidatesAsync(int tripId, int candidateId)
        {
            var trip = await Querry
                .Where(x => x.TripId.Equals(tripId))
                .FirstOrDefaultAsync()
                ?? throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(User)));
            if (trip.FreeSeats == 0)
            {
                throw new InvalidUserOperationException(ExceptionMessages.TripIsFull);
            }
            if (!trip.CandidatesForTrip.Any(x => x.UserId.Equals(candidateId)))
            {
                throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(User)));
            }
            var user = await userService.GetByIdAsync(candidateId);
            trip.Passangers.Add(new PassengerTrip()
            {
                TripId = trip.TripId,
                Trip = trip,
                UserId = user.UserId,
                Passanger = user
            });
            trip.CandidatesForTrip.Remove(user);
            trip.FreeSeats--;
            await this.database.SaveChangesAsync();
            return new TripResponseModel(trip);
        }
        public async Task<TripResponseModel> DeclineCandidatesAsync(int tripId, int candidateId)
        {
            var trip = await Querry
                .Where(x => x.TripId.Equals(tripId))
                .FirstOrDefaultAsync()
                ?? throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(User)));

            if (!trip.CandidatesForTrip.Any(x => x.UserId.Equals(candidateId)))
            {
                throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(User)));
            }
            var user = trip.CandidatesForTrip.FirstOrDefault(x => x.UserId.Equals(candidateId));
            trip.CandidatesForTrip.Remove(user);
            await this.database.SaveChangesAsync();
            return new TripResponseModel(trip);
        }

        public async Task<TripResponseModel> RemovePassanger(int tripId, int passangerId)
        {
            var trip = await Querry
                .Where(x => x.TripId.Equals(tripId))
                .FirstOrDefaultAsync()
                ?? throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(User)));

            if (trip.Passangers.Count <= 0)
            {
                throw new InvalidUserOperationException(ExceptionMessages.CannotRemoveFromTrip);
            }
            var user = trip.Passangers
                .FirstOrDefault(x => x.UserId.Equals(passangerId))
                ?? throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(User)));
            trip.Passangers.Remove(user);
            trip.FreeSeats++;
            await this.database.SaveChangesAsync();
            return new TripResponseModel(trip);
        }
        public async Task<DataTablePaginator<TripResponseModel>> GetAllPaginatedTripsAsync(int page)
        {
            var count = await database.Trips.CountAsync();
            var skip = (page * Pagin.Take) - Pagin.Take;
            var paginatedTrips = await Querry
                .OrderByDescending(x => x.TripId)
                .Skip(skip).Take(Pagin.Take)
                .Select(x => new TripResponseModel(x))
                .ToListAsync();
            var PaginatedTrips = new DataTablePaginator<TripResponseModel>(paginatedTrips, page, count);

            return PaginatedTrips;
        }
        public async Task<DataTablePaginator<TripResponseModel>> GetPaginatedTripsByDriverId(int page, int driverId)
        {
            var skip = (page * Pagin.Take) - Pagin.Take;
            var count = await this.database.Trips
               .Where(x => x.DriverId.Equals(driverId))
               .CountAsync();
            var userTripsAsDriver = await Querry
                .Where(x => x.DriverId.Equals(driverId))
                .Where(x => x.Driver.TripsAsDriver.Count() > 0)
                .OrderByDescending(x => x.TripId)
                .Skip(skip).Take(Pagin.Take)
                .Select(x => new TripResponseModel(x))
                .ToListAsync();
            var PaginatedTripsAsDriver = new DataTablePaginator<TripResponseModel>(userTripsAsDriver, page, count);
            return PaginatedTripsAsDriver;
        }
        public async Task<DataTablePaginator<TripResponseModel>> GetPaginatedTripsByPassangerId(int page, int passangerId)
        {
            var skip = (page * Pagin.Take) - Pagin.Take;
            var count = await this.database.PassangerTrips
                .Where(x => x.UserId.Equals(passangerId))
                .CountAsync();
            var userTripsAsPassanger = await this.database.PassangerTrips
                .Include(x => x.Trip)
                .ThenInclude(x => x.StartingPoint)
                .ThenInclude(x => x.City)
                .Include(x => x.Trip)
                .ThenInclude(x => x.EndingPoint)
                .ThenInclude(x => x.City)
                .Include(x => x.Trip)
                .ThenInclude(x => x.Passangers)
                .ThenInclude(x => x.Passanger)
                .Include(x => x.Trip)
                .ThenInclude(x => x.CandidatesForTrip)
                .Include(x => x.Trip)
                .ThenInclude(x => x.Driver)
                .Where(x => x.UserId.Equals(passangerId))
                .OrderByDescending(x => x.TripId)
                .Skip(skip).Take(Pagin.Take)
                .Select(x => new TripResponseModel(x.Trip))
                .ToListAsync()
                ?? throw new EntityNotFoundException(ExceptionMessages.NotFound);

            var PaginatedTripsAsPassanger = new DataTablePaginator<TripResponseModel>(userTripsAsPassanger, page, count);
            return PaginatedTripsAsPassanger;
        }
        public async Task<DataTablePaginator<TripResponseModel>> FilterByDestination(string destination)
        {
            var tripsByDestination = await this.Querry
                .Where(x => x.TripStatus.Equals(TripStatus.Active) && (x.EndingPoint.Street.ToLower().Contains(destination.ToLower()) || x.EndingPoint.City.Name.ToLower().Contains(destination.ToLower())))
                .OrderByDescending(x => x.TripId)
                .Select(x => new TripResponseModel(x))
                .ToListAsync();
            var PaginatedTripsByDestination = new DataTablePaginator<TripResponseModel>(tripsByDestination, 1, 1);
            return PaginatedTripsByDestination;
        }

        public async Task<TripWeatherResponseModel> GetWeather(Trip trip)
        {
            var lat = ((trip.StartingPoint.City.Latitude + trip.EndingPoint.City.Latitude) / 2).ToString(System.Globalization.CultureInfo.InvariantCulture);

            var lon = ((trip.StartingPoint.City.Longitude + trip.EndingPoint.City.Longitude) / 2).ToString(System.Globalization.CultureInfo.InvariantCulture);

            var departure = (trip.DepartureTime.Date - DateTime.Now.Date).Days;
            if (departure < 0 || departure > 15)
            {
                departure = 0;
            }

            var client = new HttpClient();

            string uri = $"https://weatherbit-v1-mashape.p.rapidapi.com/forecast/daily?lat={lat}&lon={lon}";

            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(uri),
                Headers =
                        {
                            { "x-rapidapi-host", "weatherbit-v1-mashape.p.rapidapi.com" },
                            { "x-rapidapi-key", "de6cf78228msha489c4cf7da7224p18527cjsndf7490fb1908" },
                        },
            };
            using (var response = await client.SendAsync(request))
            {
                try
                {
                    response.EnsureSuccessStatusCode();
                    var jsonString = await response.Content.ReadAsStringAsync();

                    var obj = JObject.Parse(jsonString);
                    var visability = obj["data"][departure]["vis"];
                    var date = obj["data"][departure]["datetime"];
                    var temperature = obj["data"][departure]["temp"];
                    var description = obj["data"][departure]["weather"]["description"];
                    var icon = obj["data"][departure]["weather"]["icon"];
                    var countrycode = obj["country_code"];
                    var cityname = obj["city_name"];

                    var weatherModel = new TripWeatherResponseModel
                    {
                        Temperature = $"{temperature:f1}",
                        Date = $"{date}",
                        Visibility = $"{visability:f2}",
                        Description = $"{description}",
                        CountryCode = $"{countrycode}",
                        CityName = $"{cityname}",
                        Icon = $"{icon}",
                    };
                    return weatherModel;
                }
                catch (Exception)
                {

                    return null;
                }

            }
        }
    }
}
