﻿using CarPooling.Data.Database.Contracts;
using CarPooling.Service.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Service.Services
{
    public class HelperService : IHelperService
    {
        private readonly IDatabase database;
        public HelperService(IDatabase database)
        {
            this.database = database;
        }
        public async Task<int> CitiesCount()
        {
            return await this.database.Cities.CountAsync();
        }
        public async Task<int> UsersCount()
        {
            return await this.database.Users.CountAsync();
        }
        public async Task<int> PositiveRatingsCount()
        {
            return await this.database.Ratings.Where(x => x.RatingPoints > 3).CountAsync();
        }
        public async Task<int> TripsCount()
        {
            return await this.database.Trips.CountAsync();
        }

    }
}
