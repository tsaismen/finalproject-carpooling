﻿using CarPooling.Data.Database.Contracts;
using CarPooling.Data.Models;
using CarPooling.Service.Constants;
using CarPooling.Service.ResponseModels;
using CarPooling.Service.Services.Contracts;
using CarPooling.Services.Exceptions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Service.Services
{
    public class RatingService : IRatingService
    {
        private readonly IDatabase database;
        public RatingService(IDatabase database)
        {
            this.database = database;
        }
        private IQueryable<Rating> Querry
            => this.database.Ratings
            .Include(x => x.RatingUser)
            .Include(x => x.RatedUser);
        public async Task<Rating> GetByIdAsync(int id)
        {
            var rating = await Querry
                .Where(x => x.RatingId.Equals(id))
                .FirstOrDefaultAsync()
                ?? throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(Rating)));
            return rating;
        }
        public async Task<IEnumerable<RatingResponseModel>> GetAllAsync()
        {
            var ratings = await Querry
                .Select(x => new RatingResponseModel(x))
                .ToListAsync();

            return ratings;
        }
        public async Task<Rating> CreateAsync(Rating rating)
        {
            if (await Querry.AnyAsync(x => x.RatedUserId.Equals(rating.RatedUserId) && x.RatingUserId.Equals(rating.RatingUserId)))
            {
                throw new DuplicateEntityException(ExceptionMessages.CannotRateAgain);
            }
            await this.database.Ratings.AddAsync(rating);
            await this.database.SaveChangesAsync();
            return rating;
        }
        public async Task<Rating> UpdateAsync(int id, Rating incomingRating)
        {
            var ratingToUpdate = await Querry
                .FirstOrDefaultAsync(x => x.RatingId.Equals(id))
                ?? throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(Rating)));
            ratingToUpdate.RatingPoints = incomingRating.RatingPoints;
            ratingToUpdate.Comment = incomingRating.Comment;
            await this.database.SaveChangesAsync();
            return ratingToUpdate;
        }
        public async Task<Rating> DeleteAsync(int id)
        {
            var ratingToDelete = await Querry
                .FirstOrDefaultAsync(x => x.RatingId.Equals(id))
                ?? throw new EntityNotFoundException(string.Format(ExceptionMessages.NotFound, nameof(Rating)));
            this.database.Ratings.Remove(ratingToDelete);
            await this.database.SaveChangesAsync();
            return ratingToDelete;
        }
        public async Task<DataTablePaginator<RatingResponseModel>> GetPaginatedRatingForUser(int page, int userId)
        {
            var count = await database.Ratings
                .Where(x => x.RatedUserId.Equals(userId))
                .CountAsync();
            var skip = (page * Pagin.Take) - Pagin.Take;
            var paginatedRatings = await Querry
                .Where(x => x.RatedUserId.Equals(userId))
                .Include(x => x.RatingUser)
                .OrderByDescending(x => x.RatingId)
                .Skip(skip).Take(Pagin.Take)
                .Select(x => new RatingResponseModel(x))
                .ToListAsync();
            var paginatedRatingsList = new DataTablePaginator<RatingResponseModel>(paginatedRatings, page, count);

            return paginatedRatingsList;
        }
    }
}
