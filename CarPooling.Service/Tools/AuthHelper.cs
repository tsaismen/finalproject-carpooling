﻿using CarPooling.Data.Models;
using CarPooling.Service.Services.Contracts;
using CarPooling.Services.Exceptions;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace CarPooling.Service.Tools
{
    public class AuthHelper : IAuthHelper
    {
        private readonly IUserService userService;

        public AuthHelper(IUserService userService)
        {
            this.userService = userService;
        }

        public async Task<User> FetchUserByCredentials(string email, string password)
        {
            try
            {
                var user = await this.userService.GetSingleUserByEmailAsync(email);
                if (user.Blocked)
                    return null;

                var passwordVerificationResult = new PasswordHasher<object?>().VerifyHashedPassword(null, user.Password, password);
                switch (passwordVerificationResult)
                {
                    case PasswordVerificationResult.Failed:
                        return null;
                    case PasswordVerificationResult.Success:
                        return user;
                    case PasswordVerificationResult.SuccessRehashNeeded:
                        return user;
                    default:
                        return null;
                }
            }
            catch (AuthenticationException)
            {
                return null;
            }
        }
        public bool IsAdminCheck(User user)
        {
            if (!user.Role.Equals(Role.Admin))
                return false;
            return true;
        }
    }
}
