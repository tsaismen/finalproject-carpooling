﻿using CarPooling.Data.Models;
using System.Threading.Tasks;

namespace CarPooling.Service.Tools
{
    public interface IAuthHelper
    {
        Task<User> FetchUserByCredentials(string email, string password);

        bool IsAdminCheck(User user);
    }
}
