﻿using CarPooling.Data.Database.Contracts;
using CarPooling.Data.Models;
using CarPooling.Service.Constants;
using CarPooling.Services.Exceptions;
using System.Linq;
using System.Text.RegularExpressions;

namespace CarPooling.Service.Tools
{
    public class Validator
    {
        private readonly IDatabase database;

        public Validator(IDatabase database)
        {
            this.database = database;
        }

        public void ValidateUsername(string username)
        {
            if (this.database.Users.Any(x => x.UserName.Equals(username)))
            {
                throw new DuplicateEntityException(string.Format(ExceptionMessages.Duplicate, nameof(User), username));
            }
        }

        public void ValidateEmail(string email)
        {
            if (this.database.Users.Any(x => x.Email.Equals(email)))
            {
                throw new DuplicateEntityException(string.Format(ExceptionMessages.Duplicate, nameof(User), email));
            }
        }

        public void ValidatePassword(string password)
        {
            Regex rx = new Regex(@"(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$!%*?&^*()_\-=+/.,'<>:;~`])[A-Za-z\d@#$!%*?&^*()_\-=+/.,'<>:;~`]{8,}",
            RegexOptions.Compiled);

            if (!rx.IsMatch(password))
            {
                throw new InvalidInputDataException(ExceptionMessages.WrongFormatPassword);
            }
        }

        public void ValidatePhoneNumber(string phoneNumber)
        {
            if (this.database.Users.Any(x => x.PhoneNumber.Equals(phoneNumber)))
            {
                throw new DuplicateEntityException(string.Format(ExceptionMessages.Duplicate, nameof(User), phoneNumber));
            }
        }

        //ask? can it be combined in a validate user
    }
}
