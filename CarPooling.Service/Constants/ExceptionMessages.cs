﻿namespace CarPooling.Service.Constants
{
    public class Pagin
    {
        public const int Take = 8;
    }
    public class ExceptionMessages
    {
        //General
        public const string NotFound = "Cannot find {0} with that id!";
        public const string YouCannotDoThat = "You cannot do this action!";
        public const string Duplicate = "{0} with that {1} already exsists!";
        public const string InvalidData = "The data that you have entered is invalid!";

        //Authentication
        public const string AuthenticationException = "User with that email does not exist!";
        public const string WrongFormatPassword = "Please ensure that you password has atleast one lowercase, upercase, special symbol and digit!";
        public const string WrongCredentials = "Wrong credentials!";

        //Objects
        public const string SuccessfullyDeleted = "{0} successfully deleted!";
        public const string NullObject = "Input object cannot be null!";

        //Specific
        public const string TripIsFull = "No free seats available!";
        public const string TripStatusInvalid = "Trip status is Invalid!";
        public const string CannotLeaveCar = "You are not a part of the trip to do that!";
        public const string TripMultipleApply = "You can't apply for the same trip twice!";
        public const string CannotRemoveFromTrip = "There are no passengers!";
        public const string CannotApplyForYourOwnTrip = "You cannot apply for your own trip!";

        public const string CannotRateAgain = "You cannot rate the same person again!";
    }
}
