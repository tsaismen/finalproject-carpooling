﻿using CarPooling.API.Helpers;
using CarPooling.API.PostModels;
using CarPooling.Service.Constants;
using CarPooling.Service.Services.Contracts;
using CarPooling.Service.Tools;
using CarPooling.Services.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AddressesController : ControllerBase
    {
        private readonly IAddressService addressService;
        private readonly IAuthHelper authHelper;

        public AddressesController(IAddressService addressService, IAuthHelper authHelper)
        {
            this.addressService = addressService;
            this.authHelper = authHelper;
        }

        [HttpGet("{id}")]
        [SwaggerOperation(Summary = "Returns the address matching the provided Id from the database.")]

        public async Task<IActionResult> GetByIdAsync(int id, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);

            try
            {
                var addressResponseModel = await addressService.GetByIdAsync(id);
                return this.Ok(addressResponseModel);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }

        [HttpGet("")]
        [SwaggerOperation(Summary = "Returns all addresses from the database.")]

        public async Task<IActionResult> GetAllAsync([FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);

            var addressResponseModel = await addressService.GetAllAsync();
            return this.Ok(addressResponseModel);
        }
        [HttpPost("")]
        [SwaggerOperation(Summary = "Creates an address in the database.")]

        public async Task<IActionResult> CreateAsync([FromBody] AddressPostModel addressModel, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);

            if (addressModel == null)
            {
                return this.BadRequest(ExceptionMessages.NullObject);
            }

            try
            {
                var address = ModelMapper.ToModel(addressModel);
                address.UserUsesId = user.UserId;
                var addressResponseModel = await addressService.CreateAsync(address);
                return this.Created("post", addressResponseModel);
            }
            catch (DuplicateEntityException e)
            {
                return this.Conflict(e.Message);
            }
        }

        [HttpPut("{addressId}")]
        [SwaggerOperation(Summary = "Updates an address in the database.")]

        public async Task<IActionResult> UpdateAsync(int addressId, [FromBody] AddressPostModel addressModel, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            if (!user.UsedAddresses.Any(x => x.AddressId.Equals(addressId)))
                return this.Unauthorized(ExceptionMessages.YouCannotDoThat);
            try
            {
                var address = ModelMapper.ToModel(addressModel);
                var updatedAddress = await addressService.UpdateAsync(addressId, address);
                return this.Ok(updatedAddress);
            }
            catch (DuplicateEntityException e)
            {
                return this.Conflict(e.Message);
            }
        }

        [HttpDelete("{addressId}")]
        [SwaggerOperation(Summary = "Deletes an address from the database.")]

        public async Task<IActionResult> DeleteAsync(int addressId, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            if (!user.UsedAddresses.Any(x => x.AddressId.Equals(addressId)))
                return this.Unauthorized(ExceptionMessages.YouCannotDoThat);
            try
            {
                var addressResponseModel = await addressService.DeleteAsync(addressId);
                return this.Ok("Address successfully deleted!");
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }
    }
}
