﻿using CarPooling.API.Helpers;
using CarPooling.API.PostModels;
using CarPooling.Service.Constants;
using CarPooling.Service.Services.Contracts;
using CarPooling.Service.Tools;
using CarPooling.Services.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CarPooling.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RatingsController : ControllerBase
    {
        private readonly IRatingService ratingService;
        private readonly IUserService userService;
        private readonly IAuthHelper authHelper;

        public RatingsController(IRatingService ratingService, IUserService userService, IAuthHelper authHelper)
        {
            this.ratingService = ratingService;
            this.userService = userService;
            this.authHelper = authHelper;
        }
        [HttpGet("{id}")]
        [SwaggerOperation(Summary = "Returns the rating matching the provided Id from the database.")]

        public async Task<IActionResult> GetByIdAsync(int id, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            try
            {
                var ratingResponseModel = await ratingService.GetByIdAsync(id);
                return this.Ok(ratingResponseModel);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }

        [HttpGet("")]
        [SwaggerOperation(Summary = "Returns all ratings from the database.")]

        public async Task<IActionResult> GetAllAsync([FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            var ratingResponseModel = await ratingService.GetAllAsync();
            return this.Ok(ratingResponseModel);
        }
        [HttpPost]
        [SwaggerOperation(Summary = "Creates a rating in the database.")]

        public async Task<IActionResult> CreateAsync([FromBody] RatingPostModel ratingPostModel, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            try
            {
                var rating = ModelMapper.ToModel(ratingPostModel);
                rating.RatingUserId = user.UserId;
                var ratingResponse = await ratingService.CreateAsync(rating);
                return this.Created("post", ratingResponse);
            }
            catch (DuplicateEntityException e)
            {
                return this.Conflict(e.Message);
            }
        }
        [HttpPut("{ratingId}")]
        [SwaggerOperation(Summary = "Updates a address in the database.")]

        public async Task<IActionResult> UpdateAsync(int ratingId, [FromBody] RatingPostModel ratingPostModel, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            if (!user.GivenRatings.Any(x => x.RatingId.Equals(ratingId)))
                return this.Unauthorized(ExceptionMessages.YouCannotDoThat);
            try
            {
                var rating = ModelMapper.ToModel(ratingPostModel);
                var ratingResponse = await ratingService.UpdateAsync(ratingId, rating);
                return this.Ok(ratingResponse);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }
        [HttpDelete("{ratingId}")]
        [SwaggerOperation(Summary = "Deletes a rating from the database.")]

        public async Task<IActionResult> DeleteAsync(int ratingId, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            if (!user.GivenRatings.Any(x => x.RatingId.Equals(ratingId)))
                return this.Unauthorized(ExceptionMessages.YouCannotDoThat);
            try
            {
                var ratingResponse = await ratingService.DeleteAsync(ratingId);
                return this.Ok(string.Format(ExceptionMessages.SuccessfullyDeleted, nameof(ratingResponse)));
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }

        [HttpGet("/user/{userId}/given")]
        [SwaggerOperation(Summary = "Returns all ratings that have been given by the user.")]

        public async Task<IActionResult> GetGivenRatingAsync(int userId, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            var ratings = await userService.GetGivenRatingAsync(userId);
            return this.Ok(ratings);
        }

        [HttpGet("/user/{userId}/recieved")]
        [SwaggerOperation(Summary = "Returns all ratings that have been recieved by the user.")]

        public async Task<IActionResult> GetRecievedRatingAsync(int userId, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            var ratings = await userService.GetRecievedRatingAsync(userId);
            return this.Ok(ratings);
        }

        [HttpGet("top10/organizers")]
        [SwaggerOperation(Summary = "Returns Top10 organisers with highest average rating score given from passengers.")]

        public async Task<IActionResult> GetTopOrganizersAsync()
        {
            var userResponseModel = await userService.GetTopOrganizersAsync();
            return this.Ok(userResponseModel);
        }

        [HttpGet("top10/passengers")]
        [SwaggerOperation(Summary = "Returns Top10 passengers with highest average rating score given from drivers.")]

        public async Task<IActionResult> GetTopPassangersAsync()
        {
            var userResponseModel = await userService.GetTopPassangersAsync();
            return this.Ok(userResponseModel);
        }
    }
}
