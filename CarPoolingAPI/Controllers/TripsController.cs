﻿using CarPooling.API.Helpers;
using CarPooling.API.PostModels;
using CarPooling.Service.Constants;
using CarPooling.Service.Services.Contracts;
using CarPooling.Service.Tools;
using CarPooling.Services.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TripsController : ControllerBase
    {
        private readonly ITripService tripService;
        private readonly IAuthHelper authHelper;

        public TripsController(ITripService tripService, IAuthHelper authHelper)
        {
            this.tripService = tripService;
            this.authHelper = authHelper;
        }

        [HttpGet("{id}")]
        [SwaggerOperation(Summary = "Returns the trip matching the provided Id from the database.")]

        public async Task<IActionResult> GetByIdAsync(int id, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            try
            {
                var tripResponseModel = await tripService.GetByIdAsync(id);
                return this.Ok(tripResponseModel);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }

        [HttpGet("")]
        [SwaggerOperation(Summary = "Returns all trips from the database.")]

        public async Task<IActionResult> GetAllAsync([FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            var tripResponseModel = await tripService.GetAllAsync();
            return this.Ok(tripResponseModel);
        }

        [HttpPost]
        [SwaggerOperation(Summary = "Creates a trip in the database.")]

        public async Task<IActionResult> CreateAsync([FromBody] TripPostModel tripPostModel, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            try
            {
                var trip = ModelMapper.ToModel(tripPostModel);
                trip.DriverId = user.UserId;
                var userResponseModel = await tripService.CreateAsync(trip);
                return this.Created("post", userResponseModel);
            }
            catch (DuplicateEntityException e)
            {
                return this.Conflict(e.Message);
            }
        }

        [HttpPut("{tripId}")]
        [SwaggerOperation(Summary = "Updates a trip in the database.")]

        public async Task<IActionResult> UpdateAsync([FromBody] TripPostModel tripPostModel, int tripId, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            if (!user.TripsAsDriver.Any(x => x.TripId.Equals(tripId)))
                return this.Unauthorized(ExceptionMessages.YouCannotDoThat);

            try
            {
                var trip = ModelMapper.ToModel(tripPostModel);
                var tripToUpdate = await tripService.UpdateAsync(tripId, trip);
                return this.Ok(tripToUpdate);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }
        [HttpPut("{tripId}/status")]
        [SwaggerOperation(Summary = "Changes the status of the trip provided with tripId.")]

        public async Task<IActionResult> ChangeTripStatus(int tripId, string newStatus, [FromHeader] string email, [FromHeader] string password)
        {

            var user = await this.authHelper.FetchUserByCredentials(email, password);

            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            if (!user.TripsAsDriver.Any(x => x.TripId.Equals(tripId)))
                return this.Unauthorized(ExceptionMessages.YouCannotDoThat);

            try
            {
                var tripToUpdate = await tripService.ChangeTripStatus(tripId, newStatus);
                return this.Ok(tripToUpdate);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
            catch (InvalidUserOperationException e)
            {
                return this.BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(Summary = "Deletes a trip from the database.")]

        public async Task<IActionResult> DeleteAsync(int id, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            if (!user.TripsAsDriver.Any(x => x.TripId.Equals(id)))
                return this.Unauthorized(ExceptionMessages.YouCannotDoThat);
            try
            {
                var tripResponse = await tripService.DeleteAsync(id);
                return this.Ok(string.Format(ExceptionMessages.SuccessfullyDeleted, nameof(tripResponse)));
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }

        [HttpGet("driver/{driverId}")]
        [SwaggerOperation(Summary = "Returns all the trips a user has created by his Id.")]

        public async Task<IActionResult> GetTripsByDriverId(int driverId, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            try
            {
                var tripResponseModel = await tripService.GetTripsByDriverId(driverId);
                return this.Ok(tripResponseModel);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }

        [HttpGet("passanger/{passangerId}")]
        [SwaggerOperation(Summary = "Returns all the trips that a user has participated in by his Id.")]

        public async Task<IActionResult> GetTripsByPassangerId(int passangerId, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            try
            {
                var tripResponseModel = await tripService.GetTripsByPassangerId(passangerId);
                return this.Ok(tripResponseModel);
            }
            catch (EntityNotFoundException е)
            {
                return this.NotFound(е.Message);
            }
        }

        [HttpPut("{tripId}/candidate/{userId}/approve")]
        [SwaggerOperation(Summary = "Approves a user into a trip, if he is already a candidate.")]

        public async Task<IActionResult> ApproveCandidate(int tripId, int userId, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            if (!user.TripsAsDriver.Any(x => x.TripId.Equals(tripId)))
                return this.Unauthorized(ExceptionMessages.YouCannotDoThat);
            try
            {
                var trip = await tripService.ApproveCandidatesAsync(tripId, userId);
                return this.Ok(trip);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
            catch (InvalidUserOperationException e)
            {
                return this.BadRequest(e.Message);
            }
        }

        [HttpDelete("{tripId}/candidate/{userId}")]
        [SwaggerOperation(Summary = "Declines a user from participating in a trip, if he is already a candidate.")]

        public async Task<IActionResult> DeclineCandidate(int tripId, int userId, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            if (!user.TripsAsDriver.Any(x => x.TripId.Equals(tripId)))
                return this.Unauthorized(ExceptionMessages.CannotApplyForYourOwnTrip);
            try
            {
                var trip = await tripService.DeclineCandidatesAsync(tripId, userId);
                return this.Ok(trip);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }

        [HttpDelete("{tripId}/passanger/{userId}")]
        [SwaggerOperation(Summary = "Removes a user from the passenger list in the trip.")]

        public async Task<IActionResult> RemovePassanger(int tripId, int userId, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            if (!user.TripsAsDriver.Any(x => x.TripId.Equals(tripId)))
                return this.Unauthorized(ExceptionMessages.YouCannotDoThat);
            try
            {
                var trip = await tripService.RemovePassanger(tripId, userId);
                return this.Ok(trip);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
            catch (InvalidUserOperationException e)
            {
                return this.BadRequest(e.Message);
            }
        }
    }
}
