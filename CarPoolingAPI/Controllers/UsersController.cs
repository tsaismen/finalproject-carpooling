﻿using CarPooling.API.Helpers;
using CarPooling.API.PostModels;
using CarPooling.Service.Constants;
using CarPooling.Service.Services.Contracts;
using CarPooling.Service.Tools;
using CarPooling.Services.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Threading.Tasks;

namespace CarPooling.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly IAuthHelper authHelper;

        public UsersController(IUserService userService, IAuthHelper authHelper)
        {
            this.userService = userService;
            this.authHelper = authHelper;
        }

        [HttpGet("{id}")]
        [SwaggerOperation(Summary = "Returns the user matching the provided Id from the database.")]

        public async Task<IActionResult> GetByIdAsync(int id, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            if (id != user.UserId || !authHelper.IsAdminCheck(user))
                return this.Unauthorized(ExceptionMessages.YouCannotDoThat);
            try
            {
                var userResponseModel = await userService.GetByIdAsync(id);
                return this.Ok(userResponseModel);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }

        [HttpGet("")]
        [SwaggerOperation(Summary = "Returns all users from the database.")]

        public async Task<IActionResult> GetAllAsync([FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            var userResponseModels = await userService.GetAllAsync();
            return this.Ok(userResponseModels);
        }

        [HttpGet("email")]
        [SwaggerOperation(Summary = "Searches for a user with a matching Email.")]

        public async Task<IActionResult> GetByEmailAsync(string partialEmail, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            try
            {
                var userResponseModel = await userService.GetByEmailAsync(partialEmail);
                return this.Ok(userResponseModel);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }

        [HttpGet("phoneNumber")]
        [SwaggerOperation(Summary = "Searches for a user with a matching PhoneNumber.")]

        public async Task<IActionResult> GetByPhoneNumberAsync(string phoneNumber, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            try
            {
                var userResponseModel = await userService.GetByPhoneNumberAsync(phoneNumber);
                return this.Ok(userResponseModel);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }

        [HttpGet("username")]
        [SwaggerOperation(Summary = "Searches for a user with a matching Username.")]

        public async Task<IActionResult> GetByUsernameAsync(string username, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            try
            {
                var userResponseModel = await userService.GetByUsernameAsync(username);
                return this.Ok(userResponseModel);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }

        [HttpGet("search")]
        [SwaggerOperation(Summary = "Searches for a user with the provides searchword in the fields - Username, Email, PhoneNumber")]

        public async Task<IActionResult> SearchAllFieldsAsync(string searchword, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);

            var userResponseModel = userService.SearchAllFieldsAsync(searchword);
            return this.Ok(userResponseModel);
        }

        [HttpPut("{userToBeUpdatedId}")]
        [SwaggerOperation(Summary = "Updates a user in the database.")]

        public async Task<IActionResult> UpdateAsync(int userToBeUpdatedId, [FromBody] UserPostModel userPostModel, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            if (userToBeUpdatedId != user.UserId || !authHelper.IsAdminCheck(user))
                return this.Unauthorized(ExceptionMessages.YouCannotDoThat);
            try
            {
                var updatedUserModel = ModelMapper.ToModel(userPostModel);
                var updatedUser = await userService.UpdateAsync(userToBeUpdatedId, updatedUserModel);
                return this.Ok(updatedUser);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }

        [HttpDelete("{userToBeDeletedId}")]
        [SwaggerOperation(Summary = "Deletes a user from the database.")]

        public async Task<IActionResult> DeleteAsync(int userToBeDeletedId, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            if (userToBeDeletedId != user.UserId || !authHelper.IsAdminCheck(user))
                return this.Unauthorized(ExceptionMessages.YouCannotDoThat);
            try
            {
                var userResponse = await userService.DeleteAsync(userToBeDeletedId);
                return this.Ok(string.Format(ExceptionMessages.SuccessfullyDeleted, nameof(userResponse)));
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }

        [HttpGet("Count")]
        [SwaggerOperation(Summary = "Returns the number of exsisting users.")]

        public async Task<IActionResult> GetCountAsync()
        {
            var count = await userService.GetCountAsync();
            return this.Ok(count);
        }


        [HttpPut("block/{id}")]
        [SwaggerOperation(Summary = "Blocks a user.(Admin only)")]

        public async Task<IActionResult> BlockAsync(int id, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            if (!authHelper.IsAdminCheck(user))
                return this.Unauthorized(ExceptionMessages.YouCannotDoThat);
            try
            {
                var blockedUser = await userService.BlockAsync(id);
                return this.Ok(blockedUser);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }

        [HttpPut("unblock/{id}")]
        [SwaggerOperation(Summary = "Unblocks a user.(Admin only)")]

        public async Task<IActionResult> UnblockAsync(int id, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            if (!authHelper.IsAdminCheck(user))
                return this.Unauthorized(ExceptionMessages.YouCannotDoThat);
            try
            {
                var unblockedUser = await userService.UnblockAsync(id);
                return this.Ok(unblockedUser);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }

        [HttpPut("/apply/{tripId}")]
        [SwaggerOperation(Summary = "User applies for a trip, adding himself to the candidates list.")]

        public async Task<IActionResult> ApplyForTrip(int tripId, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            try
            {
                var trip = await userService.ApplyForTrip(user.UserId, tripId);
                return this.Ok(trip);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
            catch (InvalidUserOperationException e)
            {
                return this.BadRequest(e.Message);
            }
        }

        [HttpDelete("{userId}/trips/{tripId}/leave")]
        [SwaggerOperation(Summary = "User leaves the passenger list of a trip.")]

        public async Task<IActionResult> LeaveTrip(int tripId, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            try
            {
                var trip = await userService.LeaveTrip(user.UserId, tripId);
                return this.Ok(trip);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
            catch (InvalidUserOperationException e)
            {
                return this.BadRequest(e.Message);
            }

        }

        [HttpDelete("/trips/{tripId}/unapply")]
        [SwaggerOperation(Summary = "The user unapplies from being a candidate for a trip.")]

        public async Task<IActionResult> LeaveFromCandidates(int tripId, [FromHeader] string email, [FromHeader] string password)
        {
            var user = await this.authHelper.FetchUserByCredentials(email, password);
            if (user == null)
                return this.Unauthorized(ExceptionMessages.WrongCredentials);
            try
            {
                var trip = await userService.LeaveFromCandidates(user.UserId, tripId);
                return this.Ok(trip);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }
    }
}
