﻿using CarPooling.API.Helpers;
using CarPooling.API.PostModels;
using CarPooling.Service.Services.Contracts;
using CarPooling.Services.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Threading.Tasks;

namespace CarPooling.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IUserService userService;

        public AccountController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpPost("Register")]
        [SwaggerOperation(Summary = "Creates user profile in the database.")]
        public async Task<IActionResult> RegisterAsync([FromBody] UserPostModel customerWebModel)
        {
            try
            {
                var user = ModelMapper.ToModel(customerWebModel);
                var userResponseModel = await this.userService.CreateAsync(user);
                return this.Ok(userResponseModel);
            }
            catch (DuplicateEntityException e)
            {
                return this.Conflict(e.Message);
            }
            catch (InvalidInputDataException e)
            {
                return this.BadRequest(e.Message);
            }
        }

    }
}
