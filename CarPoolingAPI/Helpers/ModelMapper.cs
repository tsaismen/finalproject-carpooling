﻿using CarPooling.API.PostModels;
using CarPooling.Data.Models;

namespace CarPooling.API.Helpers
{
    public static class ModelMapper
    {
        public static Trip ToModel(TripPostModel postModel)
        {
            var taskModel = new Trip()
            {
                CarColor = postModel.CarColor,
                CarModel = postModel.CarModel,
                StartingPointId = postModel.StartingPointId,
                EndingPointId = postModel.EndingPointId,
                DepartureTime = postModel.DepartureTime,
                PetsAllowed = postModel.PetsAllowed,
                SmokingAllowed = postModel.SmokingAllowed,
                AirConditioner = postModel.AirConditioner,
            };
            return taskModel;
        }
        public static User ToModel(UserPostModel postModel)
        {
            var userModel = new User()
            {
                UserName = postModel.UserName,
                FirstName = postModel.FirstName,
                LastName = postModel.LastName,
                Password = postModel.Password,
                Email = postModel.Email,
                PhoneNumber = postModel.PhoneNumber
            };
            return userModel;
        }

        public static Address ToModel(AddressPostModel postModel)
        {
            var userModel = new Address()
            {
                Street = postModel.Street,
                CityId = postModel.CityId,
            };
            return userModel;
        }
        public static Rating ToModel(RatingPostModel postModel)
        {
            var ratingModel = new Rating()
            {
                RatingPoints = postModel.RatingPoints,
                Comment = postModel.Comment,
                RatedUserId = postModel.RatedUserId,
            };
            return ratingModel;
        }
    }
}
