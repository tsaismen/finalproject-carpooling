﻿using System.ComponentModel.DataAnnotations;

namespace CarPooling.API.PostModels
{
    public class RatingPostModel
    {
        [Required]
        [Range(1, 5, ErrorMessage = "Points must be between 1 and 5")]
        public int RatingPoints { get; set; }
        [StringLength(200, ErrorMessage = "Comment cant be above 100 symbols")]
        public string Comment { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Id's must be positive")]
        public int RatedUserId { get; set; }
    }
}
