﻿using System.ComponentModel.DataAnnotations;

namespace CarPooling.API.PostModels
{
    public class UserPostModel
    {
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string UserName { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string FirstName { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string LastName { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 8, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string Password { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "Email must be valid")]
        public string Email { get; set; }
        [Required]
        [Phone]
        public string PhoneNumber { get; set; }
    }
}
