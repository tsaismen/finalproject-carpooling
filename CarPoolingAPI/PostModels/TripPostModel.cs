﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarPooling.API.PostModels
{
    public class TripPostModel
    {
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string CarColor { get; set; }
        [Required]
        [StringLength(40, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string CarModel { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "StartingPointId must be positive.")]
        public int StartingPointId { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "EndingPointId must be positive.")]
        public int EndingPointId { get; set; }
        public DateTime DepartureTime { get; set; }
        [Required]
        public bool PetsAllowed { get; set; }
        [Required]
        public bool SmokingAllowed { get; set; }
        [Required]
        public bool AirConditioner { get; set; }
    }
}
