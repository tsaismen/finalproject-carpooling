﻿namespace CarPooling.API.PostModels
{
    public class AddressPostModel
    {
        public string Street { get; set; }
        public int CityId { get; set; }
    }
}
